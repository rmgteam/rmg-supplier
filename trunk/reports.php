<?
require_once("utils.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."contractors.reports.class.php");
require_once($UTILS_CLASS_PATH."mysql.class.php");
require_once($UTILS_CLASS_PATH."po.class.php");

Global $UTILS_CLASS_PATH;
Global $UTILS_SERVER_PATH;
Global $UTILS_LOG_PATH;
Global $UTILS_WEBROOT;
Global $UTILS_URL_BASE;

$contractor_report = new contractor_report();
$mysql = new mysql();

if ($_REQUEST['whichaction'] == 'order'){
	$data = new data;

	$result = $contractor_report->purchase_order($_SESSION['contractors_qube_id'], $_REQUEST);
	$num_results = $mysql->num_rows($result);
	$result_array['num_results'] = $num_results;
	if($num_results > 0){
		$querystring = $contractor_report->http_parse_query($_REQUEST);
		$i = 0;
		while($row = $mysql->fetch_array($result)){

			$lines = $row['no_complete'] + $row['no_incomplete'];

			$result_array['REPORT_ID'][$i] = $row['cpm_po_id'];
			$result_array['REPORT_ONCLICK'][$i] = 'do_job('."'".$row['cpm_po_id']."', '".$querystring."'".')';
			$result_array['REPORT_NUMBER'][$i] = $row['cpm_po_number'];
			$result_array['REPORT_PROPERTY'][$i] = $row['rmc_name'];
			$result_array['REPORT_LINES'][$i] = $row['no_incomplete']."/".$lines;
			$result_array['REPORT_DATE'][$i] = $data->ymd_to_date($row['cpm_po_date_raised']);
			$i ++;
		}
	}

	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'user'){
	$data = new data;

	$result = $contractor_report->user_accounts($_SESSION['contractors_qube_id'], $_REQUEST);
	$num_results = $mysql->num_rows($result);
	$result_array['num_results'] = $num_results;
	if($num_results > 0){
		$i = 0;
		while($row = $mysql->fetch_array($result)){
			$result_array['USER_ID'][$i] = $row['cpm_contractors_user_ref'];
			$result_array['USER_ONCLICK'][$i] = "do_audit('".$row['cpm_contractors_user_ref']."')";
			$result_array['USER_USERNAME'][$i] = $row['cpm_contractors_user_ref'];
			$result_array['USER_NAME'][$i] = $row['cpm_contractors_user_name'];
			$result_array['USER_EMAIL'][$i] = $row['cpm_contractors_user_email'];
			$result_array['USER_DISABLED'][$i] = '<img src="/images/'.$row['cpm_contractors_user_disabled'].'.png" />';
			$result_array['USER_COUNT'][$i] = $row['cpm_contractors_user_name'];
			$result_array['USER_LAST'][$i] = $contractor_report->sort_date_time($row['last_login']);
			$result_array['USER_COUNT'][$i] = $row['login_count'];
			$i ++;
		}
	}

	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'audit'){
	$output = $contractor_report->user_audit($_REQUEST['user_id']);
	$result_array = $output;

	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'job'){
	$data = new data;

	$result_job = $contractor_report->job($_REQUEST['po_no'], $_REQUEST);

	$num_result_jobs = $mysql->num_rows($result_job);
	$per_page = 12;
	$pages = ceil($num_result_jobs/$per_page);
	$page_count = 1;
	$i = 0;
	if($num_result_jobs > 0){
		$output .= '<div class="header_row" style="width:565px;text-align:left;">';
		$output .= '<div class="form_label border" style="width:85px;">Job Number</div>';
		$output .= '<div class="form_label border" style="width:155px;">Expected Completion</div>';
		$output .= '<div class="form_label border" style="width:80px;">Quote</div>';
		$output .= '<div class="form_label border" style="width:120px;">Closed By</div>';
		$output .= '<div class="form_label" style="width:70px;">On</div>';
		$output .= '</div>';

		$output .= '<ul class="paging" id="job_pages"><li id="page-1">';

		while($row_job = $mysql->fetch_array($result_job)){

			$i += 1;

			$output .= '<div class="form_row" style="width:565px;">';
			$output .= '<div class="form_label border" style="width:85px;">'.$row_job['cpm_po_job_no'].'</div>';
			$output .= '<div class="form_label border" style="width:155px;">'.$data->ymd_to_date($row_job['cpm_po_job_completion_date']).'</div>';
			$output .= '<div class="form_label border" style="width:80px;">'.$row_job['cpm_po_job_amount'].'</div>';
			$output .= '<div class="form_label border" style="width:120px;">'.$contractor_report->is_blank($row_job['user_name'], $contractor_report->is_blank($row_job['cpm_po_job_user_ref'],"Not Complete")).'</div>';
			$output .= '<div class="form_label" style="width:70px;">'.$contractor_report->is_blank($data->ymd_to_date($row_job['cpm_po_job_ts'])).'</div>';
			$output .= '</div>';

			if($row_job['cpm_po_job_reason_id'] != ""){
				$output .= '<div class="sub_header_row" style="width:565px;text-align:left;">';
				$output .= '<div class="form_label" style="width:555px;">Reason for Incompletion</div>';
				$output .= '</div><div class="form_row" style="width:565px;text-align:left;">';
				$output .= '<div class="form_label" style="width:555px;"><strong>'.$row_job['the_reason'].":</strong><br />".$row_job['cpm_po_job_reason'].'</div>';
				$output .= '</div>';
			}

			if($row_job['cpm_po_job_advice'] != ""){
				$output .= '<div class="sub_header_row" style="width:565px;text-align:left;">';
				$output .= '<div class="form_label" style="width:555px;">Advice</div>';
				$output .= '</div><div class="form_row" style="width:565px;text-align:left;">';
				$output .= '<div class="form_label" style="width:555px;">'.$row_job['cpm_po_job_advice'].'</div>';
				$output .= '</div>';
			}

			if($i == $per_page){
				$i = 0;
				$page_count += 1;
				$output .= '</li><li id="page-'.$page_count.'">';
			}
		}

		$output .= '</li></ul>';
	}else{
		$output .= $sql_job;
	}

	$output .= '</div>';

	$result_array['results'] = $output;

	$output = '<ul class="pagination" id="jobination">';

	for($i=1; $i<=$pages; $i++){
		$output .= '<li id="'.$i.'"';
		if($i == 1){
			$output .= ' class="active"';
		}
		$output .= ' onclick="javascript:$('."'#job_pages'".').setPage('."'".$i."'".');">'.$i.'</li>';
	}
	$output .= '</ul>';

	$result_array['paging'] = $output;

	echo json_encode($result_array);
	exit;
}

$template = "backend";
$page_array = explode('/', $_SERVER['PHP_SELF']);
$page = str_replace('.php','',$page_array[count($page_array) - 1]);

$tpl = new Template($UTILS_SERVER_PATH.'includes/body.tpl');
$tpl->set('title', 'RMG Suppliers - Purchase Order Reports');
$tpl->set('page_title', 'Purchase Order Reports');
$tpl->set('UTILS_WEBROOT', $UTILS_WEBROOT);
$tpl->set('UTILS_LOG_PATH',$UTILS_LOG_PATH);
$tpl->set('UTILS_CLASS_PATH', $UTILS_CLASS_PATH);
$tpl->set('UTILS_URL_BASE', $UTILS_URL_BASE);
$tpl->set('UTILS_SERVER_PATH', $UTILS_SERVER_PATH);
$tpl->set('po_data', $tpl->set_sortable_table($UTILS_SERVER_PATH."templates/report_row.tpl"));
$tpl->set('job_data', $tpl->set_sortable_table($UTILS_SERVER_PATH."templates/report_job_row.tpl"));
$tpl->set('user_data', $tpl->set_sortable_table($UTILS_SERVER_PATH."templates/user_report_row.tpl"));
$header = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'_header.tpl');
$content = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'.tpl');
$page_details = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$page.'.tpl');
$tpl->set('header', $header);
$tpl->set('content', $content.$page_details);
echo $tpl->fetch();
?>