<?
require("utils.php");
require_once($UTILS_CLASS_PATH."login.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_FILE_PATH."library/securimage/securimage.php");	 

Global $UTILS_CLASS_PATH;
Global $UTILS_SERVER_PATH;
Global $UTILS_LOG_PATH;
Global $UTILS_WEBROOT;
Global $UTILS_URL_BASE;

$login = new login;
$data = new data;
$securimage = new Securimage();

if($_REQUEST['a'] == "s"){
	
	$result_array = $login->forgot_check_fields($_REQUEST, $securimage);
	$success = true;
	
	for($i=0;$i<count($result_array);$i++){
		if($result_array[$i]['outcome'] == true){
			$success = false;
		}
	}
	
	if($success === true){	
		$success = $login->do_forgot($_REQUEST);
		
		if($success === false){
			$result_array[0]['outcome'] = true;
			$result_array[0]['message'] = 'This account does not exist.';
		}
	}
	
	$result_array[0]['success'] = $success;
	
	echo json_encode($result_array);
	exit;
}

$template = "backend";
$page_array = explode('/', $_SERVER['PHP_SELF']);
$page = str_replace('.php','',$page_array[count($page_array) - 1]);

$tpl = new Template($UTILS_SERVER_PATH.'includes/body.tpl');
$tpl->set('title', 'RMG Suppliers - Forgotten Details');
$tpl->set('page_title', 'Forgotten Details?');
$tpl->set('UTILS_WEBROOT', $UTILS_WEBROOT);
$tpl->set('UTILS_LOG_PATH',$UTILS_LOG_PATH);
$tpl->set('UTILS_CLASS_PATH', $UTILS_CLASS_PATH);
$tpl->set('UTILS_URL_BASE', $UTILS_URL_BASE);
$tpl->set('UTILS_SERVER_PATH', $UTILS_SERVER_PATH);
$header = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'_header.tpl');
$content = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'.tpl');
$page_details = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$page.'.tpl');
$tpl->set('header', $header);
$tpl->set('content', $content.$page_details);
echo $tpl->fetch();
?>