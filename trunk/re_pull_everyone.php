<?
ini_set("max_execution_time","14400");
require_once("utils.php");
require_once($UTILS_CLASS_PATH."po.class.php");
require_once($UTILS_CLASS_PATH."mysql.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");

Global $UTILS_CLASS_PATH;
Global $UTILS_SERVER_PATH;
Global $UTILS_LOG_PATH;
Global $UTILS_WEBROOT;
Global $UTILS_URL_BASE;
Global $conn;

$mysql = new mysql();
$data = new data();
$po = new po;

$sql = "SELECT DISTINCT cpm_po_contractors_ref FROM cpm_po ORDER BY cpm_po_contractors_ref";
$result = $mysql->query($sql);
while( $row = $mysql->fetch_array($result) ){
	$po->retrieve_po($row['cpm_po_contractors_ref']);
}

?>