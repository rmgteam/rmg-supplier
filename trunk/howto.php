<?
require("utils.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."field.class.php");
require_once($UTILS_CLASS_PATH."contractors.class.php");
require_once($UTILS_CLASS_PATH."mysql.class.php");

Global $UTILS_CLASS_PATH;
Global $UTILS_SERVER_PATH;
Global $UTILS_LOG_PATH;
Global $UTILS_WEBROOT;
Global $UTILS_URL_BASE;

$crypt = new encryption_class;
$field = new field;
$mysql = new mysql();


$template = "backend";
$page_array = explode('/', $_SERVER['PHP_SELF']);
$page = str_replace('.php','',$page_array[count($page_array) - 1]);

$tpl = new Template($UTILS_SERVER_PATH.'includes/body.tpl');
$tpl->set('title', 'RMG Suppliers - How To');
$tpl->set('page_title', 'How To Use');
$tpl->set('UTILS_WEBROOT', $UTILS_WEBROOT);
$tpl->set('UTILS_LOG_PATH',$UTILS_LOG_PATH);
$tpl->set('UTILS_CLASS_PATH', $UTILS_CLASS_PATH);
$tpl->set('UTILS_URL_BASE', $UTILS_URL_BASE);
$tpl->set('UTILS_SERVER_PATH', $UTILS_SERVER_PATH);
//$tpl->set('user_data', $tpl->set_sortable_table($UTILS_SERVER_PATH."templates/user_row.tpl"));
$header = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'_header.tpl');
$content = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'.tpl');
$page_details = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$page.'.tpl');
$tpl->set('header', $header);
$tpl->set('content', $content.$page_details);
echo $tpl->fetch();
?>