<?
require("utils.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."field.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."mysql.class.php");

Global $UTILS_CLASS_PATH;
Global $UTILS_SERVER_PATH;
Global $UTILS_LOG_PATH;
Global $UTILS_WEBROOT;
Global $UTILS_URL_BASE;
Global $UTILS_HTTPS_ADDRESS;
Global $UTILS_TEL_MAIN;
$crypt = new encryption_class;
$field = new field;
$security = new security;
$mysql = new mysql;

$err_message = "";
//print $crypt->encrypt($UTILS_DB_ENCODE, trim("rah"));

#===================================
# Check new details
#===================================

if($_REQUEST['which_action'] == "check"){

	
	$user_id = $_SESSION['contractors_username'];
	$err_message = "";
	$contractor_email = "";
	$result_array = array();
	$result_array["errors"] = array();
	$i = 0;
	
	$result_array['email_match'] = "true";	
	
	if($_SESSION['contractors_qube_id'] == $user_id){
	
		$sql = "SELECT * 
		FROM cpm_contractors 
		WHERE cpm_contractors_qube_id = '".$_SESSION['contractors_qube_id']."'";
		$result = $mysql->query($sql, 'Get Contractor');
		$password_exists = $mysql->num_rows($result);
	
		if($password_exists > 0){
			while($row = $mysql->fetch_array($result)){
				$contractor_email = $row['cpm_contractors_email'];
			}
		}
		
		$result_array['cemail'] = $contractor_email;
		
		if(strtoupper($contractor_email) != strtoupper($_REQUEST['email'])){
			$result_array['email_match'] = "false";		
		}
	}
	
	$sql = "SELECT * 
	FROM cpm_contractors_user 
	WHERE cpm_contractors_user_ref = '".$user_id."'
	AND cpm_contractors_user_password = '".$crypt->encrypt($UTILS_DB_ENCODE, trim($_REQUEST['old_password']))."'";
	
	$result = $mysql->query($sql, 'Test password');
	$password_exists = $mysql->num_rows($result);

	if($password_exists > 0){
		while($row = $mysql->fetch_array($result)){
			
			$parent = $row['cpm_contractors_user_parent'];
			
			$is_valid = true;
			$parent_domain = "notadomain";
			
			if ($field->is_valid_email(trim($_REQUEST['email'])) == false){
				$result_array["errors"][$i]["outcome"] = true;
				$result_array["errors"][$i]["message"] = "Email address is not a valid email address";
				$i++;
			}
			
			if($parent != 0){
				$sql2 = "SELECT * 
				FROM cpm_contractors_user
				WHERE cpm_contractors_user_ref='".$parent."'";	
				$result2 = $mysql->query($sql2, 'Get Email');
				$num_rows = $mysql->num_rows($result2);
				if($num_rows > 0){
					while($row2 = $mysql->fetch_array($result2)){
						$parent_email = explode("@", $row2['cpm_contractors_user_email']);
						$parent_domain = $parent_email[1];
					}
				}
				
				if(strpos($_REQUEST['email'], $parent_domain) === false){
					$result_array["errors"][$i]["outcome"] = true;
					$result_array["errors"][$i]["message"] = "Email address must have the same domain as the parent account: ".$parent_domain;
					$i++;
				}
			}
		}
	}else{
		$result_array["errors"][$i]["outcome"] = true;
		$result_array["errors"][$i]["message"] = "Your current password is incorrect.";
		$i++;
	}
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Save new details
#===================================

if($_REQUEST['which_action'] == "save"){

	$pass_error = "N";
	
	$user_id = $_SESSION['contractors_username'];
	
	$do_loop = false;
	
	while($do_loop == false){
	
		$guid = str_replace("}", "", str_replace("{", "", $security->guid()));
		
		$sql = "SELECT * 
		FROM cpm_contractors_user 
		WHERE cpm_contractors_user_guid = '".$guid."'";
		
		$result = $mysql->query($sql, 'Test GUID');
		$num_rows = $mysql->num_rows($result);
		if($num_rows < 1){
			$do_loop = true;
		}
	}
	
	// Update user record
	$sql = "UPDATE cpm_contractors_user 
	SET ";
	if ($_REQUEST['old_password'] != $_REQUEST['new_password']){
		$sql .= "cpm_contractors_user_password = '".$crypt->encrypt($UTILS_DB_ENCODE, trim($_REQUEST['new_password']))."',";
	}
	$sql .= "
	cpm_contractors_user_name = '".trim($_REQUEST['name'])."', 
	cpm_contractors_user_email = '".trim($_REQUEST['email'])."'
	WHERE cpm_contractors_user_ref = '".$user_id."'";
	$pass_error = $mysql->insert($sql, 'Update Contractor');
	
	if (!is_bool($pass_error)){
		
		$datetime = new DateTime(); 
		
		$sql_insert = "INSERT INTO cpm_contractors_user_trail SET
		cpm_contractors_user_trail_login = '".$datetime->format('Y-m-d-H-i-s')."',
		cpm_contractors_user_trail_user_ref = '".$_SESSION['contractors_username']."',
		cpm_contractors_user_trail_ip = '".$_SERVER["REMOTE_ADDR"]."'";
		$has_error = $mysql->insert($sql_insert, 'Insert User trail');
		
		if ($_REQUEST['old_password'] != $_REQUEST['new_password']){
			$mail_message = 'Dear '.$_REQUEST['name'].',
				
			Update from RMG Suppliers.
				
			Your username is: '.$user_id.'
			Your password has been changed to: '.$_REQUEST['new_password'].'
			
			Please visit '.$UTILS_HTTPS_ADDRESS.'
				
				
			Kind Regards,
			
			Technical Support Team
			Residential Management Group Ltd
			
			'.$UTILS_HTTPS_ADDRESS.'
			Tel. '.$UTILS_TEL_MAIN;
				
			$sql = "INSERT INTO cpm_mailer SET
			mail_to = '".$_REQUEST['email']."',
			mail_from = 'support@rmgltd.co.uk',
			mail_subject = 'New RMG Suppliers Password',
			mail_message = '".$mail_message."'";
			
			$mysql->query($sql);
		}
		
		$_SESSION['contractors_last_login'] = $datetime->format('Y-m-d-H-i-s');
	}
	
	$result_array['results'] = $pass_error;
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Get details
#===================================

if($_REQUEST['which_action'] == "get"){
	
	$user_id = $_SESSION['contractors_username'];
	
	$sql = "SELECT * 
	FROM cpm_contractors_user 
	WHERE cpm_contractors_user_ref = '".$user_id."'";
	
	$result = $mysql->query($sql, 'Get Contractor User');
	$num_rows = $mysql->num_rows($result);

	if($num_rows > 0){
		$result_array['success'] = 'Y';
		while($row = $mysql->fetch_array($result)){
			$result_array['name'] = $row['cpm_contractors_user_name'];
			$result_array['email'] = $row['cpm_contractors_user_email'];
		}
	}else{
		$result_array['success'] = 'N';	
	}
	
	echo json_encode($result_array);
	exit;
}

$template = "backend";
$page_array = explode('/', $_SERVER['PHP_SELF']);
$page = str_replace('.php','',$page_array[count($page_array) - 1]);

$tpl = new Template($UTILS_SERVER_PATH.'includes/body.tpl');
$tpl->set('title', 'RMG Suppliers - Change Details');
$tpl->set('page_title', 'Change Details');
$tpl->set('UTILS_WEBROOT', $UTILS_WEBROOT);
$tpl->set('UTILS_LOG_PATH',$UTILS_LOG_PATH);
$tpl->set('UTILS_CLASS_PATH', $UTILS_CLASS_PATH);
$tpl->set('UTILS_URL_BASE', $UTILS_URL_BASE);
$tpl->set('UTILS_SERVER_PATH', $UTILS_SERVER_PATH);
$header = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'_header.tpl');
$content = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'.tpl');
$page_details = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$page.'.tpl');
$tpl->set('header', $header);
$tpl->set('content', $content.$page_details);
echo $tpl->fetch();
?>
