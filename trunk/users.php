<?
require("utils.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."field.class.php");
require_once($UTILS_CLASS_PATH."contractors.class.php");
require_once($UTILS_CLASS_PATH."mysql.class.php");

Global $UTILS_CLASS_PATH;
Global $UTILS_SERVER_PATH;
Global $UTILS_LOG_PATH;
Global $UTILS_WEBROOT;
Global $UTILS_URL_BASE;

$crypt = new encryption_class;
$field = new field;
$contractors = new contractors($_SESSION['contractors_qube_id']);
$mysql = new mysql();

#===================================
# Get users
#===================================

if($_REQUEST['which_action'] == "edit"){
	$user_id = $_REQUEST['user_id'];
	
	$result = $contractors->get_user($user_id);
	$num_rows = $mysql->num_rows($result);

	if($num_rows > 0){
		$result_array['success'] = 'Y';
		while($row = $mysql->fetch_array($result)){
			$result_array['name'] = $row['cpm_contractors_user_name'];
			$result_array['email'] = $row['cpm_contractors_user_email'];
			$result_array['disabled'] = strtolower($row['cpm_contractors_user_disabled']);
		}
	}else{
		$result_array['success'] = 'N';	
	}
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Get users
#===================================

if($_REQUEST['which_action'] == "get"){
	
	$result = $contractors->get_users($_SESSION['contractors_qube_id']);
	$num_rows = $mysql->num_rows($result);
	$output = "";
	$result_array = Array();
	$result_array['num_results'] = $num_rows;
	$i = 0;

	if($num_rows > 0){
		while($row = $mysql->fetch_array($result)){
			$result_array['USER_ID'][$i] = $row['cpm_contractors_user_ref'];
			$result_array['USER_ONCLICK'][$i] = 'edit_user('."'".$row['cpm_contractors_user_ref']."'".');';
			$result_array['USER_NAME'][$i] = $row['cpm_contractors_user_name'];
			$result_array['USER_USERNAME'][$i] = $row['cpm_contractors_user_ref'];
			$result_array['USER_EMAIL'][$i] = $row['cpm_contractors_user_email'];
			$i++;
		}
	}
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Check new user
#===================================

if($_REQUEST['which_action'] == "check"){
	
	$result_array = $contractors->check_user($_SESSION['contractors_qube_id'], $_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Reset Password
#===================================

if($_REQUEST['which_action'] == "reset"){
	
	$result_array = $contractors->reset_password($_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Save
#===================================

if($_REQUEST['which_action'] == "save"){
	
	$result_array = $contractors->save($_SESSION['contractors_qube_id'], $_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

$template = "backend";
$page_array = explode('/', $_SERVER['PHP_SELF']);
$page = str_replace('.php','',$page_array[count($page_array) - 1]);

$tpl = new Template($UTILS_SERVER_PATH.'includes/body.tpl');
$tpl->set('title', 'RMG Suppliers - Users');
$tpl->set('page_title', 'Users');
$tpl->set('UTILS_WEBROOT', $UTILS_WEBROOT);
$tpl->set('UTILS_LOG_PATH',$UTILS_LOG_PATH);
$tpl->set('UTILS_CLASS_PATH', $UTILS_CLASS_PATH);
$tpl->set('UTILS_URL_BASE', $UTILS_URL_BASE);
$tpl->set('UTILS_SERVER_PATH', $UTILS_SERVER_PATH);
$tpl->set('user_data', $tpl->set_sortable_table($UTILS_SERVER_PATH."templates/user_row.tpl"));
$header = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'_header.tpl');
$content = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'.tpl');
$page_details = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$page.'.tpl');
$tpl->set('header', $header);
$tpl->set('content', $content.$page_details);
echo $tpl->fetch();
?>