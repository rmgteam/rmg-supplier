<?
require("utils.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."field.class.php");
require_once($UTILS_CLASS_PATH."login.class.php");

Global $UTILS_CLASS_PATH;
Global $UTILS_SERVER_PATH;
Global $UTILS_LOG_PATH;
Global $UTILS_WEBROOT;
Global $UTILS_URL_BASE;
Global $UTILS_HTTPS_ADDRESS;

$login = new login();
if($login->logged_in() === false){
	header("Location: ".$UTILS_HTTPS_ADDRESS);
}

$crypt = new encryption_class;
$field = new field;

$err_message = "";

if($_SESSION['contractors_last_login'] == ''){
	header("Location: ".$UTILS_HTTPS_ADDRESS."change_details.php");
}

#===================================
# Check new details
#===================================

if($_REQUEST['which_action'] == "check"){
	$sql = "SELECT * 
	FROM cpm_contractors_user 
	WHERE cpm_contractors_user_ref = '".$_SESSION['contractors_username']." '
	AND cpm_contractors_user_password = '".$crypt->encrypt($UTILS_DB_ENCODE, trim($_REQUEST['old_password']))."'";
	
	$result = @mysql_query($sql);
	$password_exists = @mysql_num_rows($result);

	if($password_exists > 0){
		while($row = @mysql_fetch_array($result)){
			
			$parent = $row['cpm_contractors_user_parent'];
			
			$is_valid = true;
			$parent_domain = "notadomain";
			
			if ($field->is_valid_email(trim($_REQUEST['email'])) == false){
				$is_valid = false;
				$err_message = "&bull; Email address is not a valid email address<br />";
			}
			
			if($parent != 0){
				$sql2 = "SELECT * 
				FROM cpm_contractors_user
				WHERE cpm_contractors_user_ref='".$parent."'";	
				$result2 = @mysql_query($sql2);
				$num_rows = @mysql_num_rows($result2);
				if($num_rows > 0){
					while($row2 = @mysql_fetch_array($result2)){
						$parent_email = explode("@", $row2['cpm_contractors_user_email']);
						$parent_domain = $parent_email[1];
					}
				}
				
				if(strpos($_REQUEST['email'], $parent_domain) === false){
					$is_valid = false;
					$err_message = "&bull; Email address must have the same domain as the parent account: ".$parent_domain."<br />";
				}
			}
		}
	}else{
		$err_message = "&bull; Your current password is incorrect.<br />";	
	}
	
	$result_array['results'] = $err_message;
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Save new details
#===================================

if($_REQUEST['which_action'] == "save"){

	$pass_error = "N";
	
	// Update user record
	$sql = "UPDATE cpm_contractors_user 
	SET 
	cpm_contractors_user_password = '".$crypt->encrypt($UTILS_DB_ENCODE, trim($_REQUEST['new_password']))."', 
	cpm_contractors_user_security_q_1 = '".trim($_REQUEST['new_security_q_1'])."', 
	cpm_contractors_user_security_q_2 = '".trim($_REQUEST['new_security_q_2'])."', 
	cpm_contractors_user_security_a_1 = '".trim($_REQUEST['new_security_a_1'])."', 
	cpm_contractors_user_security_a_2 = '".trim($_REQUEST['new_security_a_2'])."',
	cpm_contractors_user_name = '".trim($_REQUEST['name'])."', 
	cpm_contractors_user_email = '".trim($_REQUEST['email'])."'
	WHERE cpm_contractors_user_ref = ".$_SESSION['contractors_username'];
	@mysql_query($sql) or $pass_error = "Y";
	
	$result_array['results'] = $pass_error;
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Get details
#===================================

if($_REQUEST['which_action'] == "get"){

	$sql = "SELECT * 
	FROM cpm_contractors_user 
	WHERE cpm_contractors_user_ref = '".$_SESSION['contractors_username']."'";
	
	$result = @mysql_query($sql);
	$num_rows = @mysql_num_rows($result);

	if($num_rows > 0){
		while($row = @mysql_fetch_array($result)){
			$result_array['name'] = $row['cpm_contractors_user_name'];
			$result_array['email'] = $row['cpm_contractors_user_email'];
			$result_array['security_q_1'] = $row['cpm_contractors_user_security_q_1'];
			$result_array['security_q_2'] = $row['cpm_contractors_user_security_q_2'];
			$result_array['security_a_1'] = $row['cpm_contractors_user_security_a_1'];
			$result_array['security_a_2'] = $row['cpm_contractors_user_security_a_2'];
		}
	}
	
	echo json_encode($result_array);
	exit;
}

$template = "backend";
$page_array = explode('/', $_SERVER['PHP_SELF']);
$page = str_replace('.php','',$page_array[count($page_array) - 1]);

$tpl = new Template($UTILS_SERVER_PATH.'includes/body.tpl');
$tpl->set('title', 'RMG Suppliers - Dashboard');
$tpl->set('page_title', 'Dashboard');
$tpl->set('UTILS_WEBROOT', $UTILS_WEBROOT);
$tpl->set('UTILS_LOG_PATH',$UTILS_LOG_PATH);
$tpl->set('UTILS_CLASS_PATH', $UTILS_CLASS_PATH);
$tpl->set('UTILS_URL_BASE', $UTILS_URL_BASE);
$tpl->set('UTILS_SERVER_PATH', $UTILS_SERVER_PATH);
$header = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'_header.tpl');
$content = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'.tpl');
$page_details = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$page.'.tpl');
$tpl->set('header', $header);
$tpl->set('content', $content.$page_details);
echo $tpl->fetch();
?>