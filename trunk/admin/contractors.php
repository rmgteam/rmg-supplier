<?
ini_set("max_execution_time","7200");
require_once("utils.php");
require_once($UTILS_CLASS_PATH."mysql.class.php");
require_once($UTILS_CLASS_PATH."contractors.class.php");
require_once($UTILS_CLASS_PATH."po.class.php");
require_once($UTILS_CLASS_PATH."login.class.php");

Global $UTILS_CLASS_PATH;
Global $UTILS_SERVER_PATH;
Global $UTILS_LOG_PATH;
Global $UTILS_WEBROOT;
Global $UTILS_URL_BASE;

$mysql = new mysql();

if ($_REQUEST['whichaction'] == 'search'){
	$contractors = new contractors('');
	$result_array = $contractors->get_list($_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'load'){
	$contractors = new contractors($_REQUEST['contractor_ref']);
	$result_array = array();
	
	$result_array['contractor_disabled'] = $contractors->contractor_disabled;
	$result_array['contractor_name'] = $contractors->contractor_name;
	$result_array['contractor_default_outercodes'] = $contractors->contractor_default_outercodes;
	
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'users'){
	$contractors = new contractors($_REQUEST['contractor_ref']);
	$result_array = $contractors->get_user_list($_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Check new user
#===================================

if($_REQUEST['whichaction'] == "check"){
	$contractors = new contractors($_REQUEST['contractor_ref']);
	$result_array = $contractors->check_user($_REQUEST['contractor_ref'], $_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Reset Password
#===================================

if($_REQUEST['whichaction'] == "reset"){
	$contractors = new contractors($_REQUEST['contractor_ref']);
	$result_array = $contractors->reset_password($_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Save user
#===================================

if($_REQUEST['whichaction'] == "save"){
	$contractors = new contractors($_REQUEST['contractor_ref']);
	$result_array = $contractors->save($_REQUEST['contractor_ref'], $_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Load PO's
#===================================

if ($_REQUEST['whichaction'] == 'load_po'){
	$po = new po();
	$output = $po->retrieve_po($_REQUEST['contractor_ref']);
	$result_array['success'] = $output;

	echo json_encode($result_array);
	exit;
}

#===================================
# Save contractor
#===================================

if($_REQUEST['whichaction'] == "save_contractor"){
	$contractors = new contractors($_REQUEST['contractor_ref']);
	$result_array = $contractors->save_contractor($_REQUEST['contractor_ref'], $_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

if($_REQUEST['whichaction'] == "edit"){
	$contractors = new contractors($_REQUEST['contractor_ref']);
	$user_id = $_REQUEST['user_id'];

	$result = $contractors->get_user($user_id);
	$num_rows = $mysql->num_rows($result);

	if($num_rows > 0){
		$result_array['success'] = 'Y';
		while($row = $mysql->fetch_array($result)){
			$result_array['name'] = $row['cpm_contractors_user_name'];
			$result_array['email'] = $row['cpm_contractors_user_email'];
			$result_array['disabled'] = strtolower($row['cpm_contractors_user_disabled']);
		}
	}else{
		$result_array['success'] = 'N';
	}

	echo json_encode($result_array);
	exit;
}

if($_REQUEST['whichaction'] == "view_account"){
	$contractors = new contractors($_REQUEST['contractor_ref']);
	$login = new login();

	$user_id = $_REQUEST['user_id'];

	$result = $contractors->get_user($user_id);
	$num_rows = $mysql->num_rows($result);

	if($num_rows > 0){
		$result_array['success'] = 'Y';
		while($row = $mysql->fetch_array($result)){
			$username = $row['cpm_contractors_user_ref'];
			$login->do_contractor_login($username, '', true);
		}
	}else{
		$result_array['success'] = 'N';
	}

	echo json_encode($result_array);
	exit;
}

if($_REQUEST['whichaction'] == "manage_services"){
	if(!empty($_REQUEST['contractor_ref'])){
		$result_array['success'] = 'Y';
		$result_array['encoded'] = base64_encode($_REQUEST['contractor_ref']);
	}else{
		$result_array['success'] = 'N';
	}

	echo json_encode($result_array);
	exit;
}
#===================================
# save default outercodes
#===================================

if($_REQUEST['whichaction'] == "save_outercodes"){
	$contractors = new contractors($_REQUEST['contractor_ref']);
	$result_array = $contractors->update_outercodes($_REQUEST);

	echo json_encode($result_array);
	exit;
}

$template = "backend";
$page_array = explode('/', $_SERVER['PHP_SELF']);
$page = str_replace('.php','',$page_array[count($page_array) - 1]);

$tpl = new Template($UTILS_SERVER_PATH.'admin/includes/body.tpl');
$tpl->set('title', 'RMG Suppliers - Contractors');
$tpl->set('page_title', 'Contractors');
$tpl->set('UTILS_WEBROOT', $UTILS_WEBROOT);
$tpl->set('UTILS_LOG_PATH',$UTILS_LOG_PATH);
$tpl->set('UTILS_CLASS_PATH', $UTILS_CLASS_PATH);
$tpl->set('UTILS_URL_BASE', $UTILS_URL_BASE);
$tpl->set('UTILS_SERVER_PATH', $UTILS_SERVER_PATH);
$tpl->set('user_name', $_SESSION['admin_user_name']);
$tpl->set('contractor_data', $tpl->set_sortable_table($UTILS_SERVER_PATH."templates/contractor_row.tpl"));
$tpl->set('user_data', $tpl->set_sortable_table($UTILS_SERVER_PATH."templates/users_row.tpl"));
$header = $tpl->get_content($UTILS_SERVER_PATH.'admin/includes/'.$template.'_header.tpl');
$content = $tpl->get_content($UTILS_SERVER_PATH.'admin/includes/'.$template.'.tpl');
$page_details = $tpl->get_content($UTILS_SERVER_PATH.'admin/includes/'.$page.'.tpl');
$tpl->set('header', $header);
$tpl->set('content', $content.$page_details);
echo $tpl->fetch();
?>	