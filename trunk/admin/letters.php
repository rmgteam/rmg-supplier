<?
ini_set("max_execution_time","7200");
Global $UTILS_CLASS_PATH;
Global $UTILS_SERVER_PATH;
Global $UTILS_LOG_PATH;
Global $UTILS_WEBROOT;
Global $UTILS_URL_BASE;
Global $UTILS_EMAIL;
Global $UTILS_PRIVATEFOLDER;

require_once("utils.php");

require_once($UTILS_CLASS_PATH."mysql.class.php");
require_once($UTILS_CLASS_PATH."contractors.class.php");
require_once($UTILS_CLASS_PATH."login.class.php");
require_once($UTILS_CLASS_PATH."mailer.class.php");

$UTILS_EMAIL = "contractors@rmguk.com"; //Being used for this instance only.

$mysql = new mysql();

if ($_REQUEST['whichaction'] == 'search'){
	$contractors = new contractors('');
	
	$request = $_REQUEST;
	$request['letters'] = 'N';
	
	$result_array = $contractors->get_list($request);
	
	echo json_encode($result_array);
	exit;
}

if($_REQUEST['whichaction'] == 'letters'){
	
	$template = new Template();
	$result_array = array();
	
	$result_array['success'] = 'N';
	
	$sql = "SELECT *
	FROM cpm_contractors
	WHERE cpm_contractors_letter_sent = 'N'
	AND cpm_contractors_pcm = 'Email'";
	
	$contractors = $_REQUEST['results_table_contractors'];
	
	$contractor_array = explode(",", $contractors);
	
	if(count($contractor_array) > 0){
	
		$sql .= "
		AND (";
	
		foreach($contractor_array as $contractor){
			if($contractor != ''){
				$sql .= "
				cpm_contractors_qube_id = '".$contractor."' OR";
			}
		}
	
		$sql = substr($sql, 0, -3) . "
		)";
	}
	$result = $mysql->query($sql, 'Get Post Contractors');
	$num_rows = $mysql->num_rows($result);
	if($num_rows > 0){	
		while(($row = $mysql->fetch_array($result)) != false){
	
			$password = '';
	
			$contractors = new contractors($row['cpm_contractors_qube_id']);
	
			$output = $contractors->new_master_user();
	
			if($output['success'] == 'Y'){
				$password = $output['password'];
				
				$text = $template->get_content($UTILS_SERVER_PATH."templates/welcome_letter_text.php", get_defined_vars());

				$file = $UTILS_PRIVATEFOLDER."RMG-Suppliers-How-To-Guide.pdf";
				$headers = 'From: ' .$UTILS_EMAIL . "\r\n";
				$headers .= 'Reply-To: ' . $UTILS_EMAIL;
				
				$email = new mailer();
				$email->set_mail_type("Welcome Email");
				$email->set_mail_to($contractors->contractor_email);
				//$email->set_mail_to("arassen.pillaysamoo@rmgltd.co.uk");
				$email->set_mail_from($UTILS_EMAIL);
				$email->set_mail_subject("Welcome to RMG Suppliers");
				//$email->set_mail_message($txt_msg);
				$email->set_mail_html($text);
				$email->set_mail_headers($headers);
				$email->add_mail_attachment($file);
				//var_dump($email->send());
				
				$email->send();
				
			}
		}
		
		$result_array['success'] = 'Y';
		
	}else{
		print($sql);
	}
	
	echo json_encode($result_array);
	exit;
}

$template = "backend";

//get page we are on - $page = letters
$page_array = explode('/', $_SERVER['PHP_SELF']);
$page = str_replace('.php','',$page_array[count($page_array) - 1]);

$tpl = new Template($UTILS_SERVER_PATH.'admin/includes/body.tpl');
$tpl->set('title', 'RMG Suppliers - Letters');
$tpl->set('page_title', 'Letters');
$tpl->set('UTILS_WEBROOT', $UTILS_WEBROOT);
$tpl->set('UTILS_LOG_PATH', $UTILS_LOG_PATH);
$tpl->set('UTILS_CLASS_PATH', $UTILS_CLASS_PATH);
$tpl->set('UTILS_URL_BASE', $UTILS_URL_BASE);
$tpl->set('UTILS_SERVER_PATH', $UTILS_SERVER_PATH);
$tpl->set('user_name', $_SESSION['admin_user_name']);
$tpl->set('contractor_data', $tpl->set_sortable_table($UTILS_SERVER_PATH."templates/letters_row.tpl"));
$tpl->set('header', $tpl->get_content($UTILS_SERVER_PATH.'admin/includes/'.$template.'_header.tpl'));

$content = $tpl->get_content($UTILS_SERVER_PATH.'admin/includes/'.$template.'.tpl');
$page_details = $tpl->get_content($UTILS_SERVER_PATH.'admin/includes/'.$page.'.tpl');
$tpl->set('content', $content.$page_details);

echo $tpl->fetch();
?>	