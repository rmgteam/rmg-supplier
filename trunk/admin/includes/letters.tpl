<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.pdfembed.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.radio.js"></script>
<script language="JavaScript" type="text/JavaScript">

	var asInitVals = new Array();
	var myApp = myApp || {}; 
	
	var pushFormData = function(sFormSelector, aoData){
	    var filter = $(sFormSelector).serializeArray();
	    for(var index in filter) {
	        aoData.push({name : filter[index]['name'], value : filter[index]['value']});
	    }
	};

	$(document).ready(function(){
	
		$('#results_table').hide();
		$('#send_button').hide();
		
		$('#gDocs').pdfEmbed({ width: 850, height: 350 });

		$("#search_term").bind('keypress',
			function(e){
				var code = (e.keyCode ? e.keyCode : e.which);
				if(code == 13){
					do_search();
				}
			}
		);
	});
	
function do_search() {
       
              $('#details_container').hide();
              $('#whichaction').val('search');

              $('#results_table_contractors').val('');
              $('#results_table_all_contractors').val('');
              $('#contractor_ref').val('');

              if($('#results_table_all').data('radio') == "true"){
                     $('#results_table_all').radio('set', '');
              }

              var sort_array = new Array();

              sort_array = [[ 0 , 'asc' ]];

              if(myApp.table === undefined){

                     $('#results_table').show();

                     myApp.table = $('#results_table')
                           .dataTable({
                                  "bJQueryUI": true,
                                  "bProcessing": true,
                             "bFilter": true,
                                   "oLanguage": {
                                         "sInfoFiltered": "",
                                         "sSearch":"Search Columns Below: "
                                   },
                                   "aoColumns": [
                                         { bSortable: false },
                                         { bSortable: true },
                                         { bSortable: true },
                                         { bSortable: true }
                                   ],
                                  "sPaginationType": "full_numbers",
                             "bServerSide": true,
                             "sAjaxSource": $(location).attr('href'),
                             "sServerMethod": "POST",
                             "fnServerData": function ( sSource, aoData, fnCallback ) {
                                   $('#a').val('search')

                                         $("#processing_dialog").dialog('open');

                                   pushFormData('#form1', aoData);

                                         var contractor_refs = $('#results_table_contractors').val();

                                         aoData.push({name : 'contractors', value : contractor_refs});

                                   $.post(sSource,
                                   aoData,
                                         function(json){
                                                $('#results_table_contractors').val(json['contractors']);
                                                $('#results_table_all_contractors').val(json['all_contractor_ids']);
                                          fnCallback(json);
                                         },
                                         "json");
                             },
                                  "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                                         $(nRow).bind('click', function(){
                                                var id = $(nRow).attr('id');
                                         });
                             },
                             "fnDrawCallback": function( oSettings ) {
                                   $("#processing_dialog").dialog('close');


                                         $('#send_button').show();

                                   if($('#results_table_all_answer').length == 0){
                                   $('#results_table_all').radio({'checked' : false});
                                   $('#results_table_all').bind('radio_click', function(e,f){

                                                       var set = f['id'];

                                  select_all('results_table');

                                   if(set == '1'){
                                         $('#results_table_contractors').val($('#results_table_all_contractors').val());
                                  }else{
                                         $('#results_table_contractors').val('');
                                  }

                                   });
                                         }

                                         $('#results_table tbody div.tickbox').each(function(){
                                                var id = $(this).closest('tr').attr('id');
                                                var checked = false;

                                                if($(this).hasClass('ticked_1') || $('#results_table_all_node_radios-1_answer').val() == '1'){
                                                       checked = true;
                                                }

                                                $(this)
                                                       .removeClass('unticked_1 ticked_1')
                                                       .attr('id', 'results_table_radio_' + id)
                                                       .on('radio_click', {'id':id}, function(e, f){
                                                              var set = f['id'];
                                                              var id = e.data['id'];
                                                              var contractors = $('#results_table_contractors').val();

                                                              if(set == '1'){
                                                                     contractors += id + ',';
                                                              }else{
                                                                     contractors = contractors.replace(id + ',', '');
                                                                     $('#results_table_all').radio('set', '');
                                                              }

                                                              $('#results_table_contractors').val(contractors);
                                          });

                                                if($('#results_table_radio_' + id  + "_answer").length == 0){
                                                       $(this).radio({'checked':checked});
                                                }
                                         });
                             },
                             "aaSorting": sort_array
                           });

                     $("#results_table tfoot input").each( function (i) {
                      asInitVals[i] = this.value;
                  } );

                  $("#results_table tfoot input").bind('focus', function () {
                      if ( this.className == "search_init" )
                      {
                          this.className = "";
                          this.value = "";
                      }
                  } );

                  $("#results_table tfoot input").bind('blur', function (i) {
                      if ( this.value == "" ){
                          this.className = "search_init";
                          this.value = asInitVals[$("#results_table tfoot input").index(this)];
                      }
                  });

                  $("#results_table tfoot input").bind('keypress', function(e){
                     var code = (e.keyCode ? e.keyCode : e.which);
                           if(code == 13){
                            myApp.table.fnFilter( this.value, $("#results_table tfoot input").index(this) );
                           }
                  });

                  $('#results_table .dataTables_filter input')
                         .unbind('keypress keyup')
                         .bind('keypress', function(e){
                            var code = (e.keyCode ? e.keyCode : e.which);
                               if (code == 13) {
                                  myApp.table.fnFilter($(this).val());
                               }
                         });


              }else{
                     myApp.table.fnSort(sort_array);
              }
       }

	
	function do_send(){
		if($('#results_table_contractors').val() != '') {
			$("#confirm_dialog").dialog({
				modal: true,
				open: function (event, ui) {
					$(".ui-dialog-titlebar-close").hide();
				},
				buttons: {
					"Yes": function () {

						$("#processing_dialog").dialog('open');
						$('#whichaction').val('letters');

						$.post($(location).attr('href'),
								$("#form1").serialize(),
								function (data) {
									$("#processing_dialog").dialog('close');

									if (data['success'] == 'Y') {
										create_letters();
									}
									$("#confirm_dialog").dialog("close");
								},
								"json");
					},
					"Cancel": function () {
						$(this).dialog("close");
					}
				}
			});
		}else{
			var a = $( document.createElement('div') )
				.attr({
					'id' :		'temp-dialog',
					'title' :	"Please Select a Contractor"
				})
				.html('Please select at least one contractor')
				.addClass('dialog_1');

			$('body').append(a);

			$("#temp-dialog").dialog({
				modal: true,
				close: function(){
					$('#temp-dialog').dialog('destroy');
					$('#temp-dialog').remove();
				}
			});

		}
	}
	
	function create_letters(){
		
		$('#gDocs').pdfEmbed('load', "/admin/letter_print.php?contractors=" + $('#results_table_contractors').val());
		
		$("#letter_dialog").dialog({
			modal: true,
			width : 900,
			height : 515,
			open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();},
			buttons:{
				"Close": function() {
					$(this).dialog("close");
				}
			}
		});
		
		$("#letter_dialog").dialog('open');
	}
		
	
</script>

<div id="letter_dialog" title="Letters" style="display:none;">
	<div style="padding:10px;overflow:hidden;">
		<div id="gDocs"></div>
	</div>
</div>

<div id="confirm_dialog" title="Are you sure?" style="display:none;">
	<div style="padding:10px;overflow:hidden;">
		<p><strong>Are you sure you want to send out letters?</strong></p>
	</div>
</div>

<div class="main">
	<form id="form1" method="post">
		<div class="content">
			<div class="search_heading">
				<h2>Search Facility</h2>
			</div>
			<div class="search_box">
				<table cellspacing="0">
					<tr>
						<td colspan="2">
							<h5>Search Term</h5>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="text" id="search_term" name="search_term" value="" />
						</td>

					</tr>
					<tr>
						<td>
							<input type="button" class="button" id="search_button" name="search_button" value="Search" onclick="do_search();" />
						</td>
						<td style="text-align: right;">
							<input type="button" class="button" id="send_button" name="send_button" value="Send Letters" onclick="do_send();" />
						</td>
					</tr>
				</table>
			</div>
			<table class="display" id="results_table" border="0" cellspacing="0" cellpadding="0">
				<?=$contractor_data ?>
			</table>
		</div>
		<input type="hidden" id="whichaction" name="whichaction" value="" />
		<input type="hidden" id="results_table_contractors" name="results_table_contractors" value="" />
		<input type="hidden" id="results_table_all_contractors" name="results_table_all_contractors" value="" />
		<input type="hidden" id="contractor_ref" name="contractor_ref" value="" />
	</form>
</div>