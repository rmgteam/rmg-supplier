<div class="header_bar">
	<div class="heading">
		<div class="exit" onclick="javascript:$(location).attr('href','/admin/index.php?a=logout');">
		</div>
	</div>
</div>
<div class="menu_bar">
	<div class="heading">
		<div class="dashboard" onclick="javascript:$(location).attr('href','/admin/home.php');">
			<h4>Dashboard</h4>
		</div>
		<div class="welcome">Hi <?=$user_name;?></div>
	</div>
</div>

<ul id="menu" class="menu">
	<li><a href="/admin/contractors.php">Contractors</a></li>
	<li><a href="/admin/po.php">Purchase Orders</a></li>
	<li><a href="/admin/letters.php">Letters</a></li>
</ul>
<script language="JavaScript" type="text/JavaScript">
	$(document).ready(function(){
		var timeout;
		$( "#menu" ).menu();
		pos_menu();
		$( "#menu" ).hide();

		$( ".main" ).bind("mouseover", function(e){
			clearTimeout(timeout);
			timeout = setTimeout(function(){
				$( "#menu" ).hide();
			}, 300);
		});
		
		$( "#menu" ).bind("mouseover", function(e){
			clearTimeout(timeout);
		});
		
		$( ".dashboard" ).bind("mouseover", function(e){
			clearTimeout(timeout);
			pos_menu();
			$( "#menu" ).show();
		});

		$('ul.menu > li:last-child').addClass('last');
	});

	function pos_menu(){
		var left = $( ".dashboard" ).offset().left;
		var top = $( ".dashboard" ).offset().top;
		var height = $( ".dashboard" ).height();
		var padding = parseInt($( ".dashboard" ).css("padding-top").replace("px", ""));
		$( "#menu" ).css({
			 "left": left + 1 + "px",
			 "top": top + height + padding + "px"
		});
		$( "#menu" ).removeClass("ui-corner-all ui-widget ui-widget-content");
		$( "#menu" ).addClass("ui-corner-bottom");
	}
</script>