<div class="main">
	<div class="content">
		<a href="/admin/contractors.php">
			<div class="dash_item" id="details">
				<div class="dash_image">
					<span class="fa-stack fa-lg">
						<i class="fa fa-wrench fa-2x"></i>
					</span>
				</div>
				<div class="dash_text">
					<div class="outer">
					    <div class="middle">
					      	<div class="inner">
								<h3>Contractors</h3>
					    	</div>
						</div>
					</div>
				</div>
			</div>
		</a>
		<a href="/admin/po.php">
			<div class="dash_item" id="orders">
				<div class="dash_image">
					<span class="fa-stack fa-lg">
					  <i class="fa fa-file-text fa-stack-2x"></i>
					</span>
				</div>
				<div class="dash_text">
					<div class="outer">
					    <div class="middle">
					      	<div class="inner">
								<h3>Purchase Orders</h3>
					    	</div>
						</div>
					</div>
				</div>
			</div>
		</a>
		<a href="/admin/letters.php">
			<div class="dash_item" id="reports">
				<div class="dash_image">
					<span class="fa-stack fa-lg">
						<i class="fa fa-envelope fa-2x"></i>
					</span>
				</div>
				<div class="dash_text">
					<div class="outer">
					    <div class="middle">
					      	<div class="inner">
								<h3>Letters</h3>
					    	</div>
						</div>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>