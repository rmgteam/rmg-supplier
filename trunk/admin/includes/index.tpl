<script type="text/javascript">
	function do_login(){
		$('#a').val("login");
				
		$("#processing_dialog").dialog('open');
		
		$.post("index.php", 
		$("#form_login").serialize(), 
		function(data){
			
			if (data['result'] == "success"){
				$(location).attr('href',data[0]['message']);
			}else{
				$("#processing_dialog").dialog('close');
			}

			var arr = new Array();
			arr.push(data[0]);
			
			show_error(arr);
		}, 
		"json");
	}
	
	function forgotten(){
	
		var arr = new Array()
		var data = {"message":"Please contact IT to reset your password","outcome":true}
		
		arr.push(data);
		
		show_error(arr);
	}

	$(document).ready(function(){
		$("#login_pass_input").bind('keypress', 
			function(e){
				var code = (e.keyCode ? e.keyCode : e.which);
				if(code == 13){ 
					do_login();
				}
			}
		);
	});
</script>
	<div class="main">
		<div class="index_box">
			<div class="login_box">
				<div class="login_header">
					<h2>Admin Login</h2>
				</div>
				<form name="form_login" id="form_login">
					<input type="hidden" id="a" name="a" />
					<div class="login_area">
						<h4>Username</h4>
						<input type="text" id="login_user_input" name="login_user_input" class="shadow2" />
						<h4>Password</h4>
						<input type="password" id="login_pass_input" name="login_pass_input" />
						<input type="button" onclick="do_login();" id="login_submit_button" class="button" value="Login" /><br />
						<a href="javascript:forgotten();">Forgotten your password?</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>