<script language="JavaScript" type="text/JavaScript" src="/library/jscript/jQuery.actual.js"></script>
<script language="JavaScript" type="text/JavaScript" src="/library/jscript/jQuery.slidedeck.js"></script>
<script language="JavaScript" type="text/JavaScript" src="/library/jscript/jQuery.radio.js"></script>
<script language="JavaScript" type="text/JavaScript" src="/library/jscript/jQuery.search_filter.js"></script>
<link href="/css/jQuery.search_filter.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/admin/css/jQuery.radio.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/css/slidedeck.skin.css" media="screen" rel="stylesheet" type="text/css" />

<?require_once('/includes/contractor_selector.php');?>
<script language="JavaScript" type="text/JavaScript">

	var asInitVals = new Array();
	var myApp = myApp || {}; 

	$(document).ready(function(){
	
		$('#details_container').hide();
		$('#results_table').hide();
		
		$("#search_term").contractor_selector();
		
		
		$('#search_filter').search_filter({
			'values':[
				{
					'id': 		'search_filter_contractor',
					'type': 	'contractor_selector',
					'value': 	'contractor',
					'html':		'Contractor'
				}
			],
			'override': true,
			'page': 'contractors.php',
			'callback': do_search,
			'type_name': 'search_filter_type'
		});
	});
	
	function load_contractor(id){
	
		$("#processing_dialog").dialog('open');
	
		$('#contractor_ref').val(id);
		$('#whichaction').val('load');
		
		$.post($(location).attr('href'),
		$("#form1").serialize(),  
		function(data){
			$("#processing_dialog").dialog('close');
		
			$('#details_container').show();
			
			if(data['contractor_disabled'] == 'True'){
				$('#chk_disabled').attr('checked', 'checked');
			}else{
				$('#chk_disabled').removeAttr('checked');
			}
			
			$('#contractor_name_span').html(data['contractor_name'] + ' Details');
			$('#contractor_outercodes').val(data['contractor_default_outercodes']);

			replace_text();
			load_users();
		},
		"json");
	}
	
	function do_save_contractor() {
	
		$("#processing_dialog").dialog('open');
		
		$('#whichaction').val('save_contractor');
		
		$.post($(location).attr('href'),
		$("#form1").serialize(),  
		function(data){
			$("#processing_dialog").dialog('close');
			
			if(data['success'] == 'Y'){
			
			}else{
			
			}
			
			load_contractor($('#contractor_ref').val());
		},
		"json");
	}

	function do_get_po() {
		$('#processing_dialog_message').html('Retrieving latest purchase orders.');
		$("#processing_dialog").dialog('open');
		$("#processing_dialog_message").show();

		$('#whichaction').val('load_po');

		$.post("contractors.php",
		$("#form1").serialize(),
		function(data){
			$("#processing_dialog_message").hide();
			$("#processing_dialog").dialog('close');
		},
		"json");
	}

	function do_outercodes(){
		$("#processing_dialog").dialog('open');

		var ad = $("#outercodes_dialog").dialog({
			modal: true,
			width: "500px",
			open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
			buttons: {
				Ok: function() {
					save_default_outercodes();
				}
			},
			autoOpen: false
		});

		ad.parent().appendTo('form:first');

		$('#whichaction').val('get_outer_codes');
		var outercodes_select = '';
		$.post("/services.php",
				$("#form1").serialize(),
				function(data){
					var data_length = Object.keys(data).length;
					var i = 1;
					var outercodes = $('#contractor_outercodes' ).val();
					var outercodes_flag = false;
					if(outercodes!='') {
						var outercodes_arr = outercodes.split(",");
						outercodes_flag = true;
					}
					$.each(data,function(val, desc){
						var selected = '';
						if(outercodes_flag) {
							if ( $.inArray( val, outercodes_arr ) != -1 ) {
								selected = ' selected="selected" ';
							}
						}
						outercodes_select += '<option value="' + val+'"'+ selected + '>' + desc + '</option>';
						if( i == data_length ) {
							$( '#outer_codes' ).html( outercodes_select );
							$( ".multiSelect" ).multiselect( {
								noneSelectedText: "Select Outercode(s)"
							} ).multiselect('refresh');
							$(".ui-selectmenu-button" ).hide();
						}else{
							i++;
						}

					});
					$("#processing_dialog").dialog('close');
				},
				"json");
		$(".ui-selectmenu-button" ).hide();
		$("#outercodes_dialog").dialog("option", "title", "Default Outercodes");
		$('#outercodes_dialog').dialog('open');
	}

	function save_default_outercodes(){
		$("#processing_dialog").dialog('open');
		$('#whichaction').val('save_outercodes');
		$('#contractor_outercodes' ).val($('#outer_codes' ).val());
		$(".ui-dialog-titlebar-close").show();

		$.post("contractors.php",
				$("#form1").serialize(),
				function(data){
					$("#processing_dialog").dialog('close');
					if(data["success"] == "Y"){
						$('#error_message').html("Successfully Saved!");
						$("#error_dialog").dialog("option", "title", "Saved!");
						$("#outercodes_dialog").dialog('close');
					}else{
						save_failed();
					}
				},
				"json");
	}

	function do_manage_services(){
		//$("#add_dialog").dialog('close');
		$("#processing_dialog").dialog('open');
		$('#whichaction').val('manage_services');

		$.post("contractors.php",
				$("#form1").serialize(),
				function(data){
					$("#processing_dialog").dialog('close');
					if(data['success'] == 'Y'){

						var url = "/services.php/"+data['encoded'];
						var windowName = "contractor_services";
						var params = [
							'height='+screen.height,
							'width='+screen.width,
							'fullscreen=yes' // only works in IE, but here for completeness
						].join(',');
						window.open(url, windowName, params);
		 }
    },
    "json");
	}
	
	function do_search() {
		var id = $("#search_filter_contractor1_input").val();
		load_contractor(id);
	}
	
	function load_users() {
		$("#processing_dialog").dialog('open');
		$('#whichaction').val('users');
		
		var sort_array = new Array();

		sort_array = [[ 0 , 'asc' ]];

		if(myApp.utable === undefined){

			$('#user_table').show();
			
			myApp.utable = $('#user_table')
				.dataTable({
					"bJQueryUI": true,
					"bProcessing": true,
			        "bFilter": false,
			       	"oLanguage": {
				       	"sInfoFiltered": "",
				       	"sSearch":"Search Columns Below: "
			       	}, 
					"sPaginationType": "full_numbers",
			        "bServerSide": true,
			        "sAjaxSource": $(location).attr('href'),
			        "sServerMethod": "POST",
			        "fnServerData": function ( sSource, aoData, fnCallback ) {
			        	$('#a').val('search')
						
						$("#processing_dialog").dialog('open');
			        	
			        	pushFormData('#form1', aoData);

			        	$.post(sSource, 
			        	aoData, 
						function(json){
			        		 fnCallback(json);
						}, 
						"json");
			        },
					"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
						$(nRow).bind('click', function(){
							var id = $(this).attr('id');
							edit_user(id);
						});
			        },
			        "fnDrawCallback": function( oSettings ) {
			        	$("#processing_dialog").dialog('close');
			        	$("#user_table_wrapper div.fg-toolbar:first").hide();
			        },
			        "aaSorting": sort_array
				});

			$("#user_table tfoot input").each( function (i) {
		        asInitVals[i] = this.value;
		    } );
		     
		    $("#user_table tfoot input").bind('focus', function () {
		        if ( this.className == "search_init" )
		        {
		            this.className = "";
		            this.value = "";
		        }
		    } );
		     
		    $("#user_table tfoot input").bind('blur', function (i) {
		        if ( this.value == "" ){
		            this.className = "search_init";
		            this.value = asInitVals[$("#user_table tfoot input").index(this)];
		        }
		    });

		    $("#user_table tfoot input").bind('keypress', function(e){
		    	var code = (e.keyCode ? e.keyCode : e.which);
				if(code == 13){ 
	        		myApp.utable.fnFilter( this.value, $("#results_table tfoot input").index(this) );
				}
		    });

		    $('#user_table .dataTables_filter input')
			    .unbind('keypress keyup')
			    .bind('keypress', function(e){
			    	var code = (e.keyCode ? e.keyCode : e.which);
				    if (code == 13) {
				    	myApp.utable.fnFilter($(this).val());
				    }
			    });
		    
			    
		}else{
			myApp.utable.fnSort(sort_array);
		}
	}
	
	function edit_user(id){
			
		$("#processing_dialog").dialog('open');
		
		var ad = $("#add_dialog").dialog({  
			modal: true,
			width: "500px",
			open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
			buttons: {
				Ok: function() {
					save();
				},
				"Reset Password": function() {
					reset_password();
				},
				"View Account": function() {
					do_view_account();
				}
			},
			autoOpen: false
		});	
		
		ad.parent().appendTo('form:first');	
		
		$('#user_id').val(id);
		$('#whichaction').val('edit');
		
		$('#name').val("");
		$('#email').val("");
		$('#disabled_row').show();
		
		$.post("contractors.php", 
		$("#form1").serialize(),
		function(data){	
			$("#processing_dialog").dialog('close');
			if(data['success'] == 'Y'){
				$('#name').val(data['name']);
				$('#email').val(data['email']);
				if(data['disabled'] == 'true'){
					$("#disabled").prop("checked", true);
				}else{
					$("#disabled").prop("checked", false);
				}
				$("#add_dialog").dialog("option", "title", "Edit User");
				$('#add_dialog').dialog('open');
			}
		}, 
		"json");
	}
		
	function save(){
		$("#processing_dialog").dialog('open');
		$('#whichaction').val('check');
		
		$.post("contractors.php", 
		$("#form1").serialize(), 
		function(data){	
			$("#processing_dialog").dialog('close');
			
			if (data.length > 0){
				var arr = new Array();
				arr.push(data[0]);
				
				show_error(arr);
			}else{
				$("#add_dialog").dialog('close');
				do_save()
			}
		}, 
		"json");
	}
	
	function do_save(){
		$("#processing_dialog").dialog('open');
		$('#whichaction').val('save');
			
		$.post("contractors.php", 
		$("#form1").serialize(), 
		function(data){	
			$("#processing_dialog").dialog('close');
			if(data["success"] == "Y"){
				load_users();
			}else{
				save_failed();
			}
		}, 
		"json");
	}

	function save_failed(){
	
		var arr = new Array()
		var data = {"message":"Apologies. We seem to have a technical fault.","outcome":true}
		
		arr.push(data);
		
		show_error(arr);
	}
		
	function reset_password(){
		$("#add_dialog").dialog('close');
		$("#processing_dialog").dialog('open');
		$('#whichaction').val('reset');
		
		$.post("contractors.php", 
		$("#form1").serialize(),
		function(data){	
			$("#processing_dialog").dialog('close');
		}, 
		"json");
	}
	
	function do_view_account(){
		$("#add_dialog").dialog('close');
		$("#processing_dialog").dialog('open');
		$('#whichaction').val('view_account');
		
		$.post("contractors.php", 
		$("#form1").serialize(),
		function(data){	
			$("#processing_dialog").dialog('close');
			if(data['success'] == 'Y'){
				var url = "/contractors.php";
				var windowName = "contractor_login";
				var params = [
				    'height='+screen.height,
				    'width='+screen.width,
				    'fullscreen=yes' // only works in IE, but here for completeness
				].join(',');
	
				window.open(url, windowName, params);
			}
		}, 
		"json");
	}
	
</script>

<div id="add_dialog" title="Add User" style="display:none;">
	<table cellspacing="0" width="400">
		<tr>
			<td width="100">
				Name
			</td>
			<td width="300">
				<input type="text" id="name" name="name" />
			</td>
		</tr>
		<tr>
			<td width="200">
				Email
			</td>
			<td width="300">
				<input type="text" id="email" name="email" />
			</td>
		</tr>
		<tr id="disabled_row">
			<td width="200">
				Disabled
			</td>
			<td width="300">
				<input type="checkbox" id="disabled" name="disabled" value="True" />
			</td>
		</tr>
	</table>
</div>


<div id="outercodes_dialog" title="Default Outercodes" style="display:none;">
	<table cellspacing="0" width="400">
		<tr>
			<td width="200" style="vertical-align:middle;">
				Outercodes
			</td>
			<td width="300">
				<select id="outer_codes" multiple="multiple" class="multiSelect">
			</td>
		</tr>
	</table>
</div>
<div class="main">
	<form id="form1" method="post">
		<div class="content">
			<div class="record_heading">
				<h2>Search Facility</h2>
			</div>
			<div id="search_filter"></div>
			<div id="details_container">
				<div class="search_heading">
					<h2 id="contractor_name_span">Details</h2>
				</div>
				<div class="search_box">
					<table cellspacing="0">
						<tr>
							<td width="30%">
								<h5>Disabled</h5>
							</td>
							<td>
								<input type="checkbox" name="chk_disabled" id="chk_disabled">
							</td>
							
						</tr>
						<tr>
							<td colspan="2">
								<input type="button" class="button" id="save_button" name="save_button" value="Save" onclick="do_save_contractor()" />
								<input type="button" class="button" id="po_button" name="po_button" value="Get Purchase Orders" onclick="do_get_po()" />
								<? if($_SESSION['admin_user_id'] == 464 || $_SESSION['admin_user_id'] == 21){ ?>
								<input type="button" class="button" id="services_button" name="services_button" value="Manage Services" onclick="do_manage_services()" />
								<input type="button" class="button" id="outercodes_button" name="outercodes_button" value="Default Outercodes" onclick="do_outercodes()" />
								<? }?>
							</td>
						</tr>
					</table>
				</div>
				<div class="search_heading">
					<h2>Users</h2>
				</div>
				<table class="display" id="user_table" border="0" cellspacing="0" cellpadding="0">
					<?=$user_data ?>
				</table>
			</div>
		</div>
		<input type="hidden" id="whichaction" name="whichaction" value="" />
		<input type="hidden" id="contractor_ref" name="contractor_ref" value="" />
		<input type="hidden" id="user_id" name="user_id" value="" />
		<input type="hidden" id="contractor_outercodes" name="contractor_outercodes" value="" />
	</form>
</div>