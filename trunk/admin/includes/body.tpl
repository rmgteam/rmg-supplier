<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title><?=$title?></title>
		
		<meta http-equiv="X-UA-Compatible" content="chrome=1">
		
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/reset.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/custom-theme/jquery-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/jquery.datatable.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/jquery.qtip.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>admin/css/main.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/selectors.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/jquery.radio.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/jquery.multiselect.css" />

		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery-1.8.3.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery-ui-1.11.2.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/cufon.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/fonts/gotham.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/common.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.tablesorter.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.tablesorter.widgets.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.datatables.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.qtip.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery-build_tooltip.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.multiselect.min.js"></script>
		<script>
			var check_session;

			$(document).ready(function(){
				check_session = setInterval(CheckForSession, 5000);
			});

			function CheckForSession() {
				var str="chksession=true";
				jQuery.ajax({
					type: "POST",
					url: "/chk_session.php",
					data: str,
					cache: false,
					success: function(res){
						if(res == "1") {

							var locationArray = ['forgot_details.php', 'index.php', 'admin/index.php'];

							if($.inArray(location.pathname.substring(1), locationArray) < 0) {

								var errorDialog = $(document.createElement('div'))
										.attr({
											'id': 'alert',
											'title': "Session timeout..."
										})
										.addClass('dialog_1');

								var p = $(document.createElement('p'))
										.text('Your session has timed out!')
										.appendTo(errorDialog);

								$('body').append(errorDialog);

								$("#alert").dialog({
									modal: true,
									width: "700px",
									resizable: false,
									open: function (event, ui) {
										$(".ui-dialog-titlebar-close").hide();
									},
									buttons: {
										Ok: function () {
											window.location.href = '/index.php';
										}
									}
								});
							}
						}
					}
				});
			}
		</script>
	</head>
	<body>
		<div class="wrapper">
			<? require_once($UTILS_SERVER_PATH."templates/no_results_row.tpl");?>
			<?=$header;?>
			<?=$content;?>
		</div>
	</body>
</html>