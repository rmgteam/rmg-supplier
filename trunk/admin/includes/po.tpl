<script language="JavaScript" type="text/JavaScript" src="/library/jscript/jQuery.actual.js"></script>
<script language="JavaScript" type="text/JavaScript" src="/library/jscript/jQuery.radio.js"></script>
<script language="JavaScript" type="text/JavaScript" src="/library/jscript/jQuery.search_filter.js"></script>
<script language="JavaScript" type="text/JavaScript" src="/library/jscript/jQuery.slidedeck.js"></script>
<script language="JavaScript" type="text/JavaScript" src="/library/jscript/jQuery.iframe-transport.js"></script>
<script language="JavaScript" type="text/JavaScript" src="/library/jscript/jQuery.multiFileUpload.js"></script>
<link href="/css/jQuery.search_filter.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/css/multiFileUpload.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/css/slidedeck.skin.css" media="screen" rel="stylesheet" type="text/css" />

<?require_once('/includes/contractor_selector.php');?>
<?require_once('/includes/rmc_selector.php');?>
<script language="JavaScript" type="text/JavaScript">

	var asInitVals = new Array();
	var myApp = myApp || {}; 
	
	var pushFormData = function(sFormSelector, aoData){
	    var filter = $(sFormSelector).serializeArray();
	    for(var index in filter) {
	        aoData.push({name : filter[index]['name'], value : filter[index]['value']});
	    }
	};

	$(document).ready(function(){

		/*$('#titleSearch' ).on('click', function(){
			$("#processing_dialog").dialog('open');
			$.post($(location).attr('href'),
				{job_id: $('#job_id').val(), whichaction: 'resend_po'},
				function(data){
					$("#processing_dialog").dialog('close');
				},
				"json");
		});*/

		var ad = $("#dialog_alert").dialog({  
			modal: true,
			width: "700px",
			resizable: false,
			open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
			buttons: {
				Cancel: function() {
					$( this ).dialog( "close" );
				},
				Ok: function() {
					do_complete_job();
				}
			},
			autoOpen: false
		});

		ad.parent().appendTo('#form1');

		var aj = $("#job_dialog").dialog({
			modal: true,
			width: "900px",
			resizable: false,
			open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
			buttons: {
				'Upload Images': function() {
					$( this ).dialog( "close" );
				},
				'Download PO': function() {
					do_download();
				},
				'Download Asbestos Report': function() {
					download_report();
				}
			},
			autoOpen: false
		});

		aj.parent().appendTo('#form1');

		var aj = $("#note_dialog").dialog({
			modal: true,
			width: "900px",
			resizable: false,
			open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
			buttons: {
			},
			autoOpen: false
		});

		aj.parent().appendTo('#form1');
		
		$('#results_table').hide();
		
		$('#search_filter').search_filter({
			'values':[
				{
					'id': 		'search_filter_closed',
					'type': 	'radio',
					'params':	 {
						'value' : [{
							'name'	:	'ID',
							'multi'	:	false,
							'id'	:	'id',
							'answers' : [
								{
									'name'	:	'Yes',
									'value'	:	'True'
								},
								{
									'name'	:	'No',
									'value'	:	'False',
									'default': true
								},
								{
									'name'	:	'Awaiting PM',
									'value'	:	'Hold'
								},
								{
									'name'	:	'Either',
									'value'	:	''
								}
							]
						}]
					},
					'value': 	'closed',
					'html':		'Closed?'
				},
				{
					'id': 		'search_filter_contractor',
					'type': 	'contractor_selector',
					'value': 	'contractor',
					'html':		'Contractor(s)',
					'params':	{
						'multi_select':		true
					}
				},
				{
					'id': 		'search_filter_property',
					'type': 	'rmc_selector',
					'value': 	'property',
					'html':		'Properties',
					'params':	{
						'multi_select':		true
					}
				},
				{
					'id': 		'search_filter_key_input',
					'type': 	'input',
					'value': 	'key',
					'html':		'PO Description/PO Ref/Job Ref'
				}
			],
			'override': true,
			'page': 'po.php',
			'callback': do_search,
			'type_name': 'search_filter_type'
		});

		$("#estimated_date").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true });

		$("#completed").on( "selectmenuchange", function(){
			is_complete();
		});

		$('#reason_id').on( "selectmenuchange", function(){
			is_misc();
		});

		$('#job_dialog').parent().find('.ui-dialog-buttonpane').find('button').eq(0).multiFileUpload({
			ajax: {
				'url': 'http://webservice.intranet.rmgltd.co.uk/webservice.php',
				data: {
					'class': 'qube',
					'method': 'transfer_file',
					'username': 'qube',
					'password': 'A78941R',
					'order_no': $('#order_no'),
					'subsidiary': $('#subsidiary'),
					'year': $('#year')
				}
			},
			completeCallback: function(){
				var aoData = {
					'order_no': $('#order_no').val(),
					'whichaction': 'image_update'
				};

				$.post($(location).attr('href').split('?')[0],
				aoData,
				function(json){
					do_search();
				},
				"json");
			},
			mode: 'popup',
			accept: 'image/*'
		});


		$('#search_filter_contractor1, #search_filter_contractor2, #search_filter_contractor3, #search_filter_contractor4').on('contractor_selectorset', function(){
			load_pos($('#' + $(this ).attr('id') + '_input' ).val());
		});
	});

	function complete_job(id, days) {
		$('#dialog_alert').dialog('open');
		$('#to_complete').val(id);
		$("#reason_id").get(0).selectedIndex = 0;

		$("#completed").val($("#completed option:first").val());
		$("#completed").selectmenu('refresh');

		$('#completed_input').val('');
		$("#estimated_date").val('');
		$('#advice').val('');
		$('#info').val('');
		$('#reason').val('');
		$('#poDays').val(days);
		is_complete();
	}

	function do_complete_job() {
		$("#processing_dialog").dialog('open');

		$('#whichaction').val('complete_po');

		$.post("po.php",
		$("#form1").serialize(),
		function(data){
			$("#processing_dialog").dialog('close');
			if(data['results'] == "N"){
				$("#dialog_alert").dialog('close');
				$("#job_dialog").dialog('close');
				do_search();
			}else{
				alert(data['results']);
			}
		},
		"json");
	}

	function is_complete(){

		$('#complete_row_1').hide();
		$('#complete_row_2').hide();
		$('#complete_advice').hide();
		$('#update_row_1').hide();
		$('#update_row_2').hide();
		$("#estimated_date").datepicker('destroy');

		var days = $('#poDays').val();

		if($("#completed").val() == "Yes"){
			$('#complete_advice').show();
			$('#update_row_1').show();
			$('#update_row_1').find('td:first').html('Completion Date');
			$("#estimated_date").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true });
		}else if($("#completed").val() == "No"){
			$('#complete_row_1').show();
		}else{
			$('#update_row_1').show();
			$('#update_row_2').show();
			$('#update_row_1').find('td:first').html('Estimated Completion Date');
			$("#estimated_date").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true });
		}
	}

	function do_download(){
		var id = $('#poId').val();

		window.open('/po_download.php?poid=' + id);
	}

	function download_report(){
		var id = $('#poId').val();

		window.open('/asbestos_download.php?poid=' + id);
	}

	function expand(id){
		$('#whichaction').val('expand');

		$('#poId').val(id);

		var sort_array = new Array();

		sort_array = [[ 0 , 'desc' ]];

		$("#job_dialog").dialog('open');

		$('#job_results_div_table').show();

		myApp.ctable = $('#job_results_div_table').dataTable({
			"bJQueryUI": true,
			"bProcessing": true,
			"bDestroy": true,
			"bFilter": false,
			"bLengthChange": false,
			"oLanguage": {
				"sInfoFiltered": "",
				"sSearch":"Search Columns Below: "
			},
			"sPaginationType": "full_numbers",
			"bServerSide": true,
			"sAjaxSource": $(location).attr('href'),
			"sServerMethod": "POST",
			"fnServerData": function ( sSource, aoData, fnCallback ) {

				$("#processing_dialog").dialog('open');

				aoData.push({name : "expand_id", value : id});
				aoData.push({name : "whichaction", value : "expand"});

				$.post(sSource,
				aoData,
				function(json){
					$('#order_no').val(json['extra']['order_no']);
					$('#year').val(json['extra']['year']);
					$('#subsidiary').val(json['extra']['subsidiary']);
					$('#tif_span' ).hide();
					$('#nr_span' ).hide();
					$('#no_span' ).hide();
					if( json['extra']['asbestos'] == true ){
						$('#job_dialog' ).parent().find('.ui-dialog-buttonpane .ui-button:nth-child(3)').show();
					}else{
						$('#job_dialog').parent().find('.ui-dialog-buttonpane  .ui-button:nth-child(3)').hide();
						if( json['extra']['asbestos'] == 'tif' ){
							$('#tif_span' ).show();
						}else if(json['extra']['asbestos'] == 'NR' ){
							$('#nr_span' ).show();
						}else{
							$('#no_span' ).show();
						}
					}
					fnCallback(json);
				},
				"json");
			},
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			},
			"fnDrawCallback": function( oSettings ) {
				$("#processing_dialog").dialog('close');

				$('#job_results_div_table_wrapper .fg-toolbar.ui-toolbar.ui-widget-header.ui-corner-tl').hide();
				$(".ui-dialog-titlebar-close").show();
			},
			"aaSorting": sort_array
		});

	}

	function get_note(id){
		$('#whichaction').val('notes');

		var sort_array = new Array();

		sort_array = [[ 1 , 'desc' ]];

		$("#note_dialog").dialog('open');

		$('#note_results_div_table').show();

		myApp.ntable = $('#note_results_div_table').dataTable({
			"bJQueryUI": true,
			"bProcessing": true,
			"bDestroy": true,
			"bFilter": false,
			"bLengthChange": false,
			"oLanguage": {
				"sInfoFiltered": "",
				"sSearch":"Search Columns Below: "
			},
			"sPaginationType": "full_numbers",
			"bServerSide": true,
			"sAjaxSource": $(location).attr('href'),
			"sServerMethod": "POST",
			"fnServerData": function ( sSource, aoData, fnCallback ) {

				$("#processing_dialog").dialog('open');

				aoData.push({name : "po_id", value : id});
				aoData.push({name : "whichaction", value : "notes"});

				$.post(sSource,
						aoData,
						function(json){
							fnCallback(json);
						},
						"json");
			},
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			},
			"fnDrawCallback": function( oSettings ) {
				$("#processing_dialog").dialog('close');

				$('#note_results_div_table_wrapper .fg-toolbar.ui-toolbar.ui-widget-header.ui-corner-tl').hide();
				$(".ui-dialog-titlebar-close").show();
			},
			"aaSorting": sort_array
		});

	}

	function is_misc(){
		if($("#reason_id").val() != "5"){
			$('#complete_row_2').hide();
		}else{
			$('#complete_row_2').show();
		}
	}

	function load_pos(id) {
		if(id.split(',').length == 1 ) {
			$('#processing_dialog_message').html('Retrieving latest purchase orders.');
			$("#processing_dialog").dialog('open');
			$("#processing_dialog_message").show();

			$('#whichaction').val('load');

			$.post("po.php",
					{'whichaction': 'load', 'contractors_qube_id': id},
					function (data) {
						$("#processing_dialog_message").hide();
						$("#processing_dialog").dialog('close');

						do_search();
					},
					"json");
		}else{
			do_search();
		}
	}

	function do_search() {
		$("#processing_dialog").dialog('open');
		$('#whichaction').val('search');

		var sort_array = new Array();

		sort_array = [[ 6 , 'desc' ]];

		if(myApp.table === undefined){

			$('#results_table').show();

			myApp.table = $('#results_table')
			.dataTable({
				"bJQueryUI": true,
				"bProcessing": true,
				"bFilter": true,
				"oLanguage": {
					"sInfoFiltered": "",
					"sSearch":"Search Columns Below: "
				},
				"bPaginate": false,
				"sPaginationType": "full_numbers",
				"bServerSide": true,
				"sAjaxSource": $(location).attr('href'),
				"sServerMethod": "POST",
				"fnServerData": function ( sSource, aoData, fnCallback ) {

					$("#processing_dialog").dialog('open');
		        	$('#whichaction').val('search')
					
					$("#processing_dialog").dialog('open');
					var disabledArray = [];

					$('#form1').find(':disabled').each(function(){
						disabledArray.push($(this));
						$(this).removeAttr('disabled');
					});
		        	pushFormData('#form1', aoData);
					$.each(disabledArray, function(key, value){
						value.attr('disabled', 'disabled');
					});
			        aoData.push({name : 'no_fields', value : $('#search_filter').search_filter('get_no_fields')});
					aoData.push({name : "whichaction", value : "search"});

		        	$.post(sSource, 
		        	aoData, 
					function(json){
		        		 fnCallback(json);
					}, 
					"json");
		        },
				"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					var no_tds = 0;

					var id = $(nRow).attr('id');

					$(nRow).find('td').each(function(){
						if(no_tds != 1){
							$(this).bind('click', { 'id' : id}, function(e){
								var id = e.data['id'];
								expand(id);
							});
						}

						no_tds++;
					});
				},
		        "fnDrawCallback": function( oSettings ) {
		        	$("#processing_dialog").dialog('close');
		        },
		        "aaSorting": sort_array
			});


			$("#results_table tfoot input").each( function (i) {
		        asInitVals[i] = this.value;
		    } );
		     
		    $("tfoot input").bind('focus', function () {
		        if ( this.className == "search_init" )
		        {
		            this.className = "";
		            this.value = "";
		        }
		    } );
		     
		    $("tfoot input").bind('blur', function (i) {
		        if ( this.value == "" ){
		            this.className = "search_init";
		            this.value = asInitVals[$("#results_table tfoot input").index(this)];
		        }
		    });

		    $("tfoot input").bind('keypress', function(e){
		    	var code = (e.keyCode ? e.keyCode : e.which);
				if(code == 13){ 
	        		myApp.table.fnFilter( this.value, $("#results_table tfoot input").index(this) );
				}
		    });

		    $('.dataTables_filter input')
			    .unbind('keypress keyup')
			    .bind('keypress', function(e){
			    	var code = (e.keyCode ? e.keyCode : e.which);
				    if (code == 13) {
				    	myApp.table.fnFilter($(this).val());
				    }
			    });
		    
			    
		}else{
			myApp.table.fnSort(sort_array);
		}
	}
</script>
<input type="hidden" id="poId" name="poId" />
<input type="hidden" id="order_no" name="order_no" />
<input type="hidden" id="subsidiary" name="subsidiary" />
<input type="hidden" id="year" name="year" />

<input type="hidden" id="poId" name="poId" />
<div id="dialog_alert" title="Complete Job" class="dialog_1">
	<input type="hidden" id="poDays" name="poDays" />
	<table cellspacing="0" width="600">
		<tr>
			<td width="100">
				Status
			</td>
			<td width="300">
				<select id="completed" name="completed">
					<option value="">Update</option>
					<option value="Yes">Completed</option>
					<option value="No">Cancelled</option>
					<option value="Hold">Awaiting PM</option>
				</select>
			</td>
		</tr>
		<tr id="complete_advice">
			<td width="200">
				Advice
			</td>
			<td width="300">
				<input type="text" id="advice" name="advice" maxlength="200" />
			</td>
		</tr>
		<tr id="complete_row_1">
			<td width="200">
				Reason
			</td>
			<td width="300">
				<select id="reason_id" name="reason_id">
					<?php load_reasons(); ?>
				</select>
			</td>
		</tr>
		<tr id="complete_row_2">
			<td width="200">
				Reason
			</td>
			<td width="300">
				<input type="text" id="reason" name="reason" />
			</td>
		</tr>
		<tr id="update_row_1">
			<td width="200">
				Estimated Completed Date
			</td>
			<td width="300">
				<input type="text" id="estimated_date" name="estimated_date" readonly="readonly" />
			</td>
		</tr>
		<tr id="update_row_2">
			<td width="200">
				Information
			</td>
			<td width="300">
				<input type="text" id="info" name="info" maxlength="200" />
			</td>
		</tr>
	</table>
</div>
<div id="job_dialog" title="Jobs" class="dialog_1">
	<table class="display" id="job_results_div_table" border="0" cellspacing="0" cellpadding="0">
		<?=$job_data ?>
	</table>
	<span id="tif_span" style="display: none;">Please contact the contractors team on 0345 002 4229 to obtain a copy of this property's asbestos report.</span>
	<span id="nr_span" style="display: none;">Asbestos reports are not applicable for this property.</span>
	<span id="no_span" style="display: none;">The asbestos report for this property is currently unavailable please contact the contractors team on 0345 002 4229 to obtain more information.</span>
</div>
<div id="note_dialog" title="Notes" class="dialog_1">
	<table class="display" id="note_results_div_table" border="0" cellspacing="0" cellpadding="0">
		<?=$note_data ?>
	</table>
</div>
<div class="main">
	<form id="form1" method="post">
			<div class="record_heading">
				<h2 id="titleSearch">Search Facility</h2>
			</div>
			<div id="search_filter"></div>
			<table class="display" id="results_table" border="0" cellspacing="0" cellpadding="0">
				<?=$po_data ?>
			</table>
		</div>
		<input type="hidden" id="whichaction" name="whichaction" value="" />
		<input type="hidden" id="to_complete" name="to_complete" value="" />
		<input type="hidden" id="job_id" name="job_id" value="36741" />
	</form>
</div>