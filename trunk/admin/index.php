<?php

require("utils.php");
require_once($UTILS_CLASS_PATH."login.class.php");
$login = new login;

Global $UTILS_CLASS_PATH;
Global $UTILS_SERVER_PATH;
Global $UTILS_LOG_PATH;
Global $UTILS_WEBROOT;
Global $UTILS_URL_BASE;
Global $UTILS_HTTPS_ADDRESS;

//================================================
// Log off routine
//================================================
if($_REQUEST['a'] == "logout"){
	
	$login->do_logout();
	
	header("Location: ".$UTILS_HTTPS_ADDRESS."admin/index.php");
	exit;
}

$site_inactive = "N";
if( $site_inactive == "Y" ){
	require_once($UTILS_FILE_PATH."holding_page.php");
	exit;
}

//================================================
// Log in routine
//================================================
if($_REQUEST['a'] == "login"){
	
	$result_array = array();

	if($_REQUEST['login_user_input'] == "" || $_REQUEST['login_pass_input'] == ""){
		$result_array[0]['message'] = "Please enter both of your login credentials";
		$result_array[0]['outcome'] = true;
		$result_array['result'] = 'fail';
	}
	else{
		$r_array = $login->do_admin_login($_REQUEST['login_user_input'], $_REQUEST['login_pass_input']);
		
		if( $r_array['outcome'] === false ){
			$result_array['result'] = 'success';
			$r_array['message'] = $UTILS_HTTPS_ADDRESS."admin/home.php";
		}
		else{
			$result_array['result'] = 'fail';
			if( $r_array['message'] == "" ){
				$r_array['message'] = "Incorrect login details";
			}
		}
		$result_array[0] = $r_array;
	}
	
	echo json_encode($result_array);
	exit;
}

$template = "backend";
$page = str_replace('.php','',basename($_SERVER['PHP_SELF']));

$tpl = new Template($UTILS_SERVER_PATH.'admin/includes/body.tpl');
$tpl->set('title', 'RMG Suppliers - Admin');
$tpl->set('UTILS_WEBROOT', $UTILS_WEBROOT);
$tpl->set('UTILS_LOG_PATH',$UTILS_LOG_PATH);
$tpl->set('UTILS_CLASS_PATH', $UTILS_CLASS_PATH);
$tpl->set('UTILS_URL_BASE', $UTILS_URL_BASE);
$tpl->set('UTILS_SERVER_PATH', $UTILS_SERVER_PATH);
$header = $tpl->get_content($UTILS_SERVER_PATH.'admin/includes/'.$page.'_header.tpl');
$content = $tpl->get_content($UTILS_SERVER_PATH.'admin/includes/'.$page.'.tpl');
$tpl->set('header', $header);
$tpl->set('content', $content);
echo $tpl->fetch();

?>