<?php
require_once("utils.php");
require_once($UTILS_CLASS_PATH."letter.class.php");
require_once($UTILS_CLASS_PATH."contractors.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."template.class.php");
require_once($UTILS_CLASS_PATH."pdf/form_to_pdf.class.php");

GLOBAL $UTILS_TEL_MAIN;
$mysql = new mysql;
$security = new security;
$template = new Template;
$data = new data;
$pdf = new form_to_pdf();

Global $UTILS_SERVER_PATH;
		
$date_today = $data->ymd_to_date($data->today_to_ymd());

$sql = "SELECT * 
FROM cpm_contractors
WHERE cpm_contractors_letter_sent = 'N'
AND cpm_contractors_pcm = 'Post'";

$contractors = $_REQUEST['contractors'];

$contractor_array = explode(",", $contractors);

if(count($contractor_array) > 0){
	
	$sql .= "
	AND (";
	
	foreach($contractor_array as $contractor){
		if($contractor != ''){
			$sql .= "
			cpm_contractors_qube_id = '".$contractor."' OR";
		}
	}
	
	$sql = substr($sql, 0, -3) . "
	)";
}

$result = $mysql->query($sql, 'Get Post Contractors');
$num_rows = $mysql->num_rows($result);
if($num_rows > 0){

	$letter = new letter('P', 'mm', 'A4');
	$letter->SetDefaultFont('Arial', '', 10);
	$letter->setStyle("b","Arial","B");
	$letter->setStyle("i","Arial","I");
	$letter->bMargin = 15;
	$letter->header_template = "headed_paper";
	$letter->line_height = 4.5;
	$letter->header_email = 'customerservice@rmguk.com';
	$letter->header_phone = $UTILS_TEL_MAIN;
		
	$letter->include_header = true;
	$letter->SetMargins(12.7, 0, 12.7);
	
	while($row = $mysql->fetch_array($result)){
		
		$password = '';
		
		$contractors = new contractors($row['cpm_contractors_qube_id']);
		
		//$output = $contractors->new_master_user();
		
		//if($output['success'] == 'Y'){
			//$password = $output['password'];
			
			$password = 'asdasd';
		
			$letter->add_page();
			
			$address = explode("\r\n", $contractors->contractor_address);
			foreach($address as $line){
				if($line == ''){
					unset($line);
				}
			}
		
			$letter->parse_html("<p>".$contractors->contractor_name."</p>");
			$letter->add_letter_address($address);
			$letter->parse_html('<p>'.$date_today."<br /><br /></p>");
			$letter->add_salutation($contractors->contractor_name);
		
			$text = $template->get_content($UTILS_SERVER_PATH."templates/welcome_letter_text.php", get_defined_vars());
			
			$text = $pdf->convert_html($text);
			
			$letter->parse_html($text);
			$letter->add_closing("", "Yours sincerely,", "");
			$letter->parse_html("<p><br />Residential Management Group Ltd<br /></p>");
		/*}else{
			$letter->add_page();
			
			$letter->parse_html("<p>".$contractors->contractor_name." - creation failed</p>");
		}*/
	}

	$letter->Output();
}else{
	print("There are no letters to print.");	
}
?>