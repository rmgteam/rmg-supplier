<?
ini_set("max_execution_time","7200");
require_once("utils.php");
require_once($UTILS_CLASS_PATH."po.class.php");
require_once($UTILS_CLASS_PATH."mysql.class.php");
require_once($UTILS_CLASS_PATH."contractors.class.php");


Global $UTILS_CLASS_PATH;
Global $UTILS_SERVER_PATH;
Global $UTILS_LOG_PATH;
Global $UTILS_WEBROOT;
Global $UTILS_URL_BASE;
Global $conn;

unset($_SESSION['contractors_qube_id']);

$mysql = new mysql();

function load_reasons(){
	Global $conn;

	$sql = "SELECT *
	FROM cpm_po_reason
	ORDER BY cpm_po_reason_text";

	$options = "";
	$result = mysql_query($sql, $conn);
	$num_rows = @mysql_num_rows($result);
	if($num_rows > 0){
		$options .= '<option value=""> - Please Select - </option>';
		while($row = @mysql_fetch_array($result)){
			$options .= '<option value="'.$row['cpm_po_reason_id'].'">'.$row['cpm_po_reason_text'].'</option>';
		}
	}
	echo $options;
}

if ($_REQUEST['whichaction'] == 'resend_po'){
	$po = new po;
	$result_array = $po->complete_job($_REQUEST['job_id']);
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'load'){
	$po = new po();
	$output = $po->retrieve_po($_REQUEST['contractors_qube_id']);
	$result_array['success'] = $output;

	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'complete_po'){
	$data = new data;

	$has_error = "N";

	if ($_REQUEST['reason_id'] == ""){
		if($_REQUEST['completed'] == "No"){
			$has_error = "Please select a reason";
		}
	}elseif ($_REQUEST['reason_id'] == "4"){
		if($_REQUEST['reason'] == ""){
			$has_error = "Please enter a reason";
		}
	}

	if($_REQUEST['completed'] == ''){
		if($_REQUEST['estimated_date'] == ""){
			$has_error = "Please enter a date";
		}
		if($_REQUEST['info'] == ""){
			$has_error = "Please enter some advice";
		}
	}

	if( $has_error == "N"){
		$datetime = new DateTime();

		$status = 'False';
		$sql_middle = '';

		if($_REQUEST['completed'] != ''){
			$sql_middle = ",
			cpm_po_job_estimated_completion_date = '".$data->date_to_ymd($_REQUEST['estimated_date'])."'";

			if($_REQUEST['completed'] != 'Hold') {
				$status = 'True';
				$sql_middle .= ",
				cpm_po_job_advice = '".preg_replace("/[^A-Za-z0-9 ]/i","",$_REQUEST['advice'])."'";
			}else{
				$status = 'Hold';
				$sql_middle .= ",
				cpm_po_job_advice = '".preg_replace("/[^A-Za-z0-9 ]/i","",$_REQUEST['info'])."'";
			}
		}else{
			$sql_middle = ",
			cpm_po_job_estimated_completion_date = '".$data->date_to_ymd($_REQUEST['estimated_date'])."',
			cpm_po_job_advice = '".preg_replace("/[^A-Za-z0-9 ]/i","",$_REQUEST['info'])."'";
		}

		$description = '';
		$property = '';
		$po_num = '';
		$job = '';
		$reason = '';
		$reason_detail = '';
		$advice = '';

		$sql = "SELECT *
		FROM cpm_po_job
		WHERE cpm_po_job_id = '".$_REQUEST['to_complete']."'";
		$result = mysql_query($sql, $conn);
		$num_rows = mysql_num_rows($result);
		if($num_rows > 0){
			$row = @mysql_fetch_array($result);

			$sql = "UPDATE cpm_po_job SET cpm_po_job_complete = '".$status."',
			cpm_po_job_ts = '".$datetime->format('Ymd')."',
			cpm_po_job_reason = '".$_REQUEST['reason']."',
			cpm_po_job_reason_id = '".$_REQUEST['reason_id']."',
			cpm_po_job_user_ref = '".$_SESSION['admin_user_id']."'".$sql_middle."
			WHERE cpm_po_job_id = '".$_REQUEST['to_complete']."'";
			mysql_query($sql, $conn) or $has_error = $sql;

			$mainSQL = $sql;

			$sql = "SELECT *
			FROM cpm_po_job j
			INNER JOIN cpm_po p ON j.cpm_po_job_po_id = p.cpm_po_id
			INNER JOIN cpm_subsidiary s ON s.subsidiary_code = p.cpm_po_brand
			INNER JOIN cpm_lookup_rmcs l  ON l.rmc_ref = p.cpm_po_rmc_id
			INNER JOIN cpm_rmcs r  ON r.rmc_num = l.rmc_lookup AND s.subsidiary_id = r.subsidiary_id
			LEFT JOIN cpm_po_reason f ON f.cpm_po_reason_id = j.cpm_po_job_reason_id
			WHERE cpm_po_job_id = '".$_REQUEST['to_complete']."'";
			$result = mysql_query($sql, $conn);
			$num_rows = @mysql_num_rows($result);
			if($num_rows > 0){
				$row = @mysql_fetch_array($result);
				$description = $row['cpm_po_description'];
				$property = $row['rmc_name'];
				$po_num = $row['cpm_po_number'];
				$po_id = $row['cpm_po_id'];
				$job = $row['cpm_po_job_no'];
				$reason = $row['cpm_po_reason_text'];
				$reason_detail = $row['cpm_po_job_reason'];
				$advice = $row['cpm_po_job_advice'];
			}

			$sql = "INSERT INTO cpm_po_job_audit SET
			cpm_po_job_reason_id = '".$row['cpm_po_job_reason_id']."',
			cpm_po_job_reason = '".$row['cpm_po_job_reason']."',
			cpm_po_job_ts = '".$row['cpm_po_job_ts']."',
			cpm_po_job_user_ref = '".$row['cpm_po_job_user_ref']."',
			cpm_po_job_unique = '".$row['cpm_po_job_unique']."',
			cpm_po_job_sequence = '".$row['cpm_po_job_sequence']."',
			cpm_po_job_remind = '".$row['cpm_po_job_remind']."',
			cpm_po_job_remind_no = '".$row['cpm_po_job_remind_no']."',
			cpm_po_job_status = '".$row['cpm_po_job_status']."',
			cpm_po_job_status_int = '".$row['cpm_po_job_status_int']."',
			cpm_po_job_no = '".$row['cpm_po_job_no']."',
			cpm_po_job_amount = '".$row['cpm_po_job_amount']."',
			cpm_po_job_completion_date = '".$row['cpm_po_job_completion_date']."',
			cpm_po_job_estimated_completion_date = '".$row['cpm_po_job_estimated_completion_date']."',
			cpm_po_job_po_id = '".$row['cpm_po_job_po_id']."',
			cpm_po_job_complete = '".$row['cpm_po_job_complete']."',
			cpm_po_job_advice = '".$row['cpm_po_job_advice']."',
			cpm_po_job_parent_id = '".$_REQUEST['to_complete']."',
			cpm_po_job_user_id = '".$_SESSION['admin_user_id']."',
			last_update = '".time()."'";
			mysql_query($sql, $conn) or $has_error = $sql;

			$po = new po;
			$email = '';

			$result_array = $po->complete_job($_REQUEST['to_complete']);
			$email = $result_array['result'];

			if($_REQUEST['reason'] != '' || $_REQUEST['advice'] != ''){
				if(strpos($email, 'Error code') === false && strpos($email, 'XML is invalid') === false){
					Global $UTILS_INTRANET_DB_LINK;
					$sql = "SELECT personnel_email
					FROM personnel
					WHERE personnel_username = '".$email."'";
					@mysql_select_db("intranet", $UTILS_INTRANET_DB_LINK);
					$result = @mysql_query($sql, $UTILS_INTRANET_DB_LINK);
					$num_rows = @mysql_num_rows($result);
					if($num_rows > 0){
						$row = @mysql_fetch_array($result);

						if($_REQUEST['reason'] != ''){
							$mail_message = 'Property: ' . $property . '
							PO Number: ' . $po_num . '
							PO Description: ' . $description . '
							Job No: ' . $job . '
							Cancellation Reason: ' . $reason . '
							Detail: ' . $reason_detail;
						}

						if($_REQUEST['advice'] != ''){
							if($_REQUEST['completed'] != ''){
								$mail_message = 'Property: ' . $property . '
								PO Number: ' . $po_num . '
								PO Description: ' . $description . '
								Job No: ' . $job . '
								Completed Successfully with Advice: ' . $advice;
								}else{
									$mail_message = 'Property: ' . $property . '
								PO Number: ' . $po_num . '
								PO Description: ' . $description . '
								Job No: ' . $job . '
								Updated with Advice: ' . $advice . '
								Estimated Completion: ' . $job;
							}
						}

						$sql = "INSERT INTO cpm_mailer SET
						mail_to = '".$row['personnel_email']."',
						mail_from = 'support@rmgltd.co.uk',
						mail_subject = 'Purchase Order - Job closed with notification',
						mail_message = '".$mail_message."'";
						@mysql_query($sql, $conn);

						$sql = "INSERT INTO cpm_mailer SET
						mail_to = 'customservice@rmguk.com',
						mail_from = 'support@rmgltd.co.uk',
						mail_subject = 'Purchase Order - Job closed with notification',
						mail_message = '".$mail_message."'";

						//@mysql_query($sql, $conn);
					}
				}
				//trigger survey if last job bieng closed
				$po = new po;
				$result_array = $po->trigger_survey($po_id);
			}
		}else{
			$has_error = "No PO Number Supplied";
		}
	}

	$result_array['results'] = $has_error;
	$result_array['email'] = $email;

	echo json_encode($result_array);
	exit;
}

if($_REQUEST['whichaction'] == 'image_update'){
	$po = new po();
	$result_array = $po->set_image($_REQUEST['order_no']);
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'search'){
	$po = new po();
	$result_array = $po->get_results($_REQUEST);

	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'expand'){
	$po = new po();
	$result_array = $po->get_jobs($_REQUEST, $_REQUEST['expand_id'], $_REQUEST['search_term'], $_REQUEST['closed_type']);

	$results = $po->get_po_details($_REQUEST['expand_id']);
	if($results !== false) {
		$result_array['extra'] = array();
		$result_array['extra']['order_no'] = $results['cpm_po_number'];
		$result_array['extra']['year'] = substr($results['cpm_po_date_raised'], 0, 4);
		$result_array['extra']['subsidiary'] = $results['subsidiary_code'];
		$result_array['extra']['rmc'] = $results['cpm_po_rmc_id'];

		$service = new webservice();

		/// Get subsidiary info
		$params 								= array();
		$params['params']						= array();
		$params['params']['rmc_ref']			= $results['cpm_po_rmc_id'];
		$params['params']['brand']				= $results['subsidiary_code'];
		$params['class']						= "asbestos";
		$params['method']						= "check_report_doc";
		$params['username']						= $GLOBALS['webservice']['username'];
		$params['password']						= $GLOBALS['webservice']['password'];

		$uri									= $GLOBALS['webservice']['location'];
		$response 								= $service->curlWrapper($uri, "POST", json_encode($params));
		$doc_array								= json_decode($response['output'], true);
		$result_array['extra']['asbestos']		= $doc_array['output']['output'];

		$group = $results['subsidiary_esc_login_group'];

		$sow = $po->getSOWRef( $results['cpm_po_number'], $group );
		if( !is_array($sow)){
			$result_array['extra']['sow'] = true;
		}else{
			$result_array['extra']['sow'] = false;
		}

	}
	
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'notes'){
	$po = new po();
	$result_array = $po->getNotes($_REQUEST, $_REQUEST['po_id']);

	echo json_encode($result_array);
	exit;
}

$template = "backend";
$page_array = explode('/', $_SERVER['PHP_SELF']);
$page = str_replace('.php','',$page_array[count($page_array) - 1]);

$tpl = new Template($UTILS_SERVER_PATH.'admin/includes/body.tpl');
$tpl->set('title', 'RMG Suppliers - Purchase Orders');
$tpl->set('page_title', 'Purchase Orders');
$tpl->set('UTILS_WEBROOT', $UTILS_WEBROOT);
$tpl->set('UTILS_LOG_PATH',$UTILS_LOG_PATH);
$tpl->set('UTILS_CLASS_PATH', $UTILS_CLASS_PATH);
$tpl->set('UTILS_URL_BASE', $UTILS_URL_BASE);
$tpl->set('UTILS_SERVER_PATH', $UTILS_SERVER_PATH);
$tpl->set('user_name', $_SESSION['admin_user_name']);
$tpl->set('po_data', $tpl->set_sortable_table($UTILS_SERVER_PATH."templates/po_row.tpl"));
$tpl->set('job_data', $tpl->set_sortable_table($UTILS_SERVER_PATH."templates/job_row.tpl"));
$tpl->set('note_data', $tpl->set_sortable_table($UTILS_SERVER_PATH."templates/note_row.tpl"));
$header = $tpl->get_content($UTILS_SERVER_PATH.'admin/includes/'.$template.'_header.tpl');
$content = $tpl->get_content($UTILS_SERVER_PATH.'admin/includes/'.$template.'.tpl');
$page_details = $tpl->get_content($UTILS_SERVER_PATH.'admin/includes/'.$page.'.tpl');
$tpl->set('header', $header);
$tpl->set('content', $content.$page_details);
echo $tpl->fetch();
?>	