<script language="JavaScript" type="text/JavaScript">

	$(document).ready(function(){

		$("#results_header_table").tablesorter({
			dateFormat: 'uk'
		});
		$("#search_results_div_table").tablesorter({
			dateFormat: 'uk'
		});

		$('#results_header_table').bind("sortEnd",
			function(sorter) {
		    	currentSort = sorter.target.config.sortList;
		    	$("#search_results_div_table").trigger("sorton",[currentSort]);
			}
		);

		do_get();
	});
	
	function save(){
		$("#processing_dialog").dialog('open');
		$('#which_action').val('check');
		
		$.post("users.php", 
		$("#form1").serialize() + "&" + $("#form_user").serialize(), 
		function(data){	
			$("#processing_dialog").dialog('close');
			
			if (data.length > 0){
				show_error(data);
			}else{
				$("#add_dialog").dialog('close');
				do_save();
			}
		}, 
		"json");
	}
	
	function do_save(){
		$("#processing_dialog").dialog('open');
		$('#which_action').val('save');
			
		$.post("users.php", 
		$("#form1").serialize() + "&" + $("#form_user").serialize(),
		function(data){	
			$("#processing_dialog").dialog('close');
			if(data["success"] == "Y"){
				$('#error_message').html("Successfully Saved!");
				$("#error_dialog").dialog("option", "title", "Saved!");
			}else{
				$('#error_message').html("Apologies. We seem to have a technical fault.");
				$("#error_dialog").dialog("option", "title", "Failed!");
			}
			$("#error_dialog").dialog("open");
			do_get();
		}, 
		"json");
	}
	
	function add_user(){
		var ad = $("#add_dialog").dialog({  
			modal: true,
			width: "500px",
			resizable: false,
			open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
			buttons: {
				Ok: function() {
					save();
				}
			},
			autoOpen: false
		});	
		
		ad.parent().appendTo('.wrapper');	
		
		$('#name').val("");
		$('#email').val("");
		$('#disabled_row').hide();
		$("#add_dialog").dialog("option", "title", "Add User");
		$('#add_dialog').dialog('open');
	}
	
	function edit_user(id){
		
		if (id == '<?=$_SESSION['contractors_qube_id'];?>'){
			window.location = "change_details.php";
		}else{
			$("#processing_dialog").dialog('open');
			
			var ad = $("#add_dialog").dialog({  
				modal: true,
				width: "500px",
				open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
				buttons: {
					Ok: function() {
						save();
					},
					"Reset Password": function() {
						reset_password();
					}
				},
				autoOpen: false
			});	
			
			ad.parent().appendTo('body');	
			
			$('#user_id').val(id);
			$('#which_action').val('edit');
			
			$('#name').val("");
			$('#email').val("");
			$('#disabled_row').show();
			
			$.post("users.php", 
			$("#form1").serialize(),
			function(data){	
				$("#processing_dialog").dialog('close');
				if(data['success'] == 'Y'){
					$('#name').val(data['name']);
					$('#email').val(data['email']);
					if(data['disabled'] == 'true'){
						$("#disabled").prop("checked", true);
					}else{
						$("#disabled").prop("checked", false);
					}
					$("#add_dialog").dialog("option", "title", "Edit User");
					$('#add_dialog').dialog('open');
				}
			}, 
			"json");
		}
	}
	
	function reset_password(){
		$("#add_dialog").dialog('close');
		$("#processing_dialog").dialog('open');
		$('#which_action').val('reset');
		
		$.post("users.php", 
		$("#form1").serialize(),
		function(data){	
			$("#processing_dialog").dialog('close');
		}, 
		"json");
	}
	
	function do_get(){
		$("#processing_dialog").dialog('open');
		$('#which_action').val('get');

		$.post("users.php",
		$("#form1").serialize(), 
		function(data){				
			if(data['num_results'] > 0){
			
				tr_template = $("#user_row tbody").html();
				
				results = '';
				for(d=0;d<data['USER_ID'].length;d++){			
					var tr = tr_template;
					tr = tr.replace(/USER_ID/g, data['USER_ID'][d]);
					tr = tr.replace(/USER_ONCLICK/g, data['USER_ONCLICK'][d]);
					tr = tr.replace(/USER_USERNAME/g, data['USER_USERNAME'][d]);
					tr = tr.replace(/USER_NAME/g, data['USER_NAME'][d]);
					tr = tr.replace(/USER_EMAIL/g, data['USER_EMAIL'][d]);
					
					results = results + tr;
				}
			}
			else{
				results = $("#no_results_row tbody").html();				
			}

			multi_tablesorter_set("results_header_table", "search_results_div_table", results);

			$("#search_results_div").show();

			var total_view_height = 0;
			
			$("#search_results_div_table tr").each(function(){
				if(total_view_height < 300){
					total_view_height += parseFloat( $(this).css("height").replace("px","") );
				}else{
					return false;
				}
			});
            
			$("#search_results_div").css({			
				"height": total_view_height,
				"overflow":"scroll",
				"overflow-x":"hidden"
			});
			
			$("#processing_dialog").dialog('close');				
		}, 
		"json");
	}
</script>
<div id="add_dialog" title="Add User" class="dialog_1">
<form id="form_user">
	<table cellspacing="0" width="400">
		<tr>
			<td width="100" style="vertical-align:middle;">
				Name
			</td>
			<td width="300">
				<input type="text" id="name" name="name" />
			</td>
		</tr>
		<tr>
			<td width="200" style="vertical-align:middle;">
				Email
			</td>
			<td width="300">
				<input type="text" id="email" name="email" />
			</td>
		</tr>
		<tr id="disabled_row">
			<td width="200">
				Disabled
			</td>
			<td width="300">
				<input type="checkbox" id="disabled" name="disabled" value="True" />
			</td>
		</tr>
	</table>
	</form>
</div>
<div class="main">
	<? require_once($UTILS_SERVER_PATH."templates/user_row.tpl");?>
	<form id="form1" method="post">
		<div class="content">
			<p><a href="javascript:add_user();">Add User</a><br />&nbsp;</p>
			<div class="results_header_table">
				<table id="results_header_table" cellspacing="0">
					<?=$user_data?>
				</table>
			</div>
			<div class="search_results_div" id="search_results_div">
				<table id="search_results_div_table" cellspacing="0" class="hover">
					<?=$user_data ?>
				</table>
			</div>
		</div>
		<input type="hidden" name="which_action" id="which_action" value="" />
		<input type="hidden" name="user_id" id="user_id" value="<?=$_REQUEST['user_id'];?>" />
	</form>
</div>