<?php

$table_1_config = array(
			
	'ROW' => array(
		
		'BORDER_TYPE'			=>	'1',
		'BORDER_COLOR'			=>	array(53,53,53),
		'PADDING_TOP'			=>	2,
		'PADDING_RIGHT'			=>	1,
		'PADDING_BOTTOM'		=>	2,
		'PADDING_LEFT'			=>	1,
		'TEXT_ALIGN'			=>	'L',
		'TEXT_COLOR'			=>	array(53,53,53),
		'TEXT_FONT'				=>	'Arial',
		'TEXT_SIZE'				=>	9,
		'TEXT_TYPE'				=>	'',   
		'VERTICAL_ALIGN'		=>	'T'
	),
	'TABLE' => array(
	
		'BORDER_COLOR'			=>	array(53,53,53),
		'BORDER_TYPE'			=>	'1',
		'TABLE_ALIGN'			=>	'L', 
		'TABLE_LEFT_MARGIN'		=>	0
	)
);

$table_no_border_config = array(

	'ROW' => array(

		'BORDER_TYPE'			=>	'0',
		'BORDER_COLOR'			=>	array(53,53,53),
		'PADDING_TOP'			=>	1,
		'PADDING_RIGHT'			=>	1,
		'PADDING_BOTTOM'		=>	1,
		'PADDING_LEFT'			=>	1,
		'TEXT_ALIGN'			=>	'L',
		'TEXT_COLOR'			=>	array(53,53,53),
		'TEXT_FONT'				=>	'Arial',
		'TEXT_SIZE'				=>	9,
		'TEXT_TYPE'				=>	'',
		'VERTICAL_ALIGN'		=>	'T'
	),
	'TABLE' => array(

		'BORDER_COLOR'			=>	array(53,53,53),
		'BORDER_TYPE'			=>	'0',
		'TABLE_ALIGN'			=>	'L',
		'TABLE_LEFT_MARGIN'		=>	0
	)
);


$h1_config = array(
			
	'ROW' => array(
	
		'BORDER_COLOR'			=>	array(0,92,177),
		'BORDER_TYPE'			=>	'B',
		'LINE_SIZE'				=>	10, 
		'PADDING_BOTTOM'		=>	2,
		'TEXT_ALIGN'			=>	'L',
		'TEXT_COLOR'			=>	array(23,54,93),
		'TEXT_FONT'				=>	'Arial',
		'TEXT_SIZE'				=>	26,
		'VERTICAL_ALIGN'		=>	'T'
	),
	'TABLE' => array(
	
		'BORDER_TYPE'			=>	'0',
		'TABLE_ALIGN'			=>	'L', 
		'TABLE_LEFT_MARGIN'		=>	0
	)
);


$h3_config = array(
			
	'ROW' => array(
	
		'BORDER_TYPE'			=>	0,
		'LINE_SIZE'				=>	6, 
		'PADDING_BOTTOM'		=>	2,
		'TEXT_ALIGN'			=>	'L',
		'TEXT_COLOR'			=>	array(0,0,0),
		'TEXT_FONT'				=>	'Arial',
		'TEXT_SIZE'				=>	14,
		'VERTICAL_ALIGN'		=>	'T'
	),
	'TABLE' => array(
	
		'BORDER_TYPE'			=>	'0',
		'TABLE_ALIGN'			=>	'L', 
		'TABLE_LEFT_MARGIN'		=>	0
	)
);

?>