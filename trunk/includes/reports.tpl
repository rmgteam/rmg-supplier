<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.page.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#results_header_table").tablesorter({
		dateFormat: 'uk'
	});
	$("#search_results_div_table").tablesorter({
		dateFormat: 'uk'
	});

	$('#results_header_table').bind("sortEnd",
			function(sorter) {
				currentSort = sorter.target.config.sortList;
				$("#search_results_div_table").trigger("sorton",[currentSort]);
			}
	);

	$("#user_results_header_table").tablesorter({
		dateFormat: 'uk'
	});
	$("#user_search_results_div_table").tablesorter({
		dateFormat: 'uk'
	});

	$('#user_results_header_table').bind("sortEnd",
			function(sorter) {
				currentSort = sorter.target.config.sortList;
				$("#user_search_results_div_table").trigger("sorton",[currentSort]);
			}
	);

	$(".date").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true });
	toggle_fields();
});

function show_dialog(the_width, title){
	var ad = $("#dialog_audit").dialog({
		modal: true,
		resizable: false,
		width: the_width,
		open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
		buttons: {
			Ok: function() {
				$( this ).dialog( "close" );
			}
		}
	});

	$("#dialog_audit").dialog("option", "title", title);

	ad.parent().appendTo('#wrapper');
}

function do_search() {
	if( $('#search_type').val() == '1'){
		do_po();
	}else{
		do_user();
	}
}

function do_user(){
	$("#processing_dialog").dialog('open');
	$('#whichaction').val('user');
	toggle_fields();

	$.post("reports.php",
			$("#form1").serialize(),
			function(data){
				if(data['num_results'] > 0){

					var tr_template = $("#user_row tbody").html();

					results = '';
					for(d=0;d<data['USER_ID'].length;d++){
						var tr = tr_template;
						tr = tr.replace(/USER_ID/g, data['USER_ID'][d]);
						tr = tr.replace(/USER_ONCLICK/g, data['USER_ONCLICK'][d]);
						tr = tr.replace(/USER_NAME/g, data['USER_NAME'][d]);
						tr = tr.replace(/USER_USERNAME/g, data['USER_USERNAME'][d]);
						tr = tr.replace(/USER_EMAIL/g, data['USER_EMAIL'][d]);
						tr = tr.replace(/USER_DISABLED/g, data['USER_DISABLED'][d]);
						tr = tr.replace(/USER_COUNT/g, data['USER_COUNT'][d]);
						tr = tr.replace(/USER_LAST/g, data['USER_LAST'][d]);

						results = results + tr;
					}
				}
				else{
					results = $("#no_results_row tbody").html();
				}

				multi_tablesorter_set("user_results_header_table", "user_search_results_div_table", results);

				$("#user_search_results_div").show();

				var total_view_height = 0;

				$("#user_search_results_div_table tr").each(function(){
					if(total_view_height < 300){
						total_view_height += parseFloat( $(this).css("height").replace("px","") );
					}else{
						return false;
					}
				});

				$("#user_search_results_div").css({
					"height": total_view_height,
					"overflow":"scroll",
					"overflow-x":"hidden"
				});

				$("#processing_dialog").dialog('close');
			},
			"json");
}

function do_po(){
	$("#processing_dialog").dialog('open');
	$('#whichaction').val('order');
	toggle_fields();

	$.post("reports.php",
			$("#form1").serialize(),
			function(data){
				if(data['num_results'] > 0){

					tr_template = $("#report_row tbody").html();

					results = '';
					for(d=0;d<data['REPORT_ID'].length;d++){
						var tr = tr_template;
						tr = tr.replace(/REPORT_ID/g, data['REPORT_ID'][d]);
						tr = tr.replace(/REPORT_ONCLICK/g, data['REPORT_ONCLICK'][d]);
						tr = tr.replace(/REPORT_NUMBER/g, data['REPORT_NUMBER'][d]);
						tr = tr.replace(/REPORT_PROPERTY/g, data['REPORT_PROPERTY'][d]);
						tr = tr.replace(/REPORT_LINES/g, data['REPORT_LINES'][d]);
						tr = tr.replace(/REPORT_DATE/g, data['REPORT_DATE'][d]);

						results = results + tr;
					}
				}
				else{
					results = $("#no_results_row tbody").html();
				}

				multi_tablesorter_set("results_header_table", "search_results_div_table", results);

				$("#search_results_div").show();

				var total_view_height = 0;

				$("#search_results_div_table tr").each(function(){
					if(total_view_height < 300){
						total_view_height += parseFloat( $(this).css("height").replace("px","") );
					}else{
						return false;
					}
				});

				$("#search_results_div").css({
					"height": total_view_height,
					"overflow":"scroll",
					"overflow-x":"hidden"
				});

				$("#processing_dialog").dialog('close');
			},
			"json");
}

function do_audit(id) {
	$("#processing_dialog").dialog('open');
	$('#user_id').val(id);
	$('#whichaction').val('audit');

	$.post("reports.php",
			$("#form1").serialize(),
			function(data){
				$('#audit_results').html(data['results']);
				$('#audit_paging').html(data['paging']);
				$('#audit_results').css('width', '470px');
				$('#audit_paging').css('width', '470px');
				$("#processing_dialog").dialog('close');
				show_dialog("500px", "Audit Trail");
				toggle_fields();
			},
			"json");
}

function do_job(id, request) {
	$("#processing_dialog").dialog('open');
	$('#user_id').val(id);

	var new_request = request.replace("whichaction=order&", "");

	$.post("reports.php",
			new_request.replace("amp;", "") + "&whichaction=job&po_no=" + id,
			function(data){
				$('#audit_results').html(data['results']);
				$('#audit_paging').html(data['paging']);
				$('#audit_results').css('width', '565px');
				$('#audit_paging').css('width', '565px');
				$("#processing_dialog").dialog('close');
				show_dialog("600px", "Jobs Within PO");
				toggle_fields();
				$("#job_pages").paging({pageName : "jobination"});
			},
			"json");
}

function toggle_fields(){

	$('#purchase_row_1').hide();
	$('#purchase_row_2').hide();

	$('#po_report').hide();
	$('#user_report').hide();

	if( $('#search_type').val() == '1'){
		$('#whichaction').val('order');
		$('#purchase_row_1').show();
		$('#purchase_row_2').show();

		$('#po_report').show();
	}else{
		$('#whichaction').val('user');
		$('#date_from').val('');
		$('#date_to').val('');

		$('#user_report').show();
	}
	toggle_complete();
}

function toggle_complete(){

	$('#purchase_row_3').hide();
	$('#purchase_row_4').hide();

	if( $('#is_completed').val() == 'True'){
		$('#purchase_row_3').show();
		$('#purchase_row_4').show();
	}else{
		$('#date_from').val('');
		$('#date_to').val('');
	}
}

</script>
<div id="dialog_audit" title="Audit Trail" class="dialog_1">
	<div class="results" style="width:470px;" id="audit_results">
	</div>
	<div class="results" style="border:none; width:470px;" id="audit_paging">
	</div>
</div>
<div class="main">
	<? require_once($UTILS_SERVER_PATH."templates/report_row.tpl");?>
	<? require_once($UTILS_SERVER_PATH."templates/user_report_row.tpl");?>
	<? require_once($UTILS_SERVER_PATH."templates/report_job_row.tpl");?>
	<form id="form1" method="post">
		<div class="content">
			<div class="search_heading">
				<h2>Search Facility</h2>
			</div>
			<div class="search_box">
				<table cellspacing="0">
					<tr>
						<td width="30%">
							<h5>Type of Report</h5>
						</td>
						<td>
							<h5>Search Term</h5>
						</td>
					</tr>
					<tr>
						<td width="30%">
							<select id="search_type" name="search_type" onchange="toggle_fields();">
								<option value="1">Purchase Orders</option>
								<option value="2">User Accounts</option>
							</select>
						</td>
						<td width="30%">
							<input type="text" id="search_term" name="search_term" />
						</td>
					</tr>
					<tr id="purchase_row_1">
						<td colspan="2">
							<h5>Closed</h5>
						</td>
					</tr>
					<tr id="purchase_row_2">
						<td colspan="2">
							<select id="is_completed" name="is_completed" onchange="toggle_complete()">
								<option value="False">No</option>
								<option value="True">Yes</option>
								<option value="">Either</option>
							</select>
						</td>
					</tr>
					<tr id="purchase_row_3">
						<td width="30%">
							<h5>Date From</h5>
						</td>
						<td>
							<h5>Date To</h5>
						</td>
					</tr>
					<tr id="purchase_row_4">
						<td width="30%">
							<input class="date" type="text" id="date_from" name="date_from" />
						</td>
						<td>
							<input class="date" type="text" id="date_to" name="date_to" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="button" class="button" id="search_button" name="search_button" value="Search" onclick="do_search()" />
						</td>
					</tr>
				</table>
			</div>
			<div id="po_report" style="float:left; width:100%;">
				<div class="results_header_table">
					<table id="results_header_table" cellspacing="0">
						<?=$po_data?>
					</table>
				</div>
				<div class="search_results_div" id="search_results_div">
					<table id="search_results_div_table" cellspacing="0" class="hover">
						<?=$po_data ?>
					</table>
				</div>
			</div>
			<div id="user_report" style="float:left; width:100%;">
				<div class="results_header_table">
					<table id="user_results_header_table" cellspacing="0">
						<?=$user_data?>
					</table>
				</div>
				<div class="search_results_div" id="user_search_results_div">
					<table id="user_search_results_div_table" cellspacing="0" class="hover">
						<?=$user_data ?>
					</table>
				</div>
			</div>
		</div>
		<input type="hidden" id="whichaction" name="whichaction" value="" />
		<input type="hidden" id="to_complete" name="to_complete" value="" />
		<input type="hidden" id="user_id" name="user_id" value="" />
	</form>
</div>