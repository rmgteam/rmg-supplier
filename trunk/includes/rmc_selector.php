<script type="text/javascript">
//#########################################################################################################
//
//	USAGE:
//	- Requires /library/controllers/rmc_selector.controller.php to be included in <body>
//  - Requires /includes/common_head_1.php in the <head> section
//	- Include a <span> element with a unique id on the page where the chooser will be placed
//	- Use the span id as the selector in the head of the document, e.g. $("#rmc_chooser_1").rmcchooser();
//
//
//	EXAMPLE OF USAGE:
//
//	$('#mylink').rmcchooser({
//		
//		inclClear: true,
//		beforeEnd: function(){
//			
//			do_something(a, b);
//		},
//		func_name: function(rmc_num, rmc_name){
//			
//			do_something(rmc_num, rmc_name);
//		}
//	});
//
//##########################################################################################################

/*
 * jQuery UI RMC Selector
 */
(function( $, undefined ) {
	
	var emptyFunction = function(){};
	
	$.widget( "ui.rmc_selector", {
		options: {
			objID: '',
			link_attach: false,
			buttonText: '(Please Select)',
			disabledNonText: '(None Selected)',
			index_field: 'rmc_ref',
			clear_var: '',
			is_disabled: false,
			link: '',
    		inclClear: true,
			beforeEnd: emptyFunction,
			func_name: '',
			start_slide: 1,
			show_ref: true,
			multi_select: false,
			owner_id: '',
			myApp: {},
			css_width: ''
		},
	
		_create: function() {
			
			var self = this;
			var options = this.options;
			var classes = this.classes;
	
			options.objID = this.element.attr("id");
			
			if(options.link_attach === true){
				
				$("#" + options.objID).attr("href","Javascript:;");
				options.link = options.objID;
			}
			else{
		
				options.link = options.objID + '_a';
	
				var selector_block = $( document.createElement('div') )
					.attr('id', options.objID + '_block')
					.addClass('selector_block');
	
				var selector_buttons = $( document.createElement('div') )
					.attr('id', options.objID + '_buttons')
					.addClass('selector_buttons');
	
				var search_button = $( document.createElement('a') )
					.attr({
						'id':	 	options.objID + '_a',
						'title':	"Search"
					})
					.addClass('ui-blue')
					.appendTo(selector_buttons);
				
				var clear_button = $( document.createElement('a') )
					.attr({
						'id':	 	options.objID + '_clear',
						'title':	"Clear"
					})
					.addClass('ui-blue')
					.appendTo(selector_buttons);
				
				var info_button = $( document.createElement('a') )
					.attr({
						'id':	 	options.objID + '_info',
						'title':	"View"
					})
					.addClass('ui-blue')
					.appendTo(selector_buttons);
	
				selector_block.append(selector_buttons);
	
				var chosen_item = $( document.createElement('div') )
					.attr('id', options.objID + '_select')
					.addClass('ui-blue-text')
					.html(options.buttonText)
					.appendTo(selector_block);
	
				var hidden_value = $( document.createElement('input') )
					.attr({
						'id':	 	options.objID + '_input',
						'name':		options.objID + '_input',
						'type':		'hidden'
					})
					.appendTo(selector_block);
	
				var hidden_value = $( document.createElement('input') )
					.attr({
						'id':	 	options.objID + '_selected',
						'name':		options.objID + '_selected',
						'type':		'hidden'
					})
					.appendTo(selector_block);
	
				$("#" + options.objID).append(selector_block);
				
				// append the loading message
				var loading = $( document.createElement('div') )
					.attr('id', options.objID + '_loading')
					.addClass('loading_msg_1');
		
				$("#" + options.objID).append(loading);
			}
			
			//Bind click event to the icon
			$('#'+options.link)
				.button({
					text: false,
					icons: {
			            primary: "ui-icon-search"
			        }
				})
				.on('click', {'self' : self}, function(e){

					var self = e.data['self'];
					if($('#' + options.objID + '_search_dialog').length > 0){
						self.open();
					}else{
						self.build(function(){self.open();});
					}
				})
				.next()
				.button( {
					text: false,
					icons: {
						primary: "ui-icon-closethick"
					}
				})
				.on('click', {'self' : self}, function(e){
					self.clear();
				})
				.next()
				.button( {
					text: false,
					icons: {
						primary: "ui-icon-info"
					}
				})
				.on('click', {'self' : self}, function(e){
					self.view($("#" + options.objID + "_input").val());
				})
				.parent()
					.buttonset();

			options.css_width = $("#" + options.objID).css('width').replace('px', '');

			var ss = document.styleSheets;

		    for (var i=0; i<ss.length; i++) {
		        var rules = ss[i].cssRules || ss[i].rules;

		        for (var j=0; j<rules.length; j++) {
			        var selector_text = rules[j].selectorText;
		            if (selector_text === ".search_filter_fields div:first-child" || selector_text === ".search_filter_fields DIV:first-child") {
		            	options.css_width = rules[j].style['width'];
		            }
		        }
		    }
			
			this.set_bind();
			this.clear();
		},

		build: function(callback){
			var self = this;
			var options = this.options;
			
			var rmc_search_dialog = $( document.createElement('div') )
				.attr({
					'id' :		options.objID + '_search_dialog', 
					'title' :	"Property Search"
				})
				.addClass('dialog_1');
	
			var search_dialog_inner = $( document.createElement('div') )
				.css({
					'margin':	'0px',
					'overflow': 'hidden'
				});
	
			var form = $( document.createElement('form') )
				.attr({
					'id' :		options.objID + '_search_form', 
					'name' :	options.objID + '_search_form'
				});
	
			var slidedeck_frame = $( document.createElement('div') )
				.attr({
					'id' :		options.objID + '_slidedeck_frame'
				})
				.addClass('skin-slidedeck');
			
			var dl = $( document.createElement('dl') )
				.attr({
					'id' :		options.objID + '_slide'
				})
				.addClass('slidedeck');			
	
			var dt = $( document.createElement('dt') )
				.html('Search');
			
			dl.append(dt);
	
			var dd = $( document.createElement('dd') );
	
			var list = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_list'
				});
	
			var heading = $( document.createElement('div') )
				.addClass('record_heading');
	
			var heading_text = $( document.createElement('div') )
				.addClass('left')
				.html('Search Facility');
	
			heading.append(heading_text);
			list.append(heading);
	
			var filter = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_search_filter'
				})
				.appendTo(list);
	
			dd.append(list);
			dl.append(dd);
	
			var dt = $( document.createElement('dt') )
				.html('Results');
			
			dl.append(dt);
	
			var dd = $( document.createElement('dd') );
	
			var waiting = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_search_waiting'
				})
				.addClass('selector_waiting')
				.html('<p style="text-align:center;font-size:16px;">Retrieving data...</p><p style="text-align:center; margin-top:15px;"><img src="/images/ajax-loader-2.gif" /></p>')
				.appendTo(dd);
	
			var list = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_results_list'
				});
	
			var table_div = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_results_table_div'
				});
				
			var table = $( document.createElement('table') )
				.attr({
					'id':			options.objID  + '_results_table',
					'cellpadding':	'0',
					'cellspacing':	'0',
					'border':	'0',
					'width':	'100%'
				})
				.addClass('display')
				.html($('#rmc_row').html())
				.appendTo(table_div);
	
			list.append(table_div);
			dd.append(list);
			dl.append(dd);
		
			var dt = $( document.createElement('dt') )
				.html('Information');
			
			dl.append(dt);
	
			var dd = $( document.createElement('dd') );
	
			var waiting = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_details_waiting'
				})
				.addClass('selector_waiting')
				.html('<p style="text-align:center;font-size:16px;">Retrieving data...</p><p style="text-align:center; margin-top:15px;"><img src="/images/ajax-loader-2.gif" /></p>')
				.appendTo(dd);
	
			var row = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_details_no_result'
				})
				.html('Please select a property')
				.appendTo(dd);
	
			var list = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_details_list'
				});
	
			var row = $( document.createElement('div') )
				.addClass('search_filter_row');
	
			var col = $( document.createElement('div') )
				.addClass('search_filter_label')
				.html('<strong>Qube Ref</strong>')
				.appendTo(row);
			
			var col = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr({
					'id':		options.objID + '_details_ref'
				})
				.appendTo(row);
	
			list.append(row);
	
			var row = $( document.createElement('div') )
				.addClass('search_filter_row');
	
			var col = $( document.createElement('div') )
				.addClass('search_filter_label')
				.html('<strong>Name</strong>')
				.appendTo(row);
			
			var col = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr({
					'id':		options.objID + '_details_name'
				})
				.appendTo(row);
			
			list.append(row);
	
			var row = $( document.createElement('div') )
				.addClass('search_filter_row');
	
			var col = $( document.createElement('div') )
				.addClass('search_filter_label')
				.html('<strong>County</strong>')
				.appendTo(row);
				
			var col = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr({
					'id':		options.objID + '_details_county'
				})
				.appendTo(row);
				
			list.append(row);
	
			var row = $( document.createElement('div') )
				.addClass('search_filter_row');
	
			var col = $( document.createElement('div') )
				.addClass('search_filter_label')
				.html('<strong>Postcode</strong>')
				.appendTo(row);
			
			var col = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr({
					'id':		options.objID + '_details_postcode'
				})
				.appendTo(row);
				
			list.append(row);
	
			var row = $( document.createElement('div') )
				.addClass('search_filter_row');
	
			var col = $( document.createElement('div') )
				.addClass('search_filter_label')
				.html('<strong>Brand</strong>')
				.appendTo(row);
			
			var col = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr({
					'id':		options.objID + '_details_brand'
				})
				.appendTo(row);
				
			list.append(row);
			var row = $( document.createElement('div') )
				.addClass('search_filter_row');
	
			var col = $( document.createElement('div') )
				.addClass('search_filter_label')
				.html('<strong>Owner</strong>')
				.appendTo(row);
			
			var col = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr({
					'id':		options.objID + '_details_owner'
				})
				.appendTo(row);
				
			list.append(row);
	
			var row = $( document.createElement('div') )
				.addClass('search_filter_row');
	
			var col = $( document.createElement('div') )
				.addClass('search_filter_label')
				.html('<strong>Operational Director</strong>')
				.appendTo(row);
			
			var col = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr({
					'id':		options.objID + '_details_od'
				})
				.appendTo(row);
				
			list.append(row);
	
			var row = $( document.createElement('div') )
				.addClass('search_filter_row');
	
			var col = $( document.createElement('div') )
				.addClass('search_filter_label')
				.html('<strong>Regional Manager</strong>')
				.appendTo(row);
			
			var col = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr({
					'id':		options.objID + '_details_rm'
				})
				.appendTo(row);
				
			list.append(row);
	
			var row = $( document.createElement('div') )
				.addClass('search_filter_row');
	
			var col = $( document.createElement('div') )
				.addClass('search_filter_label')
				.html('<strong>Property Manager</strong>')
				.appendTo(row);
			
			var col = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr({
					'id':		options.objID + '_details_pm'
				})
				.appendTo(row);
				
			list.append(row);
	
			var row = $( document.createElement('div') )
				.addClass('search_filter_row');
	
			var col = $( document.createElement('div') )
				.addClass('search_filter_label')
				.html('<strong>Companies House No.</strong>')
				.appendTo(row);
			
			var col = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr({
					'id':		options.objID + '_details_co_ho_no'
				})
				.appendTo(row);
	
			list.append(row);
	
			var row = $( document.createElement('div') )
				.addClass('search_filter_row');
	
			var col = $( document.createElement('div') )
				.addClass('search_filter_label')
				.html('<strong>Qube Status</strong>')
				.appendTo(row);
			
			var col = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr({
					'id':		options.objID + '_details_qube_status'
				})
				.appendTo(row);
			
			list.append(row);
	
			dd.append(list);
			dl.append(dd);
			
			slidedeck_frame.append(dl)
			form.append(slidedeck_frame);
	
			search_dialog_inner.append(form)
			var save_button = $( document.createElement('a') )
				.html('Save')
				.attr({
					'id':		options.objID + '_multi_save'
				})
				.addClass('jButton')
				.css({
					'float':'right',
					'position':'relative',
					'margin': '0 10px 0 0'
				})
				.bind("click", {'self' : self}, function(e){
	
					var self = e.data['self'];
					var options = self.options;
					var rmc_refs = '';
	
					var types = $('#' + options.objID + '_selected').val().split(',');
					
					for(var i=0; i<types.length; i++){
						var item = types[i].split(':');
						if(item[0] != ''){
							var id = item[0];
							rmc_refs += id + ',';
						}
					}
					
					self.set(rmc_refs.slice(0,-1));
					
				});
	
			search_dialog_inner.append(save_button);
			
			rmc_search_dialog.append(search_dialog_inner);
			
			$('body').append(rmc_search_dialog);
	
			$('#' + options.objID + '_search_dialog').dialog({
				modal: true, 
				width: 900, 
				autoOpen: false,
				open: function(event, ui) { 
					$(".ui-dialog-titlebar-close").show();
					$('#' + options.objID + '_slide').slidedeck('update');
				},
				close: function(event, ui) {
					$('#' + options.objID + '_slide').slidedeck('goTo','1');
				}
			});
			$('#' + options.objID + '_slide').slidedeck();
			$('#' + options.objID + '_slide').bind('slidedeck_spine_click', {'self':self}, function(e){
				var self = e.data['self'];
				var options = self.options;
				
				if($(this).slidedeck('getCurrent') == '3'){
					$('#' + options.objID + '_details_no_result').show();
					$('#' + options.objID + '_details_list').hide();
				}
			});
			
			if(options.multi_select == false){
				$('#rmc_multi_save').hide();
			}else{
				var a = $( document.createElement('div') )
					.attr({
						'id'	:	options.objID + '_select_all',
						'role'	:	'button'
					})
					.data({
						"toggle":"tooltip",
						"title": "Select All",
						"tooltip":"Select all properties on all pages"
					})
					.appendTo($('#' + options.objID  + '_results_table thead tr th:first'));

				$('body').build_tooltip('add_tooltip', $('#' + options.objID + '_select_all'));
				
			}

			$('#' + options.objID + '_search_filter').search_filter({
				'values':[
					{
						'id': 		options.objID + '_rmc_ref_input',
						'type': 	'input',
						'value': 	'rmc_ref',
						'html':		'Property Name/Ref.'
					},
					{
						'id': 		options.objID + '_nla',
						'type': 	'radio',
						'params':	 {
                            'value' : [{
								'name'	:	'ID',
								'multi'	:	false,
								'id'	:	'id',
								'answers' : [
									{
										'name'	:	'Yes',
										'value'	:	'Y'
									},
									{
										'name'	:	'No',
										'value'	:	'N',
										'default': true
									}
								 ]
                            }]
						},
						'value': 	'nla',
						'html':		"Include 'NLA' Management Companies"
					}
				],
				'override': true,
				'page': '/library/controllers/rmc_selector.controller.php',
				'type_name': options.objID + '_type'
			});
			
			if($.isFunction(callback) === true){
				callback();
			}

			self._trigger('build');

		},

		set_bind: function(){
			var self = this;
			var options = this.options;

			var parent = $("#" + options.objID).parent();
			var has_dn = parent.css('display');

			var i = 0;

			while(has_dn != 'none' && i < 5){
				parent = parent.parent();
				has_dn = parent.css('display');
				i++;
			}

			parent
				.data('overrideShow', "true")
				.bind('beforeShow', {'options' : options}, function(e){
					var options = e.data['options'];
					
					if($("#" + options.objID + "_input").val() != ''){
						$("#" + options.objID + "_select").css({
							"width": '0px'
						});
					}
				})
				.bind('afterShow', {'self' : self}, function(e){
					var self = e.data['self'];
					var options = self.options;
	
					if($("#" + options.objID + "_input").val() != ''){
						self.recalculate();
					}
				});
		},

		open: function() {
			var self = this;
			var options = this.options;

			$('#' + options.objID + '_search_dialog').dialog('open');

			if(options.is_disabled == false){
				$('#' + options.objID + '_slide').slidedeck('enable');
			}

			if(options.is_disabled == false){
				if(options.multi_select == false){
					$('#' + options.objID + '_multi_save').hide();
				}else{
					$('#' + options.objID + '_multi_save').show();
				}
			}else{
				$('#' + options.objID + '_multi_save').hide();
			}

			$('#' + options.objID + '_results_list').show();
			$('#' + options.objID + '_results_table_div').hide();
			$('#' + options.objID + '_search_waiting').hide();

			$('#' + options.objID + '_details_list').hide();
			$('#' + options.objID + '_details_waiting').hide();
			$('#' + options.objID + '_details_no_result').hide();

			$('#' + options.objID + '_search_filter').search_filter('setOption', 'callback', function(){$('#' + options.objID).rmc_selector('search');});

			$('#' + options.objID + '_search_filter').search_filter('set_focus', 1, 'rmc_ref');
			self.reset_pos();
			
			if(options.start_slide != 1){
				$('#' + options.objID + '_slide').slidedeck('goTo', options.start_slide );
			}

			this._trigger('open');
		},

		reset_pos: function(){
			var self = this;
			var options = this.options;

			if(options.is_disabled == true){
				$('#' + options.objID + '_search_filter').hide();
			}else{
				$('#' + options.objID + '_search_filter').show();
			}
		},
		
		recalculate: function(vari) {
			
			var options = this.options;

			var pwidth = $("#" + options.objID).actual('width');
			var tag_name = $("#" + options.objID).get(0).tagName;
			var css_width = options.css_width;
			
			if((tag_name == 'SPAN' || tag_name == 'DIV') && (css_width == '100%' || css_width == 'auto')){
				pwidth = $("#" + options.objID).parent().actual('width');
				$("#" + options.objID).addClass('selector_surround');
			}
			
			var bwidth = $("#" + options.objID + "_buttons").outerWidth(true);
			
			var span_width_2 = pwidth - (bwidth + 1);

			var t = $( document.createElement('span') )
				.attr('id', options.objID + '_text_test')
				.addClass('ui-blue-text')
				.css({
					'display': 'inline-block'
				})
				.html($("#" + options.objID + "_select").html());
			
			$('body').prepend(t);
			
			var span_width = $('#' + options.objID + '_text_test').actual( 'outerWidth', { includeMargin : true });
			
			$('#' + options.objID + '_text_test').remove();
			
			if(span_width > span_width_2){
				span_width = span_width_2;
			}
			
			if(vari == true){
				span_width = 'auto';
			}else{
				span_width += 'px';
			}
			
			$("#" + options.objID + "_select").css({
				"width": span_width
			});

			var pheight =  $("#" + options.objID + "_select").actual('outerHeight');

			if($("#" + options.objID).parent().actual('outerHeight') < pheight){
				$("#" + options.objID).parent().css("height", pheight + "px");
			}
		},

		setOption: function(key, value){

			$.Widget.prototype._setOption.apply( this, arguments );
		},
	
		/* Enable the rmc selector. */
		enable: function() {
			var self = this;
			var options = this.options;
			if(options.is_disabled == true){
				options.is_disabled = false;
				
				if( $('#' + options.objID + '_input').val() != "" ){
					$('#' + options.objID + '_clear').show();
				}
				
				if(options.multi_select == false){
					$('#' + options.objID + '_multi_save').hide();
				}else{
					$('#' + options.objID + '_multi_save').show();
				}

				if($("#" + options.objID + '_select').html() == options.disabledNonText){
					$("#" + options.objID + '_select').html(options.buttonText);
					self.recalculate();
				}
				
				$('#' + options.link).show();

				$("#" + options.objID + '_info').removeClass('ui-corner-left');
				$("#" + options.objID + '_info').removeClass('ui-corner-all');
				$("#" + options.objID + '_info').addClass('ui-corner-right');
				$("#" + options.objID + '_slide').slidedeck("enable");
			}
			
			this._trigger('enable');
		},
	
		/* Enable the rmc selector. */
		disable: function() {
			var self = this;
			var options = this.options;
			if(options.is_disabled == false){
				options.is_disabled = true;
				$('#' + options.objID + '_clear').hide();

				$('#' + options.objID + '_multi_save').hide();

				if($("#" + options.objID + '_select').html() == options.buttonText){
					$("#" + options.objID + '_select').html(options.disabledNonText);
					self.recalculate();
				}
				
				$('#' + options.link).hide();
				$("#" + options.objID + '_info').removeClass('ui-corner-left');
				$("#" + options.objID + '_info').addClass('ui-corner-all');
				$("#" + options.objID + '_slide').slidedeck("disable");
			}
			
			this._trigger('disable');
		},
		
		/* Erase the input field and hide the rmc selector. */
		clear: function() {
		
			var options = this.options;
			
			$("#" + options.objID + "_input").val('');
	 	  	$("#" + options.objID + "_select").html(options.buttonText);
			$('#' + options.objID + '_clear').hide();
			$("#" + options.objID + '_info').hide();

			$("#" + options.link).removeClass('ui-corner-left');
			$("#" + options.link).addClass('ui-corner-all');

			$('#' + options.objID + '_results_table tbody div.tickbox').each(function (i) {
				$(this).find("input[type='checkbox']").removeAttr('checked');
			});

			this.recalculate();
			this._trigger('clear');
		},
		
		select_all: function(){
			var self = this;
			var options = this.options;
			
			var has = 0;
			var id = options.objID + '_results_table';
			
			$('#' + id + ' tr td div.tickbox').each(function(){
				var input = $(this).find('input[type=hidden]');
				
				if(input.val() == "1"){
					has++;
					return false;
				}
			});
			
			$('#' + id + ' tr td div.tickbox').each(function(){
				var radio_id = $(this).attr('id');
				if(has > 0){
					$('#' + radio_id ).radio('set', '');
				}else{
					$('#' + radio_id ).radio('set', "1");
				}
			});

			if(has > 0){
				$('#' + options.objID + '_select_all').radio('set', '');
        		$('#' + options.objID + '_selected').val('');
			}else{
				$('#' + options.objID + '_select_all').radio('set', '1');	

        		var aoData = [];

    			pushFormData('#' + options.objID + '_search_form', aoData);
	        	aoData.push({name : 'no_fields', value : $('#' + options.objID + '_search_filter').search_filter('get_no_fields')});
	        	aoData.push({name : 'rmc_search_php', value : 'get_all'});
	        	aoData.push({name : 'owner_id', value : options.owner_id});
	        	aoData.push({name : 'select', value : options.objID});
	        	aoData.push({name : 'multi', value : false});
	        	aoData.push({name : 'index_field', value : options.index_field});
        		
        		$.post("/library/controllers/rmc_selector.controller.php", 
    			aoData, 
    			function(data){
        			$('#' + options.objID + '_selected').val(data['values']);
    			}, 
				"json");
			}
			
			self._trigger('select_all');
		},

		search: function() {
			var self = this;
			var options = this.options;
			
			var owner_id = '';

			if(options.owner_id != ''){
				owner_id = $('#' + options.owner_id).val();
			}
			
            if(options.multi_select == false || (options.multi_select == true && options.is_disabled == true)){
				var show_multi = false;
				var name_width = "260";
			}else{
				var show_multi = true;
				var name_width = "235";
			}
			
			var col_defs = [
				{
					"bVisible":		show_multi,
					"bSortable":	false,
					"sWidth": 		"25"
				},
				{
					"bVisible":		true,
					"sWidth": 		"50"
				},
				{
					"bVisible":		true,
					"sWidth": 		"85"
				},
				{
					"bVisible":		true,
					"sWidth": 		name_width
				},
				{
					"bVisible":		true,
					"bSortable":	false,
					"sWidth": 		"35"
				},
				{
					"bVisible":		true,
					"bSortable":	false,
					"sWidth": 		"65"
				}		
			];
			
			$('#' + options.objID + '_slide').slidedeck('goTo', '2');
			
			var sort_array = new Array();

			sort_array = [[ 2 , 'asc' ]];

			options.myApp.can = false;
			if(options.myApp.table === undefined){
				options.myApp.can = true;
			}
			
			options.myApp.table = $('#' + options.objID + '_results_table').DataTable({
				"bJQueryUI": true,
				"bProcessing": true,
				"bDestroy": true,
				"bAutoWidth": false,
				"bLengthChange": false,
				"bScrollCollapse": true,
				"bDeferRender": true,
				"bPaginate": true,
			    "bFilter": false,
				"sPaginationType": "full_numbers",
			    "oLanguage": {
					"sInfoFiltered": ""
				},
				"aoColumns": col_defs,
				"bServerSide": true,
				"sAjaxSource": "/library/controllers/rmc_selector.controller.php",
				"sServerMethod": "POST",
				"fnServerData": function ( sSource, aoData, fnCallback ) {
				
					$('#' + options.objID + '_search_waiting').show();
					$('#' + options.objID + '_results_list').hide();

		        	var return_types = [];

		        	var types = $('#' + options.objID + '_selected').val().split(',');
					
					for(var i=0; i<types.length; i++){
						var item = types[i].split(':');
						if(item[0] != ''){
							return_types.push({name: item[0], value: item[1]});
						}
					}
		        	
		        	pushFormData('#' + options.objID + '_search_form', aoData);
		        	aoData.push({name : options.objID + '_selected', value :  JSON.stringify(return_types)});
		        	aoData.push({name : 'no_fields', value : $('#' + options.objID + '_search_filter').search_filter('get_no_fields')});
		        	aoData.push({name : 'rmc_search_php', value : 'rmc_search'});
		        	aoData.push({name : 'owner_id', value : owner_id});
		        	aoData.push({name : 'select', value : options.objID});
		        	aoData.push({name : 'type_fields', value : options.type_array});
		        	aoData.push({name : 'func_name', value : options.func_name});
		        	aoData.push({name : 'multi', value : options.multi_select});
		        	aoData.push({name : 'value', value : $("#" + options.objID + "_input").val()});
		        	aoData.push({name : 'index_field', value : options.index_field});

		        	$.post(sSource, 
		        	aoData, 
					function(json){
		        		var return_obj = JSON.parse(json[options.objID + '_selected']);
						var return_string = '';
						
						for(var i=0; i<return_obj.length; i++){
							var item = return_obj[i];
							
							if(item['value'] !== undefined){
								return_string += item['name'] + ':' + item['value'] + ',';
							}
						}
						
						if(return_string != ''){
							return_string = return_string.slice(0,-1);
						}
						
						$('#' + options.objID + '_selected').val(return_string);
						
						fnCallback(json);
					}, 
					"json");
		        },
				"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					var no_tds = 0;

					var id = $(nRow).attr('id');

					if(options.multi_select == false || (options.multi_select == true && options.is_disabled == true)){
						$(nRow).find('td').each(function(){
							if(no_tds != 3 && no_tds != 4){
								$(this).bind('click', {'self':self}, function(e){
									var self = e.data['self'];
									
									self.rowClick(id);
								});
							}
	
							no_tds++;
						});
					}
		        },
		        "fnDrawCallback": function( oSettings ) {
			        
		        	$('#' + options.objID + '_search_waiting').hide();
					$('#' + options.objID + '_results_table_div').show();
					$('#' + options.objID + '_results_list').show();
	        		$('.jButton:not(.ui-button)').button();
		        	
		        	if($('#' + options.objID + '_select_all').data('radio') != "true"){
	                	$('#' + options.objID + '_select_all').radio({'checked':false});
	                	$('#' + options.objID + '_select_all').off('radio_click');
	                	$('#' + options.objID + '_select_all').on('radio_click', {'self':self}, function(e,f){
							var self = e.data['self'];
	                    	self.select_all();
	                    	
	                	});
					}
	        		
					$('#' + options.objID + '_results_table tbody div.tickbox').each(function(){
						var id = $(this).closest('tr').attr('id');
						var checked = false;
						
						if($(this).hasClass('ticked_1')){
							checked = true;
						}

						$(this)
							.removeClass('unticked_1 ticked_1')
							.attr('id', options.objID + '_radio_' + id);

						if($('#' + options.objID + '_radio_' + id  + "_answer").length == 0){
							$(this).radio({'checked':checked});
						}

						$(this).on('radioset', {'options': options}, function(e){
							var options = e.data['options'];
							var return_string = '';
							var id = $(this).attr('id');
							var types = $('#' + options.objID + '_selected').val().split(',');

							var value = $('#' + id + '_input').val().replace(/,/g, '');

							id = id.replace(options.objID + '_radio_', '');
							
							for(var i=0; i<types.length; i++){
								var item = types[i].split(':');
								if(item[0] != ''){
									if(item[0] != id){
										return_string += item[0] + ':' + item[1] + ',';
									}
								}
							}

							if(value == 1){
								return_string += id + ':' + value + ',';
							}
							
							return_string = return_string.slice(0,-1);
							
							$('#' + options.objID + '_selected').val(return_string);
						});
					});
					
		        },
		        "aaSorting": sort_array
				});

			$('#' + options.objID + '_results_table_wrapper div.fg-toolbar:first').hide();
		},
	
		/* Set the date(s) directly. */
		set: function(rmc_ref) {

			var self = this;
			var options = this.options;
	
			if(rmc_ref != "" && rmc_ref != "null" && rmc_ref != null){
	
				$("#" + options.objID + "_block").hide();
				$("#" + options.objID + "_loading").show();
				
				$("#" + options.objID + "_input").val(rmc_ref);
			   	$("#" + options.objID + "_select").html(options.buttonText);

			   	var owner_id = '';

				if(options.owner_id != ''){
					owner_id = $('#' + options.owner_id).val();
				}
	
				if(options.multi_select == false){

					var aoData = [];
					aoData.push({name : 'rmc_search_php', value : 'rmc_get'});
		        	aoData.push({name : 'select', value : options.objID});
		        	aoData.push({name : 'rmc_ref', value : rmc_ref});
		        	aoData.push({name : 'owner_id', value : owner_id});
		        	aoData.push({name : 'index_field', value : options.index_field});
					
					$.post("/library/controllers/rmc_selector.controller.php", 
					aoData, 
					function(data){
						var rmc_name = data['rmc_name'];
			
						if (rmc_name == ''){
							rmc_name = options.buttonText;
						}

						if(options.show_ref == true){
							rmc_name += ' (' + data['rmc_ref'] + ')';
						}
						
						$("#" + data['select'] + "_select").html(rmc_name);
						
						if(options.is_disabled == false){
							$('#' + options.objID + '_clear').show();
						}
						
						options.beforeEnd();
						
						$("#" + options.objID + '_info').show();

						$("#" + options.objID + "_loading").hide();
						$("#" + options.objID + "_block").show();
						
						self.recalculate();
						if($('#' + options.objID + '_search_dialog').dialog('isOpen')){
							$('#' + options.objID + '_search_dialog').dialog('close');
						}
						
						if(options.rmc_id != "" && $('#' + options.rmc_id + '_input').val() == ''){
							$('#' + options.rmc_id).rmc_selector('set', data['rmc_num']);
						}

						self._trigger('set', null, data);
					}, 
					"json");
				}
				else{

					var owner_id = '';

					if(options.owner_id != ''){
						owner_id = $('#' + options.owner_id).val();
					}

					var aoData = [];
					aoData.push({name : 'rmc_search_php', value : 'set_multi_rmc'});
		        	aoData.push({name : 'select', value : options.objID});
		        	aoData.push({name : 'rmcs', value : rmc_ref});
		        	aoData.push({name : 'owner_id', value : owner_id});
		        	aoData.push({name : 'button', value : options.buttonText});
		        	aoData.push({name : 'index_field', value : options.index_field});
					
					$.post("/library/controllers/rmc_selector.controller.php", 
					aoData, 
					function(data){
						
						$("#" + options.objID + "_input").val(data['rmcs']);
						
						if (data['num_rows'] > 1){
							$("#" + data['select'] + "_select").html('Multiple Properties');
						}else{
							$("#" + data['select'] + "_select").html(data['results']);
						}
						
						if(options.is_disabled == false){
							$('#' + options.objID + '_clear').show();
						}

						options.beforeEnd();
						
						$("#" + options.objID + '_info').show();

						$("#" + options.objID + "_loading").hide();
						$("#" + options.objID + "_block").show();
						
						self.recalculate();
						if($('#' + options.objID + '_search_dialog').dialog('isOpen')){
							$('#' + options.objID + '_search_dialog').dialog('close');
						}

						self._trigger('set', null, data);
					}, 
					"json");
				}
	
				$("#" + options.link).removeClass('ui-corner-all');
				$("#" + options.link).addClass('ui-corner-left');
			}
			else{	
				this.clear();
			}
		},
		
		rowClick: function(rmc_ref) {
			var self = this;
			var options = this.options;
			
			if(options.func_name != ""){
				options.func_name(rmc_ref);
			}else{
				this.set(rmc_ref);
				$('#' + options.objID + '_search_dialog').dialog('close');
			}
			
			this._trigger('rowclick');
		},

		view: function(rmc_ref) {
			var self = this;
			var options = this.options;

			if(options.is_disabled == false){
				if(options.multi_select == false){
					$('#' + options.objID + '_multi_save').hide();
				}else{
					$('#' + options.objID + '_multi_save').show();
				}
			}else{
				$('#' + options.objID + '_multi_save').hide();
			}
			
			if(!$('#' + options.objID + '_search_dialog').dialog('isOpen')){
			
				self.open();
				if(options.multi_select == false){
					this.do_view(rmc_ref);
				}else{
					var search_filter_array = [];
					var total_fields = $('#' + options.objID + '_search_filter').search_filter('get_no_fields');
					
					for(var k=1;k<=total_fields;k++){
						var current = $('#' + options.objID + '_search_filter').search_filter('get_field', k);
						search_filter_array.push([k, current[0], current[1]]);
					}
					
					$('#' + options.objID + '_search_filter').search_filter('clear_field');
					$('#' + options.objID + '_search_filter').search_filter('set_field', 1, 'rmc_ref', '*');
					$('#' + options.objID + '_search_filter').bind('search_filter_submit', function(){
						for(var k=0;k<search_filter_array.length;k++){
							var item = search_filter_array[k];
							if(item[0] != '1'){
								$('#' + options.objID + '_search_filter').search_filter('add_field');
							}
							$('#' + options.objID + '_search_filter').search_filter('set_field', item[0], item[1], item[2]);
						}
					});
					$('#' + options.objID + '_search_filter').search_filter('submit');
					$('#' + options.objID + '_search_filter').unbind('search_filter_submit');
				}
			}else{
				this.do_view(rmc_ref);
			}
		},

		do_view: function(rmc_ref) {
			var self = this;
			var options = this.options;			

			if(!$('#' + options.objID + '_search_dialog').dialog('isOpen')){
				$('#' + options.objID + '_search_dialog').dialog('open');
			}

			if(options.is_disabled == true && options.multi_select == false){
				$('#' + options.objID + '_slide').slidedeck('disable');
			}else{
				$('#' + options.objID + '_slide').slidedeck('enable');
			}

			//$('#' + options.objID + '_slide').slidedeck('goTo', 1);
			$('#' + options.objID + '_slide').slidedeck('goTo', 4);
			
			$('#' + options.objID + '_details_start').hide();
			$('#' + options.objID + '_details_list').hide();
			$('#' + options.objID + '_details_waiting').show();
			$('#' + options.objID + '_details_no_result').hide();

			var owner_id = '';

			if(options.owner_id != ''){
				owner_id = $('#' + options.owner_id).val();
			}

			var aoData = [];
			aoData.push({name : 'rmc_search_php', value : 'rmc_get'});
        	aoData.push({name : 'select', value : options.objID});
        	aoData.push({name : 'rmc_ref', value : rmc_ref});
        	aoData.push({name : 'owner_id', value : owner_id});
		    aoData.push({name : 'index_field', value : options.index_field});
			
			$.post("/library/controllers/rmc_selector.controller.php", 
			aoData, 
			function(data){

				$('#' + options.objID + '_details_name').html(data['rmc_name']);
				$('#' + options.objID + '_details_ref').html(data['rmc_ref']);
				$('#' + options.objID + '_details_county').html(data['rmc_county']);
				$('#' + options.objID + '_details_postcode').html(data['rmc_postcode']);
				$('#' + options.objID + '_details_co_ho_no').html(data['rmc_co_house_num']);
				$('#' + options.objID + '_details_qube_status').html(data['rmc_status']);
				$('#' + options.objID + '_details_owner').html(data['rmc_company_owner']);
				$('#' + options.objID + '_details_od').html(data['operational_director_name']);
				$('#' + options.objID + '_details_rm').html(data['regional_manager_name']);
				$('#' + options.objID + '_details_pm').html(data['property_manager_name']);
				$('#' + options.objID + '_details_brand').html(data['brand_name']);
				
				$('#' + options.objID + '_details_list').show();
				$('#' + options.objID + '_details_waiting').hide();
				$('#' + options.objID + '_details_no_result').hide();
			}, 
			"json");

			this._trigger('view');
		},
       
		destroy: function() {
			var options = this.options;
			
			$('#' + options.objID + '_block').remove();
			$('#' + options.objID + '_loading').remove();
			$('#' + options.objID + '_a').remove();
			$('#' + options.objID + '_select').remove();
			$('#' + options.objID + '_clear').remove();
			$('#' + options.objID + '_input').remove();

			this._trigger('destroy');

			return $.Widget.prototype.destroy.call( this );
		},

		remove_from_my_portfolio: function(rmc_num){
			var self = this;
			var options = this.options;
			
			var aoData = [];
			aoData.push({name : 'rmc_search_php', value : 'rmc_remove_my_port'});
        	aoData.push({name : 'rmc_num', value : rmc_num});
			
			$.post("/library/controllers/rmc_selector.controller.php", 
			aoData, 
			function(data){
			
				if( data['save_result'] == "success" ){
					
					$('#' + options.objID + '_portfolio_table #' + rmc_num).hide('1500', function(){
						$('#' + options.objID + '_portfolio_table #' + rmc_num).remove();
					});
					
				}
			}, 
			"json");
		},
		
		toggle_my_portfolio: function(rmc_num){
			var self = this;
			var options = this.options;

			var td_no = 3;

			if(options.multi_select == true){
				td_no = 4;
			}
			
			var action = "add";
			if( $('#' + options.objID + '_results_table #' + rmc_num + ' td:eq(' + td_no + ') a').hasClass("my_portfolio_active") ){
				action = "remove";
			}
			if( $('#' + options.objID + '_portfolio_table #' + rmc_num + ' td:eq(' + td_no + ') a').hasClass("my_portfolio_active") ){
				action = "remove";
			}
			
			var aoData = [];
			aoData.push({name : 'rmc_search_php', value : 'rmc_my_port'});
        	aoData.push({name : 'rmc_num', value : rmc_num});
        	aoData.push({name : 'do', value : action});
			
			$.post("/library/controllers/rmc_selector.controller.php", 
			aoData, 
			function(data){
			
				if( data['save_result'] == "success" ){
					if( action == "add" ){
						$('#' + options.objID + '_results_table #' + rmc_num + ' td:eq(' + td_no + ') a').attr("title", "Remove from my portfolio");
						$('#' + options.objID + '_results_table #' + rmc_num + ' td:eq(' + td_no + ') a').removeClass("my_portfolio_inactive").addClass("my_portfolio_active");
					}
					else{
						$('#' + options.objID + '_results_table #' + rmc_num + ' td:eq(' + td_no + ') a').attr("title", "Add to my portfolio");
						$('#' + options.objID + '_results_table #' + rmc_num + ' td:eq(' + td_no + ') a').removeClass("my_portfolio_active").addClass("my_portfolio_inactive");
					}

					options.myApp.table.ajax.reload();
				}
			}, 
			"json");
		}
       
	});
}( jQuery ) );
</script>
<?php require_once($UTILS_SERVER_PATH."includes/templates/rmc_selector_row.tpl.php"); ?>
