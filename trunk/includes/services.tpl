<script language="JavaScript" type="text/JavaScript">

	$(document).ready(function(){
		$("#results_header_table").tablesorter({
			dateFormat: 'uk'
		});
		$("#search_results_div_table").tablesorter({
			dateFormat: 'uk'
		});

		$('#results_header_table').bind("sortEnd",
			function(sorter) {
		    	currentSort = sorter.target.config.sortList;
		    	$("#search_results_div_table").trigger("sorton",[currentSort]);
			}
		);
		$('#contractor_ref' ).val(<?=$contractor_qube_ref;?>);
		do_get();
	});
	
	function save(){
		$("#processing_dialog").dialog('open');
		$('#whichaction').val('check');

		$.post("services.php",
		$("#form1").serialize() + "&service_name_id="+$('#name').val()+"&outercodes_id="+$('#outer_codes').val(),
		function(data){	
			$("#processing_dialog").dialog('close');
			
			if (data.length > 0){
				show_error(data);
			}else{
				$("#add_dialog").dialog('close');
				do_save();
			}
		}, 
		"json");
	}
	
	function do_save(){
		$("#processing_dialog").dialog('open');
		$('#whichaction').val('save');
			
		$.post("services.php",
		$("#form1").serialize() + "&service_name_id="+$('#name').val()+"&outercodes_id="+$('#outer_codes').val(),
		function(data){	
			$("#processing_dialog").dialog('close');
			if(data["success"] == "Y"){
				$('#error_message').html("Successfully Saved!");
				$("#error_dialog").dialog("option", "title", "Saved!");
			}else{
				$('#error_message').html("Apologies. We seem to have a technical fault.");
				$("#error_dialog").dialog("option", "title", "Failed!");
			}
			$("#error_dialog").dialog("open");
			do_get();
		}, 
		"json");
	}
	
	function add_service(){
		$("#processing_dialog").dialog('open');
		$('#whichaction').val('get_service_names');
		var services_select = '';
		$("#name").val('');
		$("#outer_codes").val('');

		$.post("services.php",
				$("#form1").serialize(),
				function(data){
					services_select += '<option value="">Select a Service</option>';
					$.each(data,function(val, name){
						services_select += '<option value="'+val+'">'+name+'</option>';
					})
					$('#name').html(services_select);
					$(".singleSelect").multiselect({
						multiple: false,
						header: false,
						noneSelectedText: "Select a Service",
						selectedList: 1
					}).multiselect('refresh', true);
				},
				"json");

		$('#whichaction').val('load');

		$.post("/admin/contractors.php",
				$("#form1").serialize(),
				function(data1){
					$("#processing_dialog").dialog('close');

					var outercodes = data1['contractor_default_outercodes'];
					var outercodes_flag = false;
					if(outercodes!='') {
						var outercodes_arr = outercodes.split(",");
						outercodes_flag = true;
					}
					$('#whichaction').val('get_outer_codes');
					var outercodes_select = '';
					$.post("services.php",
							$("#form1").serialize(),
							function(data){
								$.each(data,function(val, desc){
									var selected = '';
									if(outercodes_flag) {
										if ( $.inArray( val, outercodes_arr ) != -1 ) {
											selected = ' selected="selected" ';
										}
									}
									outercodes_select += '<option value="' + val+'"'+ selected + '>' + desc + '</option>';
								});
								$('#outer_codes').html(outercodes_select);
								$(".multiSelect").multiselect({
									noneSelectedText: "Select Outercode(s)"
								}).multiselect('refresh', true);
								$("#processing_dialog").dialog('close');
							},
							"json");
				},
				"json");



		$(".ui-selectmenu-button" ).hide();

		var ad = $("#add_dialog").dialog({  
			modal: true,
			width: "500px",
			resizable: false,
			open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
			buttons: {
				Ok: function() {
					save();
				}
			},
			autoOpen: false
		});	
		
		ad.parent().appendTo('.wrapper');	
		
		$('#name').val("");
		$('#outer_codes').val("");
		//$('#disabled_row').hide();
		$("#add_dialog").dialog("option", "title", "Add Service");
		$('#add_dialog').dialog('open');
	}
	
	function edit_service(id){
		
		/*if (id == '<?=$_SESSION['contractors_qube_id'];?>'){
			window.location = "change_details.php";
		}else{ */
			$("#processing_dialog").dialog('open');
			
			var ad = $("#add_dialog").dialog({  
				modal: true,
				width: "500px",
				open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
				buttons: {
					Ok: function() {
						save();
					}
				},
				autoOpen: false
			});	
			
			ad.parent().appendTo('body');	
			
			$('#service_id').val(id);
			$('#whichaction').val('edit');

			
			$.post("services.php",
			$("#form1").serialize(),
			function(data){
				if(data['success'] == 'Y'){

					$('#whichaction').val('get_service_names');
					var services_select = '';
					$.post("services.php",
							$("#form1").serialize(),
							function(data1){
								$( "#processing_dialog" ).dialog( 'close' );
								$( "#processing_dialog" ).dialog( 'open' );
								$(".ui-dialog-titlebar-close").show();
								$.each(data1,function(val, name){
									var selected = '';
									if(val == data['service_name_id']) {
										selected = ' selected="selected" ';
									}
									services_select += '<option value="'+val+'"'+ selected + '>'+name+'</option>';
								})
								$('#name').html(services_select);
								$(".singleSelect").multiselect({
									multiple: false,
									header: false,
									noneSelectedText: "Select a Service",
									selectedList: 1
								});
								$( "#processing_dialog" ).dialog( 'close' );
							},
							"json");
					$("#processing_dialog").dialog('open');
					$('#whichaction').val('get_outer_codes');
					var outercodes_select = '';
					$.post("services.php",
							$("#form1").serialize(),
							function(data2){
								var data2_length = Object.keys(data2).length;
								var i = 1;
								$.each(data2,function(val, desc){
									var selected = '';
									if($.inArray( val ,data.outercodes_id) != -1) {
										selected = ' selected="selected" ';
									}
									outercodes_select += '<option value="' + val+'"'+ selected + '>' + desc + '</option>';
									if( i == data2_length ) {
										$( '#outer_codes' ).html( outercodes_select );
										$( ".multiSelect" ).multiselect( {
											noneSelectedText: "Select Outercode(s)"
										} ).multiselect('refresh');
										$(".ui-selectmenu-button" ).hide();
										$( "#processing_dialog" ).dialog( 'close' );
									}else{
										i++;
									}

								});

							},
							"json");

					$("#add_dialog").dialog("option", "title", "Edit Service");
					$('#add_dialog').dialog('open');
				}
			}, 
			"json");
		//}
	}

	
	function do_get(){
		$("#processing_dialog").dialog('open');
		$('#whichaction').val('get');
		$.post("services.php",
		$("#form1").serialize(), 
		function(data){
			if(data['num_results'] > 0){
			
				tr_template = $("#service_row tbody").html();
				
				results = '';
				for(d=0;d<data['SERVICE_ID'].length;d++){
					var tr = tr_template;
					tr = tr.replace(/SERVICE_ID/g, data['SERVICE_ID'][d]);
					tr = tr.replace(/SERVICE_ONCLICK/g, data['SERVICE_ONCLICK'][d]);
					tr = tr.replace(/SERVICE_NAME/g, data['SERVICE_NAME'][d]);
					tr = tr.replace(/SERVICE_OUTER_CODES/g, data['SERVICE_OUTER_CODES'][d]);
					//tr = tr.replace(/SERVICE_DESCR/g, data['SERVICE_DESCR'][d]);

					results = results + tr;
				}
			}
			else{
				results = $("#no_results_row tbody").html();				
			}

			multi_tablesorter_set("results_header_table", "search_results_div_table", results);

			$("#search_results_div").css("background-color","none !important");

			$("#search_results_div").show();

			var total_view_height = 0;
			
			$("#search_results_div_table tr").each(function(){
				if(total_view_height < 300){
					total_view_height += parseFloat( $(this).css("height").replace("px","") );
				}else{
					return false;
				}
			});
            
			$("#search_results_div").css({			
				"height": total_view_height,
				"overflow":"scroll",
				"overflow-x":"hidden",
				"background-color":"#fff !important"

			});

			$("#processing_dialog").dialog('close');				
		}, 
		"json");
	}
</script>
<div id="add_dialog" title="Add Service" class="dialog_1">
<form id="form_service">
	<table cellspacing="0" width="400">
		<tr>
			<td width="100" style="vertical-align:middle;">
				Name
			</td>
			<td width="300">
				<select id="name" class="singleSelect">
				<!--<input type="text" id="name" name="name" />-->
			</td>
		</tr>
		<tr>
			<td width="200" style="vertical-align:middle;">
				Outercodes
			</td>
			<td width="300">
				<select id="outer_codes" multiple="multiple" class="multiSelect">
				<!--<input type="text" id="outer_codes" name="outer_codes" />-->
			</td>
		</tr>
	</table>
	</form>
</div>
<div class="main">
	<? require_once($UTILS_SERVER_PATH."templates/service_row.tpl");?>
	<form id="form1" method="post">
		<div class="content">
			<p><a href="javascript:add_service();">Add Service</a><br />&nbsp;</p>
			<div class="results_header_table">
				<table id="results_header_table" cellspacing="0">
					<?=$service_data?>
				</table>
			</div>
			<div class="search_results_div" id="search_results_div">
				<table id="search_results_div_table" cellspacing="0" class="hover">
					<?=$service_data ?>
				</table>
			</div>
		</div>
		<input type="hidden" name="whichaction" id="whichaction" value="" />
		<input type="hidden" name="service_id" id="service_id" value="<?=$_REQUEST['service_id'];?>" />
		<input type="hidden" name="contractor_ref" id="contractor_ref" value="<?=$contractor_qube_ref;?>" />
	</form>
</div>