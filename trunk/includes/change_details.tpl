<script language="JavaScript" type="text/JavaScript">

	$(document).ready(function(){
		do_get();
	});

	function save(){
		
		rebuild_dialogs();
		
		var data = new Array();
		var i = 0;
		
		if($('#name').val() == ""){
			data[i] = {"outcome" : true, "message" : "Enter your name in the field provided."};
			i ++;
		}
		if($('#email').val() == ""){
			data[i] = {"outcome" : true, "message" : "Enter your email in the field provided."};
			i ++;
		}
		if($('#old_password').val() == ""){
			data[i] = {"outcome" : true, "message" : "Enter your old password in the field provided."};
			i ++;
		}
		if($('#new_password').val() == ""){
			data[i] = {"outcome" : true, "message" : "Enter your new password in the field provided."};
			i ++;
		}
		if($('#new_password2').val() == ""){
			data[i] = {"outcome" : true, "message" : "Confirm your new password in the field provided."};
			i ++;
		}
		if($('#new_password').val().length < 8){
			data[i] = {"outcome" : true, "message" : "Your password needs to be at least 8 characters."};
			i ++;
		}
		if($('#new_password2').val() != $('#new_password').val()){
			data[i] = {"outcome" : true, "message" : "The new passwords you have entered do not match, please try again."};
			i ++;
		}
		
		if (data.length > 0){
			show_error(data);
		}else{
			$("#processing_dialog").dialog('open');
			$('#which_action').val('check');
			
			$.post("change_details.php", 
			$("#form1").serialize(), 
			function(data){	
				$("#processing_dialog").dialog('close');
				
				if (data["errors"].length > 0){
					show_error(data["errors"]);
				}else{
					if (data["email_match"] == "false"){
						$("#error_dialog").dialog({
							modal: true,
							open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
							buttons: {
								Yes: function() {
									$( this ).dialog( "close" );
									do_save();
								},
								No: function() {
									$( this ).dialog( "close" );
								}
							},
							autoOpen: false
						});
						$('#error_message').html("The email address you have entered does not match our records. Do you wish to use the one you have entered?");
						$("#error_dialog").dialog("option", "title", "Email Question!");
						$("#error_dialog").dialog('open');
					}else{
						do_save();
					}
				}
			}, 
			"json");
		}
	}
	
	function do_save(){
		$("#processing_dialog").dialog('open');
		$('#which_action').val('save');
			
		$.post("change_details.php", 
		$("#form1").serialize(), 
		function(data){	
			$("#processing_dialog").dialog('close');
			
			$("#error_dialog").dialog({
				modal: true,
				open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
				buttons: {
					Ok: function() {
						if ($('#error_message').html() == "Successfully Saved!"){
							<? if($_SESSION['contractors_parent'] != "0"){?>
							window.location = 'contractors.php';
							<? }else{ ?>
							window.location = 'users.php';
							<? } ?>							
						}
						$( this ).dialog( "close" );
					}
				},
				autoOpen: false
			});
			
			if(data["results"] == "0"){
				$('#error_message').html("Successfully Saved!");
				$("#error_dialog").dialog("option", "title", "Saved!");
			}else{
				$('#error_message').html("Apologies. We seem to have a technical fault.");
				$("#error_dialog").dialog("option", "title", "Failed!");
			}
			$("#error_dialog").dialog("open");
		}, 
		"json");
	}
	
	function do_get(){
		$("#processing_dialog").dialog('open');
		$('#which_action').val('get');
		
		$.post("change_details.php", 
		$("#form1").serialize(),
		function(data){	
			$("#processing_dialog").dialog('close');
			if(data['success'] == 'Y'){
				$('#name').val(data['name']);
				$('#email').val(data['email']);
			}
		}, 
		"json");
	}
	
</script>
<div class="main">
	<form id="form1" method="post">
		<div class="content">
			<table width="722" cellspacing="5">
				<tr>
					<td colspan="2">Passwords needs to be at least 8 characters long. </td>
				</tr>
				<tr>
					<td width="300">&nbsp;</td>
					<td width="422">&nbsp;</td>
				</tr>
				<tr>
					<td>Name: </td>
					<td><input name="name" type="text" id="name" /></td>
				</tr>
				<tr>
					<td>Email: </td>
					<td><input name="email" type="text" id="email" /></td>
				</tr>
				<tr>
					<td>Current password: </td>
					<td><input name="old_password" type="password" id="old_password" /></td>
				</tr>
				<tr>
					<td>New password: </td>
					<td><input name="new_password" type="password" id="new_password" /></td>
				</tr>
				<tr>
					<td>Confirm new password: </td>
					<td><input name="new_password2" type="password" id="new_password2" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><input type="button" value="Save" class="button" onclick="save()" /></td>
				</tr>
			</table>
		</div>
		<input type="hidden" name="which_action" id="which_action" value="" />
	</form>
</div>