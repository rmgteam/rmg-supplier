<div class="header_bar">
	<div class="heading">
		<div class="exit" onclick="javascript:$(location).attr('href','/index.php?a=logout');">
		</div>
		<div class="dashboard" onclick="javascript:$(location).attr('href','/contractors.php');">
			<h4>Dashboard</h4>
		</div>
	</div>
</div>

<ul id="menu" class="menu">
	<!-- <li class="ui-state-disabled"><a href="#">Aberdeen</a></li> -->
	<li><a href="<?=(($_SESSION['contractors_parent'] != "0") ? "/change_details.php" : "/users.php");?>">User Details</a></li>
	<li><a href="/po.php">Purchase Orders</a></li>
	<li><a href="/reports.php">Reports</a></li>
	<li><a href="/howto.php">How To Use</a></li>
</ul>
<script language="JavaScript" type="text/JavaScript">
	$(document).ready(function(){
		var timeout;
		$( "#menu" ).menu();
		pos_menu();
		$( "#menu" ).hide();

		$( ".main" ).bind("mouseover", function(e){
			clearTimeout(timeout);
			timeout = setTimeout(function(){
				$( "#menu" ).hide();
			}, 300);
		});
		
		$( "#menu" ).bind("mouseover", function(e){
			clearTimeout(timeout);
		});
		
		$( ".dashboard" ).bind("mouseover", function(e){
			clearTimeout(timeout);
			pos_menu();
			$( "#menu" ).show();
		});

		$('ul.menu > li:last-child').addClass('last');
	});

	function pos_menu(){
		var left = $( ".dashboard" ).offset().left;
		var top = $( ".dashboard" ).offset().top;
		var height = $( ".dashboard" ).height();
		var padding = parseInt($( ".dashboard" ).css("padding-top").replace("px", ""));
		$( "#menu" ).css({
			 "left": left + "px",
			 "top": top + height + padding + "px"
		});
		$( "#menu" ).removeClass("ui-corner-all ui-widget ui-widget-content");
		$( "#menu" ).addClass("ui-corner-bottom");
	}
</script>