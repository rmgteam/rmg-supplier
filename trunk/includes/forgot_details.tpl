<script type="text/JavaScript">

	function do_forgot(){
		$("#processing_dialog").dialog('open');
		
		$.post("forgot_details.php", 
		$("#form1").serialize(), 
		function(data){	
			$("#processing_dialog").dialog('close');
			
			if (data[0]['success'] == false){
				show_error(data);
				do_change();
				$('#captcha_code').val('');
			}else{
				$(location).attr('href','/');
			}
		}, 
		"json");
	}

	function do_change(){
		$('#captcha').attr('src', '/library/securimage/securimage_show.php?' + Math.random());
		return false;
	}
	
	$(document).ready(function(){
		$('.dashboard').hide();
		$('.exit').hide();
	});
	
</script>
<div class="main">
	<form id="form1" method="post">
		<div class="content">
			<input type="hidden" name="a" id="a" value="s">

			<p>Ooops, so you've forgotten your login details ... </p>
			
			<div style="width:100%;margin-top:20px;">
				<p>Enter your details into the fields below.</p>					
				<table class="table_3" cellspacing="0" style="margin-top:5px;width:100%">
					<tr>
						<td style="width:280px;">Email address:</td>
						<td><input type="text" name="email" id="email" value="<?=$_REQUEST['email']?>" size="40" maxlength="100" /></td>
					</tr>
					<tr>
						<td colspan="2" style="padding:5px 0;">
							Which part of your login details do you want to retreive ?
						</td>
					</tr>
					<tr>
						<td style="padding:0 0 8px 0;"><input onclick="toggle_up()" type="radio" name="up_type" id="up_type_1" value="password" <? if($_REQUEST['up_type'] != "username"){?>checked="checked"<? }?> />&nbsp;Password</td>
						<td style="padding:0 0 8px 0;"><input onclick="toggle_up()" type="radio" name="up_type" id="up_type_2" value="username" <? if($_REQUEST['up_type'] == "username"){?>checked="checked"<? }?> />&nbsp;Username</td>
					</tr>
					<tr>
						<td style="width:280px;">Enter the characters that displayed below into the box provided.</td>
						<td>
							<input type="text" id="captcha_code" name="captcha_code" size="10" maxlength="6" />
							<div style="position:relative;float:left;margin: 7px 0 0 10px;">
								[&nbsp;<a href="#" onclick="do_change();">Change Image</a>&nbsp;]
							</div>
						</td>
					</tr>
				</table>
			</div>
			
			<div style="position:relative; margin-top:20px;">
				<p>These characters are case-sensitive.</p>					
				<div style="position:relative;float:left;width:100%;">
					<img id="captcha" src="/library/securimage/securimage_show.php" alt="CAPTCHA Image" style="clear:both;margin-top:15px;" />
				</div>
			</div>
			
			<div style="float:left; position:relative; margin-top:20px;">
				<input type="button" onclick="do_forgot();" id="forgot_button" class="button" value="Submit" />
			</div>
			
		</div>
	</form>
</div>