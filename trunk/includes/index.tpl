<script type="text/javascript">
	function do_login(){
		$('#a').val("login");
				
		$("#processing_dialog").dialog('open');
		
		$.post("index.php", 
		$("#form_login").serialize(), 
		function(data){
			
			if (data['result'] == "success"){
				$(location).attr('href',data[0]['message']);
			}else{
				$("#processing_dialog").dialog('close');
			}

			var arr = new Array();
			arr.push(data[0]);
			
			show_error(arr);
		}, 
		"json");
	}
	
	function browser_aware(){
		$("#browser_dialog").dialog('open');
	}

	$(document).ready(function(){
		$("#login_pass_input").bind('keypress', 
			function(e){
				var code = (e.keyCode ? e.keyCode : e.which);
				if(code == 13){ 
					do_login();
				}
			}
		);
	});
</script>
	<div class="main">
		<div class="index_box">
			<div class="image_box">
			</div>
			<div class="login_box">
				<div class="login_header">
					<h2>Login</h2>
				</div>
				<form name="form_login" id="form_login">
					<input type="hidden" id="a" name="a" />
					<div class="login_area">
						<h4>Username</h4>
						<input type="text" id="login_user_input" name="login_user_input" class="shadow2 login" />
						<h4>Password</h4>
						<input type="password" id="login_pass_input" name="login_pass_input" class="login" />
					</div>
					<input type="button" onclick="do_login();" id="login_submit_button" class="button" value="Login" /><br />
					<a href="forgot_details.php">Forgotten your password?</a>
				</form>
			</div>
		</div>
		<div class="content">
			<hr class="hr_grey" />
			<h1>Welcome to RMG Suppliers</h1>
			<hr class="hr_grey" />
			<a class="index_third twitter" href="https://twitter.com/rmgltd" target="_blank">
			</a>
			<!--[if lte ie 9]>
			<style>
				.qtip-rmg-blue .qtip-titlebar {
					width: 235px;
				}
			</style>
			<a class="index_third browser" href="javascript:browser_aware();">
			</a>
			<![endif]-->
			<a class="index_third rewards" href="http://rmgltd.co.uk/rewards" target="_blank">
			</a>
		</div>
	</div>
</div>