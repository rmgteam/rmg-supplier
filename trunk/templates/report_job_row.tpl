<table id="report_job_row" class="template_include">
	<thead>
		<tr>
			<th width="100">Job Number</th>
			<th width="200">Expected Completion</th>
			<th width="200">Quote</th>
			<th width="200">Closed By</th>
			<th class="end">On</th>
		</tr>
	</thead>
	<tbody>
		<tr id="report_job_row_JOB_ID">
			<td class="hover" width="100">JOB_NUMBER</td>
			<td class="hover" width="200">JOB_DATE</td>
			<td class="hover" width="200">JOB_AMOUNT</td>
			<td class="hover" width="200">JOB_CLOSE</td>
			<td class="hover end">JOB_ON</td>
		</tr>
	</tbody>
</table>