<table id="po_row" class="template_include">
	<thead>
		<tr>
			<th width="50">PO No.</th>
			<th width="20"></th>
			<th width="20"></th>
			<th width="50">Brand</th>
			<th width="300">Property</th>
			<th width="310">Description</th>
			<th width="110">Estimated Date</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="dataTables_empty">Loading</td>
			<td class="dataTables_empty"></td>
			<td class="dataTables_empty"></td>
			<td class="dataTables_empty">Data</td>
			<td class="dataTables_empty">From</td>
			<td class="dataTables_empty">Server</td>
			<td class="dataTables_empty"></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th><input type="text" value="Search PO Number." class="search_init" /></th>
			<th></th>
			<th></th>
			<th><input type="text" value="Search Brand" class="search_init" /></th>
			<th><input type="text" value="Search Property" class="search_init" /></th>
			<th><input type="text" value="Search Description" class="search_init" /></th>
			<th><input type="text" value="Search Date" class="search_init" /></th>
		</tr>
	</tfoot>
</table>