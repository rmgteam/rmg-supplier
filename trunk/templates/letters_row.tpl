<table id="letter_row" class="template_include">
	<thead>
		<tr>
			<th width="20">
				<div id="results_table_all" data-toggle="tooltip" data-title="Select All" data-tooltip="Select all Contractors on all pages">
				</div>
			</th>
			<th width="100">Qube Ref.</th>
			<th width="420">Contractor Name</th>
			<th width="280">Email</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="dataTables_empty">Loading</td>
			<td class="dataTables_empty">Data</td>
			<td class="dataTables_empty">From</td>
			<td class="dataTables_empty">Server</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th>&nbsp;</th>
			<th><input type="text" value="Search Ref." class="search_init" /></th>
			<th><input type="text" value="Search Name" class="search_init" /></th>
			<th><input type="text" value="Search Email" class="search_init" /></th>
		</tr>
	</tfoot>
</table>