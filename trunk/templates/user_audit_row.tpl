<table id="audit_row" class="template_include">
	<thead>
		<tr>
			<th width="300">Name</th>
			<th width="300">Login Date/Time</th>
			<th class="end">Login Location</th>
		</tr>
	</thead>
	<tbody>
		<tr id="user_row_USER_ID">
			<td class="hover" width="300">USER_NAME</td>
			<td class="hover" width="300">USER_DATE</td>
			<td class="hover end">USER_LOCATION</td>
		</tr>
	</tbody>
</table>