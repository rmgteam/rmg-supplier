<table id="user_row" class="template_include">
	<thead>
		<tr>
			<th width="80">Username</th>
			<th width="120">Name</th>
			<th width="350">Email</th>
			<th width="80">Disabled</th>
			<th width="80">No. Logins</th>
			<th class="end">Last Login</th>
		</tr>
	</thead>
	<tbody>
		<tr id="user_row_USER_ID" onClick="USER_ONCLICK ">
			<td class="hover" width="80">USER_USERNAME</td>
			<td class="hover" width="120">USER_NAME</td>
			<td class="hover" width="350">USER_EMAIL</td>
			<td class="hover" width="80">USER_DISABLED</td>
			<td class="hover" width="80">USER_COUNT</td>
			<td class="hover end">USER_LAST</td>
		</tr>
	</tbody>
</table>