<html>
<body>
	<font face="verdana" size="2">
		<p>Dear <?=$contractors->contractor_name; ?>, <br /></p>
		<p><b>Make life easier through RMG Suppliers!</b><br /></p>
		<p>Welcome to RMG Suppliers!  RMG is always looking for ways to improve the service we offer to our customers and would like to take this opportunity to introduce the on-line contractors portal, RMG Suppliers.<br /></p>
		<p>From today, you can use RMG Suppliers to:<br /></p>
		<ul>
			<li>Manage multiple users to use your account</li>
			<li>Manage your Purchase Orders</li>
		</ul>
		<p>The following login details are specific for the master account of your company. To access your account visit <a href="http://www.rmgsuppliers.co.uk">www.rmgsuppliers.co.uk</a> and use the details below:<br /></p>
		<table cellpadding="0" cellspacing="0" border="0">
			<tbody>
				<tr>
					<td style="width: 100px;"><b>Login Id:</b></td>
					<td><?=$contractors->contractor_qube_id?></td>
				</tr>
				<tr>
					<td style="width: 100px;"><b>Password:</b></td>
					<td><?=$password?></td>
				</tr>
			</tbody>
		</table>
		<p><br />The first time you login you will asked to set your own password. If you forget your password you can use the password reminder facility on the homepage of the website. Please retain this letter as a reference of your Contractor ID.<br /></p>
		<p>This is the first of many steps which forms part of our continued drive to improve the service we deliver to all of our customers. We hope you enjoy the current features and would be very interested to hear of other features that you think may be of benefit to you. Please forward any ideas or general feedback on RMG Suppliers to <a href="mailto:contractors@rmg.gb.com">contractors@rmg.gb.com</a><br /></p>
		<p>Please find attached a user guide that will assist you in your initial logon, managing the Purchase Orders, running reports and creating additional accounts, which we hope you will find useful.</p>
		<p><br />Yours sincerely,<br /></p>
		<p><br />Residential Management Group Ltd<br /></p>
	</font>
</body>
</html>