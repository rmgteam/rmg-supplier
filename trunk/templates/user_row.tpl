<table id="user_row" class="template_include">
	<thead>
		<tr>
			<th width="300">Username</th>
			<th width="300">Name</th>
			<th class="end">Email Address</th>
		</tr>
	</thead>
	<tbody>
		<tr id="user_row_USER_ID" onclick="USER_ONCLICK ">
			<td class="hover" width="300">USER_USERNAME</td>
			<td class="hover" width="300">USER_NAME</td>
			<td class="hover end">USER_EMAIL</td>
		</tr>
	</tbody>
</table>