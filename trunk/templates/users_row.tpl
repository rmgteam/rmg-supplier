<table id="user_row" class="template_include">
	<thead>
		<tr>
			<th width="250">Username</th>
			<th width="250">Name</th>
			<th width="320">Email Address</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="dataTables_empty">Loading</td>
			<td class="dataTables_empty">Data From</td>
			<td class="dataTables_empty">Server</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th><input type="text" value="Search Username." class="search_init" /></th>
			<th><input type="text" value="Search Name" class="search_init" /></th>
			<th><input type="text" value="Search Email" class="search_init" /></th>
		</tr>
	</tfoot>
</table>