<?
	require("utils.php");
	require_once($UTILS_CLASS_PATH."encryption.class.php");
	require_once($UTILS_CLASS_PATH."field.class.php");
	require_once($UTILS_CLASS_PATH."contractors.class.php");
	require_once($UTILS_CLASS_PATH."mysql.class.php");

	Global $UTILS_CLASS_PATH;
	Global $UTILS_SERVER_PATH;
	Global $UTILS_LOG_PATH;
	Global $UTILS_WEBROOT;
	Global $UTILS_URL_BASE;

	$crypt = new encryption_class;
	$field = new field;

	$page_array = explode('/', $_SERVER['PHP_SELF']);

	$contractor_qube_ref_encoded = str_replace( '.php', '', $page_array[count( $page_array ) - 1] );
	$contractor_qube_ref = base64_decode( $contractor_qube_ref_encoded );
	if(empty($contractor_qube_ref)) {
		$contractor_qube_ref = $_SESSION['contractors_qube_id'];
	}
	$contractors = new contractors($contractor_qube_ref);
	$mysql = new mysql();

#===================================
# Get users
#===================================

	if($_REQUEST['whichaction'] == "edit"){
		$service_id = $_REQUEST['service_id'];

		$result = $contractors->get_service($service_id);
		$num_rows = $mysql->num_rows($result);
		if($num_rows > 0){
			$result_array['success'] = 'Y';
			while($row = $mysql->fetch_array($result)){
				$result_array['service_name_id'] = $row['cpm_contractors_service_service_id'];
				if( strpos($row['cpm_contractors_service_outercode_ids'],',')!==false ) {
					$result_array['outercodes_id'] = explode(',',$row['cpm_contractors_service_outercode_ids']);
				}else{
					$result_array['outercodes_id'] = array($row['cpm_contractors_service_outercode_ids']);
				}
			}
		}else{
			$result_array['success'] = 'N';
		}
		echo json_encode($result_array);
		exit;
	}

#===================================
# Get users
#===================================

	if($_REQUEST['whichaction'] == "get"){
		$result = $contractors->get_services($_REQUEST['contractor_ref']);
		$num_rows = $mysql->num_rows($result);
		$output = "";
		$result_array = Array();
		$result_array['num_results'] = $num_rows;
		$i = 0;

		$outer_codes_arr = $contractors->get_outercodes_array();

		if($num_rows > 0){
			while($row = $mysql->fetch_array($result)){
				$result_array['SERVICE_ID'][$i] = $row['cpm_contractors_service_id'];
				$result_array['SERVICE_ONCLICK'][$i] = 'edit_service('."'".$row['cpm_contractors_service_id']."'".');';
				$result_array['SERVICE_NAME'][$i] = $row['service_name'];
				$outercodes_str = '';
				if(strpos($row['service_outercodes_ids'],',') !== false) {
					$service_codes_arr = explode(',',$row['service_outercodes_ids']);

					foreach($service_codes_arr as $id ){
						$outercodes_str .= ',<span title="'.$outer_codes_arr[$id]['desc'].'">'.$outer_codes_arr[$id]['code'].'</span>';
					}
					$outercodes_str = trim($outercodes_str,',');
				}else{//only one outercode
					$id = $row['service_outercodes_ids'];
					$outercodes_str = '<span title="'.$outer_codes_arr[$id]['desc'].'">'.$outer_codes_arr[$id]['code'].'</span>';
				}

				$result_array['SERVICE_OUTER_CODES'][$i] = $outercodes_str;
				//$result_array['SERVICE_DESCR'][$i] = $contractors->get_service_descs($row['cpm_contractors_service_id']);
				$i++;
			}
		}
		echo json_encode($result_array);
		exit;
	}
#===================================
# Get Service Names
#===================================

	if($_REQUEST['whichaction'] == "get_service_names"){

		$result_array = $contractors->get_service_names();

		echo json_encode($result_array);
		exit;
	}
#===================================
# Get Outercodes
#===================================

	if($_REQUEST['whichaction'] == "get_outer_codes"){

		$result_array = $contractors->get_outer_codes();

		echo json_encode($result_array);
		exit;
	}

#===================================
# Check new user
#===================================

	if($_REQUEST['whichaction'] == "check"){

		$result_array = $contractors->check_service($_REQUEST);

		echo json_encode($result_array);
		exit;
	}


#===================================
# Save
#===================================

	if($_REQUEST['whichaction'] == "save"){
		$result_array = $contractors->save_service($_REQUEST);

		echo json_encode($result_array);
		exit;
	}

	$template = "backend";
	$page = str_replace('.php','',$page_array[count($page_array) - 2]);
	$tpl = new Template($UTILS_SERVER_PATH.'includes/body.tpl');

	$tpl->set('page', $page);
	$tpl->set('title', 'RMG Suppliers - Services');
	$tpl->set('page_title', 'Services');
	$tpl->set('contractor_qube_ref', $contractor_qube_ref);
	$tpl->set('UTILS_WEBROOT', $UTILS_WEBROOT);
	$tpl->set('UTILS_LOG_PATH',$UTILS_LOG_PATH);
	$tpl->set('UTILS_CLASS_PATH', $UTILS_CLASS_PATH);
	$tpl->set('UTILS_URL_BASE', $UTILS_URL_BASE);
	$tpl->set('UTILS_SERVER_PATH', $UTILS_SERVER_PATH);
	$tpl->set('service_data', $tpl->set_sortable_table($UTILS_SERVER_PATH."templates/service_row.tpl"));
	$header = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'_header.tpl');
	$content = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'.tpl');
	$page_details = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$page.'.tpl');
	$tpl->set('header', $header);
	$tpl->set('content', $content.$page_details);
	echo $tpl->fetch();
?>