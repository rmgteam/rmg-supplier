<?
ini_set("max_execution_time","7200");
require_once("utils.php");
require_once($UTILS_CLASS_PATH."po.class.php");
require_once($UTILS_CLASS_PATH."mysql.class.php");
require_once($UTILS_CLASS_PATH."contractors.class.php");
require_once($UTILS_CLASS_PATH."webservice.class.php");
require_once($UTILS_CLASS_PATH."letter.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_SERVER_PATH."includes/pdf_styles.php");

$sql = "SELECT *
FROM cpm_po_job j
INNER JOIN cpm_po p ON j.cpm_po_job_po_id = p.cpm_po_id
INNER JOIN cpm_lookup_rmcs l ON l.rmc_ref = p.cpm_po_rmc_id
INNER JOIN cpm_subsidiary s ON s.subsidiary_code = p.cpm_po_brand
INNER JOIN cpm_rmcs r ON r.rmc_num = l.rmc_lookup AND s.subsidiary_id = r.subsidiary_id
LEFT JOIN cpm_po_reason f ON f.cpm_po_reason_id = j.cpm_po_job_reason_id
WHERE cpm_po_id = '".$_REQUEST['poid']."'";

$result = mysql_query($sql, $conn);
$num_rows = @mysql_num_rows($result);
if($num_rows > 0) {
	$row = @mysql_fetch_array($result);

	$contractor_qube_ref = $row['cpm_po_contractors_ref'];
	$rmc_ref = $row['rmc_ref'];
	$po_number = $row['cpm_po_number'];
	$price = $row['cpm_po_job_amount'];
	$description = $row['cpm_po_description'];
	$subsidiary_code = $row['subsidiary_code'];

	$target_date = new DateTime($row['cpm_po_job_completion_date']);
	$target_date = $target_date->format('d/m/Y');

	$date_raised = new DateTime($row['cpm_po_date_raised']);
	$date_raised = $date_raised->format('d/m/Y');

	$group = $row['subsidiary_ecs_login_group'];
}

$service = new webservice();

/// Get contractor info
$params 								= array();
$params['params']['ref']				= $contractor_qube_ref;
$params['class']						= "contractors";
$params['method']						= "expose_by_ref";
$params['username']						= $GLOBALS['webservice']['username'];
$params['password']						= $GLOBALS['webservice']['password'];

$uri									= $GLOBALS['webservice']['location'];
$response 								= $service->curlWrapper($uri, "POST", json_encode($params));
$doc_array								= json_decode($response['output'], true);

$contractor_name = $doc_array['output']['contractor_name'];

$contractor_address = $doc_array['output']['contractor_address_string'];
$contractor_address = str_replace('<br/>', "\r\n", $contractor_address);

$contractor_phone = $doc_array['output']['contractor_tel_1'];
$contractor_fax = $doc_array['output']['contractor_fax'];
$contractor_email = $doc_array['output']['contractor_email_1'];

/// Get subsidiary info
$params 								= array();
$params['params']['code']				= $subsidiary_code;
$params['class']						= "subsidiary";
$params['method']						= "expose_by_code";
$params['username']						= $GLOBALS['webservice']['username'];
$params['password']						= $GLOBALS['webservice']['password'];

$uri									= $GLOBALS['webservice']['location'];
$response 								= $service->curlWrapper($uri, "POST", json_encode($params));
$doc_array								= json_decode($response['output'], true);

/// Get rmc info
$params 								= array();
$params['params']['ref']				= $rmc_ref;
$params['params']['subsidiary_id']		= $doc_array['output']['subsidiary_id'];
$params['class']						= "rmc";
$params['method']						= "expose_by_ref";
$params['username']						= $GLOBALS['webservice']['username'];
$params['password']						= $GLOBALS['webservice']['password'];

$uri									= $GLOBALS['webservice']['location'];
$response 								= $service->curlWrapper($uri, "POST", json_encode($params));
$doc_array								= json_decode($response['output'], true);

$property_ref = $rmc_ref;
$property_name = $doc_array['output']['rmc_name'];
$property_address_array = array($doc_array['output']['row']['rmc_address_1'], $doc_array['output']['row']['rmc_address_2'], $doc_array['output']['row']['rmc_county'], $doc_array['output']['row']['rmc_city'], $doc_array['output']['row']['rmc_postcode']);
$pm = $doc_array['output']['property_manager_name'];
$owner_name = $doc_array['output']['rmc_company_owner'];
$property_address = '';

foreach($property_address_array as $address_item){
	if(!is_null($address_item)){
		if($address_item != ''){
			$property_address .= $address_item . "\r\n";
		}
	}
}

$data = '<parameters>
<Order-Number>'.$po_number.'</Order-Number>
</parameters>';

$webservice = new webservice;
$security = new security;
$session_id = $security->gen_serial(16);
$xml_result = $webservice->qube_execute(1, 'getorddet', $data, $group, $session_id);

$ch = curl_init();

if(!is_null($xml_result['code'])){
	$output_html = "XML is invalid/Curl is unavailable. Http response: " . $xml_result['code'] . " XML = " . $xml_result['desc'];
}else {
	$return_code = $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['order-data'][0]['code'];

	if($return_code != "00"){ //service unavailable
		$result_array['result'] = "Error code: " . $return_code . " - " . $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['order-data'][0]['desc'];
	}else{
		$num_trans = $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['order-data'][0]['recordcount'];

		if($num_trans == 0 || $num_trans == ""){
			echo "There is no information for this PO.";
			exit;
		} else {
			$transact_array = $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['order-data'][0]['order-details'];
		}
	}
}
curl_close($ch);
//$webservice->logout($session_id);

$asbestos = $transact_array[0]['asbestos'];
$comments = $transact_array[0]['job-details-1'][0]['job-notes'];

$letter = new letter('P', 'mm', 'A4');
$letter->SetMargins(12.7, 0, 12.7);
$letter->SetDefaultFont('Arial', '', 10);
$letter->setStyle("b","Arial","B");
$letter->setStyle("i","Arial","I");
$letter->bMargin = 15;
$letter->include_header = true;
$letter->header_template = "headed_paper";
$letter->header_title = 'Purchase Order';
$letter->line_height = 4.5;
$letter->header_email = 'customer.service@rmgltd.co.uk';
$letter->header_phone = '0345 002 4444';

$letter->include_header = true;
$letter->SetMargins(12.7, 0, 12.7);
$letter->add_page();

$replacement_array = array();

$description = str_replace("\r\n", '<br/>', $description);

$letter->parse_html('<p><b>This purchase order is placed on behalf of our client:</b><br /><b>'.$owner_name.'</b></p>');

$table = new tfpdfTable($letter);
$table->initialize(array(45, 47, 45, 47), $table_no_border_config);

$t_row = array();
$t_row[0]['TEXT'] = 'Contractors Address:';
$t_row[0]['TEXT_TYPE'] = 'B';
$t_row[0]['COLSPAN'] = 2;
$t_row[2]['TEXT'] = 'Date PO issued:';
$t_row[2]['TEXT_TYPE'] = 'B';
$t_row[3]['TEXT'] = $date_raised;
$t_row[3]['TEXT_TYPE'] = '';
$table->addRow($t_row);

$t_row = array();
$t_row[0]['TEXT'] = '<p>' . $contractor_name . "\r\n" . $contractor_address . '</p>';
$t_row[0]['TEXT_TYPE'] = '';
$t_row[0]['ROWSPAN'] = 5;
$t_row[0]['COLSPAN'] = 2;
$t_row[2]['TEXT'] = 'Qube PO No:';
$t_row[2]['TEXT_TYPE'] = 'B';
$t_row[3]['TEXT'] = $po_number;
$t_row[3]['TEXT_TYPE'] = '';
$table->addRow($t_row);

$t_row = array();
$t_row[2]['TEXT'] = 'Target Completion Date:';
$t_row[2]['TEXT_TYPE'] = 'B';
$t_row[3]['TEXT'] = $target_date;
$t_row[3]['TEXT_TYPE'] = '';
$table->addRow($t_row);

$t_row = array();
$t_row[2]['TEXT'] = 'Property Manager:';
$t_row[2]['TEXT_TYPE'] = 'B';
$t_row[3]['TEXT'] = $pm;
$t_row[3]['TEXT_TYPE'] = '';
$table->addRow($t_row);

$t_row = array();
$t_row[2]['TEXT'] = 'Property/Site Address:';
$t_row[2]['TEXT_TYPE'] = 'B';
$t_row[2]['COLSPAN'] = 2;
$table->addRow($t_row);

$t_row = array();
$t_row[2]['TEXT'] = '<p>' . $property_name . "\r\n" . $property_address . '</p>';
$t_row[2]['TEXT_TYPE'] = '';
$t_row[2]['COLSPAN'] = 2;
$table->addRow($t_row);

$table->close();

$letter->ln(3);

//$letter->parse_html('<p>If you have any queries relating to this purchase order please contact the Customer Contact Centre by emailing<br /><b><a href="mailto:rmgsupport@rmg.gb.com">rmgsupport@rmg.gb.com</a></b> or alternatively <b>01606 336134</b> quoting the purchase order number.</p>');
$letter->parse_html('<p><b>Work Order contains important information on a) the new CDM 2015 Regulations and</b></p>');
$letter->parse_html('<p><b>b) asbestos - all operatives are to read this information prior to commencing the tasking</b></p>');


$letter->ln(3);

$letter->line(10,$letter->y,200,$letter->y);

$letter->ln(3);

$letter->parse_html('<p><b>Description of Works:</b> '.$description.'<br /></p>');

if(count($comments) > 0) {
	$letter->parse_html('<p><b>Additional Comments:</b></p>');

	$paragraph = '';

	foreach($comments as $comment) {
		if($comment['text'] != '<br/>') {
			$paragraph .= html_entity_decode($comment['text']);
		}
	}

	$letter->parse_html('<p>' . $paragraph . '</p>');
}

if($letter->y < 225) {
	$letter->y = 225;
}

$letter->ln(3);

$letter->line(10,$letter->y,200,$letter->y);

$letter->ln(3);

$letter->parse_html('<p><b>This Purchase order authorises work to a maximum cost of £' . $price .' excluding VAT.</b> Should the estimated cost exceed this, or if you have any queries, please contact RMG on the details above.</p>');
$letter->Ln(1);

$letter->parse_html('<p><i>Please include the Purchase Order Number:</i> <b>'.$po_number.'</b> <i>and Property Ref:</i> <b>'.$rmc_ref.'</b> <i>on your invoice, failure to record this information will result in your invoice being returned.</i></p>');
$letter->Ln(1);
$letter->parse_html('<p><b>May we remind you that invoices will not be paid unless the Purchase Order has been completed on the Supplier' . "'" .'s Portal, or notification of completion has been sent to <a href="mailto:RMGSupport@rmg.gb.com">RMGSupport@rmg.gb.com</a>.</b>');
$letter->Ln(1);
$letter->parse_html('<p>Please post invoices to our Hoddesdon address or email your invoice to: <b><a href="mailto:postroom@rmgltd.co.uk">postroom@rmgltd.co.uk</a></b> and ensure all invoices are addressed as follows:</p>');
$letter->Ln(1);
$letter->parse_html('<p><b>'.$owner_name.' c/o Residential Management Group Ltd </b> <br /><b>RMG House, Essex Road, Hoddesdon Herts. EN11 0DR</b></p>');

$letter->add_page();

$letter->parse_html('<p>A Risk Assessment, directly connected with this specific job instruction, has not yet been undertaken on this property and contractors are advised that they must undertake their own assessment of risk and provide instruction to their staff before the commencement of work. A copy must be forwarded to our offices upon completion.<br /></p>');

$letter->parse_html('<p><b>Asbestos</b></p>');
if($asbestos == "No Site Visit") {
	$letter->parse_html('<p><b>IMPORTANT NOTE:</b> An Asbestos Management Survey has <b>NOT</b> been carried out on this site. As such the presence of asbestos cannot be confirmed or discounted but should be presumed as being present throughout the structure. If the works specified will require any disturbance to the fabric of the building then a full \'suitable and sufficient\' risk assessment must be carried out before works begin. If in any doubt you should contact our Contractors team in order that an appropriate Asbestos Management Survey or Refurbishment and Demolition (RAD) Survey can be carried out prior to any works being undertaken. Being in possession of a copy of the relevant asbestos survey will allow you to fulfil your health & safety responsibilities to your staff and to the residents. Copies of contractors risk assessments should be forwarded to the Contractors Team for our records, contractors@rmg.gb.com.<br /></p>');
}elseif($asbestos == "No Asbestos") {
	$letter->parse_html('<p><b>IMPORTANT NOTE:</b> An Asbestos Management Survey has been carried out on this site and asbestos was not found to be present within the none intrusive limitations of the survey, however, if a refurbishment is planned, then a full Refurbishment and Demolition survey (RAD) must be carried out prior to commencement of work. If you do have any concerns that asbestos may be on site then a full \'suitable and sufficient\' risk assessment must be carried out before works begin and the Property Manager should be contacted. <br /></p>');
}if($asbestos == "Asbestos Present") {
	$letter->parse_html('<p><b>IMPORTANT NOTE:</b> An Asbestos Management Survey has been carried out on this site and asbestos is confirmed, or presumed, as being present in the structure. If the works specified will require any disturbance to the fabric of the building or if it will require entry to areas where asbestos is presumed, known and/or is signed as present, then a full \'suitable and sufficient\' risk assessment must be carried out before works begin. Contact our Contractors team for all copies of relevant documentation to allow you to fulfil your responsibilities to your staff and to the residents and please forward a copy of the risk assessment to the Contractors Team for our records, contractors@rmg.gb.com.<br /></p>');
}if($asbestos == "Post 2000") {
	$letter->parse_html('<p>An asbestos management survey has not been undertaken on this development as it was built post 2000 and hence is not required. If you do have any concerns that asbestos may be on site then a full \'suitable and sufficient\' risk assessment must be carried out before works begin and the Property Manager should be contacted.<br /></p>');
}

$letter->parse_html('<p><b>Construction (Design and Management) Regulations 2015</b></p>');

$letter->parse_html('<p>Please note that the CDM 2015 regulations now apply to ALL construction and maintenance projects regardless of the size, duration and nature of the work. Routine maintenance work will, therefore, not necessarily be exempt from the regulations.<br /></p>');

$letter->parse_html('<p>As part of this Purchase Order, RMG request that you inform us at the outset if the work is likely to be judged as <b>\'Construction Work\'</b> as defined in the regulations. The HSE has also advised the following regarding maintenance;<br /></p>');

$letter->parse_html('<p>"The definition of maintenance work has not changed. If the task in hand looks like construction work, requires construction skills and uses construction materials, it is construction work. General maintenance of fixed plant which mainly involves mechanical adjustments, replacing parts or lubrication is unlikely to be construction work".<br /></p>');

$letter->parse_html('<p>Please note that CDM 2015 places a legal responsibility on our Client to ensure a suitable <b>Construction Phase Plan (CPP)</b> is drawn up for all \'construction work\'-related projects both notifiable and non-notifiable (to the HSE). The Plan should be proportionate and risk-based with the content being representative of the nature and extent of the project.<br /></p>');

$letter->parse_html('<p>Please be aware that although it is the Client\'s responsibility to ensure a CPP is in place before the project commences it is your duty, as the appointed contractor, to produce the Plan if you deem the work you are about to undertake as being \'Construction work\'.<br /></p>');

$letter->parse_html('<p>For single contractor projects the CPP should be prepared by the contractor and forwarded to contractors@rmg.gb.com. RMG recommend that contractor staff consider using the CDM Wizard App which is designed to make it easier for small businesses to prepare themselves for the CDM Regulations. The App is available to download at: http://www.citb.co.uk/en-GB/Health-Safety-and-other-topics/Health-Safety/construction-design-and-management-regulations/CDM-Wizard-App.<br /></p>');

$letter->parse_html('<p>For projects involving more than 1 contractor, the Client must appoint (in writing) a Principal Designer and Principal Contractor who must take the lead in terms of preparing the CPP for the Client. The Client has a responsibility during this phase to ensure that a CPP is drawn up before the construction site is set up. The Principal Designer has a duty to provide any relevant information to assist the Principal Contractor in producing the CPP.<br /></p>');

$letter->parse_html('<p>For those contractors that do not have access to smart phone/tablet technology RMG has devised a MS Word version of the CPP for contractors to complete and return to RMG/our client prior to the commencement of works. Download the MS Word version of the CPP using this link http://rmgltd.co.uk/services/private/health-safety.htm and return to contractors@rmg.gb.com<br /></p>');

$letter->add_page();

$letter->parse_html('<p><b>Work at height</b></p>');
$letter->parse_html('<p>All contractor staff are to note that the Working At Height Regulations (WAHR) 2005 applies to all work at height whenever there is a risk of a fall likely to cause injury. As part of the WAHR duty holders, including employers of contractor staff, must:<br /></p>');

$letter->parse_html('<ul><li>Carry out a formal risk assessment</li><li>Ensure all work at height is properly planned, organised and takes into account weather conditions</li><li>Ensure those involved in work at height are trained and competent</li><li>Ensure equipment for work at height is appropriately inspected and records maintained</li><li>Ensure risks associated with fragile surfaces are properly controlled</li></ul>');
$letter->parse_html('<p>All contractor staff are to ensure that the legal duties outlined above are adhered to and that no work is undertaken at height if it is reasonably practicable to do it in another, safer, way.<br /></p>');

$letter->parse_html('<p><b>Data Protection Act 1998</b></p>');
$letter->parse_html('<p>In line with the requirements of The Data Protection Act 1998 we expect you to maintain policies and procedures to comply with the 8 Principles. All responsibility for compliance lies with you in respect of any data you find yourself in possession of in the course of your contracted work for Residential Management Group Ltd.<br /></p>');

$letter->parse_html('<p><b>Bribery Act</b></p>');
$letter->parse_html('<p>As per the requirements of The Bribery Act 2010. To assist RMG to comply with the Act we would expect you to adhere with the standards as stated in RMG\'s policy and guidance.<br /></p>');
$letter->parse_html('<p><b>Further details regarding all the above regulations and policies can be found at:</b><br /></p>');
$letter->parse_html('<p>http://rmgltd.co.uk/services/private/health-safety.htm<br /></p>');
$letter->parse_html('<p>Should you require keys and/or codes for access to the development please contact our Customer Contact Centre on <b>0345 002 4444</b><br /></p>');
$letter->parse_html('<p><b>Contractor Requirements Specific to the Receipt of all Gas and Electrical Purchase Orders</b></p>');
$letter->parse_html('<p>All contractor staff are to note that by accepting this specific job instruction, they accept responsibility and accountability, to ensure they, and their staff working at the property address, are fully qualified and certified, in accordance with legal requirements, to undertake the specific job instruction. Failure to comply with this requirement will result in RMG no longer recommending you, the contractor, to receive purchase orders to undertake specific job instructions.</p>');


$letter->Output();
