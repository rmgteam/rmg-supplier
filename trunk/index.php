<?php

require("utils.php");
require_once($UTILS_CLASS_PATH."login.class.php");
$login = new login;

Global $UTILS_CLASS_PATH;
Global $UTILS_SERVER_PATH;
Global $UTILS_LOG_PATH;
Global $UTILS_WEBROOT;
Global $UTILS_URL_BASE;
Global $UTILS_HTTPS_ADDRESS;

//================================================
// Log off routine
//================================================
if($_REQUEST['a'] == "logout"){
	
	$login->do_logout();
	
	header("Location: ".$UTILS_HTTPS_ADDRESS."index.php");
	exit;
}

$site_inactive = "N";
if( $site_inactive == "Y" ){
	require_once($UTILS_FILE_PATH."holding_page.php");
	exit;
}

//================================================
// Log in routine
//================================================
if($_REQUEST['a'] == "login"){
	
	$result_array = array();

	if($_REQUEST['login_user_input'] == "" || $_REQUEST['login_pass_input'] == ""){
		$result_array[0]['message'] = "Please enter both of your login credentials";
		$result_array[0]['outcome'] = true;
		$result_array['result'] = 'fail';
	}
	else{
		$r_array = $login->do_contractor_login($_REQUEST['login_user_input'], $_REQUEST['login_pass_input']);
		
		if( $r_array['outcome'] === false ){
			$result_array['result'] = 'success';
			if($_SESSION['contractors_last_login'] == ''){
				$r_array['message'] = $UTILS_HTTPS_ADDRESS."change_details.php";
			}else{
				$datetime = new DateTime();
				$today = $datetime->format('Ymd');
				/*if($_SESSION['contractors_next_change'] == ""){
					$r_array['message'] = $UTILS_HTTPS_ADDRESS."change_details.php";
				}elseif($_SESSION['contractors_next_change'] <= $today){
					$r_array['message'] = $UTILS_HTTPS_ADDRESS."change_details.php";
				}else{*/
					$r_array['message'] = $UTILS_HTTPS_ADDRESS."contractors.php";
				//}
			}
		}
		else{
			$result_array['result'] = 'fail';
			if( $r_array['message'] == "" ){
				$r_array['message'] = "Incorrect login details";
			}
		}
		$result_array[0] = $r_array;
	}
	
	echo json_encode($result_array);
	exit;
}

$template = "index";

$tpl = new Template($UTILS_SERVER_PATH.'includes/body.tpl');
$tpl->set('title', 'RMG Suppliers');
$tpl->set('UTILS_WEBROOT', $UTILS_WEBROOT);
$tpl->set('UTILS_LOG_PATH',$UTILS_LOG_PATH);
$tpl->set('UTILS_CLASS_PATH', $UTILS_CLASS_PATH);
$tpl->set('UTILS_URL_BASE', $UTILS_URL_BASE);
$tpl->set('UTILS_SERVER_PATH', $UTILS_SERVER_PATH);
$header = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'_header.tpl');
$content = $tpl->get_content($UTILS_SERVER_PATH.'includes/'.$template.'.tpl');
$tpl->set('header', $header);
$tpl->set('content', $content);
echo $tpl->fetch();

?><!-- a -->