<?
ini_set("max_execution_time","7200");
require_once("utils.php");
require_once($UTILS_CLASS_PATH."po.class.php");
require_once($UTILS_CLASS_PATH."mysql.class.php");
require_once($UTILS_CLASS_PATH."contractors.class.php");
require_once($UTILS_CLASS_PATH."webservice.class.php");
require_once($UTILS_CLASS_PATH."letter.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_SERVER_PATH."includes/pdf_styles.php");

$sql = "SELECT *
FROM cpm_po_job j
INNER JOIN cpm_po p ON j.cpm_po_job_po_id = p.cpm_po_id
INNER JOIN cpm_lookup_rmcs l ON l.rmc_ref = p.cpm_po_rmc_id
INNER JOIN cpm_subsidiary s ON s.subsidiary_code = p.cpm_po_brand
INNER JOIN cpm_rmcs r ON r.rmc_num = l.rmc_lookup AND s.subsidiary_id = r.subsidiary_id
LEFT JOIN cpm_po_reason f ON f.cpm_po_reason_id = j.cpm_po_job_reason_id
WHERE cpm_po_id = '".$_REQUEST['poid']."'";

$result = mysql_query($sql, $conn);
$num_rows = @mysql_num_rows($result);
if($num_rows > 0) {
	$po = new po();
	$service = new webservice();
	$row = @mysql_fetch_array($result);
	$group = $row['subsidiary_esc_login_group'];
	$onbase = $po->getSOWRef( $row['cpm_po_number'], $group );

	if( !is_null( $onbase ) ) {
		$params = array();
		$params['params'] = array();
		$params['params']['ref'] = $onbase;
		$params['class'] = "onbase";
		$params['method'] = "downloadRef";
		$params['username'] = $GLOBALS['webservice']['username'];
		$params['password'] = $GLOBALS['webservice']['password'];

		$uri = $GLOBALS['webservice']['location'];
		$response = $service->curlWrapper( $uri, "POST", json_encode( $params ) );
		$file = $response['output'];

		header( 'Cache-Control: public' );
		header( 'Content-Disposition: inline; filename="SOW.pdf"' );
		header( 'Content-type: application/pdf' );
		header( 'Content-Length: ' . strlen( $file ) );
		echo $file;
	}else{
		var_dump('document not found');
	}
}else{
	var_dump ('error');
}
