<?php
require_once("utils.php");
require_once($UTILS_CLASS_PATH."webservice.class.php");

$service = new webservice();

if($_REQUEST['rmc_search_php'] == "rmc_get"){
	
	$index_field = $_REQUEST['index_field'];
	
	$rmc_num = $_REQUEST['rmc_ref'];

	$params 								= array();

	if($index_field == 'rmc_num'){
		$params['method']					= "expose_by_num";
	}elseif($index_field == 'rmc_ref'){

		$rmc_array 							= explode('-', $rmc_num);
		$rmc_num 							= $rmc_array[0];
		$subsidiary_code 					= $rmc_array[1];

		/// Get subsidiary info
		$params1 							= array();
		$params1['params']['code']			= $subsidiary_code;
		$params1['class']					= "subsidiary";
		$params1['method']					= "expose_by_code";
		$params1['username']				= $GLOBALS['webservice']['username'];
		$params1['password']				= $GLOBALS['webservice']['password'];

		$uri								= $GLOBALS['webservice']['location'];
		$response 							= $service->curlWrapper($uri, "POST", json_encode($params1));
		$doc_array							= json_decode($response['output'], true);

		$params['method']					= "expose_by_ref";
		$params['params']['subsidiary_id']	= $subsidiary_code;
	}
	
	$params['params']['ref']				= $rmc_num;
	$params['class']						= "rmc";
	$params['username']						= $GLOBALS['webservice']['username'];
	$params['password']						= $GLOBALS['webservice']['password'];
	
	$uri									= $GLOBALS['webservice']['location'];
	$response 								= $service->curlWrapper($uri, "POST", json_encode($params));
	$doc_array								= json_decode($response['output'], true);
	$result_array							= $doc_array['output'];
	
	$result_array['select'] 				= $_REQUEST['select'];
	
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['rmc_search_php'] == "rmc_search"){
	
	$params 								= array();
	
	foreach ($_REQUEST as $key=>$value){
		$params['params']['request'][$key]	= $value;
	}
	
	$params['params']['excl_nla']			= false;
	$params['params']['exclude_gr']			= false;
	$params['params']['pm_name']			= "";
	$params['params']['rm_name']			= "";
	$params['params']['od_name']			= "";
	$params['class']						= "rmc";
	$params['method']						= "get_results";
	$params['username']						= $GLOBALS['webservice']['username'];
	$params['password']						= $GLOBALS['webservice']['password'];
	
	$uri									= $GLOBALS['webservice']['location'];
	$response 								= $service->curlWrapper($uri, "POST", json_encode($params));
	$doc_array								= json_decode($response['output'], true);
	
	$result_array							= $doc_array['output'];

	if(count($result_array['aaData']) > 0){
		foreach($result_array['aaData'] as &$row){
			$row['DT_RowId'] = $row['DT_RowId'] . '-' . $row[1];
			$row[5] = str_replace('jButton', 'mini_button', $row[5]);
		}
	}

	$result_array['select'] = $_REQUEST['select'];
	$result_array[$_REQUEST['select'] . '_selected'] = $_REQUEST[$_REQUEST['select'] . '_selected'];
	
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['rmc_search_php'] == "get_all"){
	
	$request = $_REQUEST;
	$request['user_id'] = $_SESSION['user_id'];
	$request['iDisplayLength'] = "-1";
	$request['iDisplayStart'] = 0;
	
	$params 								= array();
	
	foreach ($request as $key=>$value){
		$params['params']['request'][$key]	= $value;
	}
	
	$params['params']['excl_nla']			= false;
	$params['params']['exclude_gr']			= false;
	$params['params']['pm_name']			= "";
	$params['params']['rm_name']			= "";
	$params['params']['od_name']			= "";
	$params['class']						= "rmc";
	$params['method']						= "get_results";
	$params['username']						= $GLOBALS['webservice']['username'];
	$params['password']						= $GLOBALS['webservice']['password'];
	
	$uri									= $GLOBALS['webservice']['location'];
	$response 								= $service->curlWrapper($uri, "POST", json_encode($params));
	$doc_array								= json_decode($response['output'], true);
	$result_array							= $doc_array['output'];
	
	if(count($result_array['aaData']) > 0){
		foreach($result_array['aaData'] as &$row){
			$row['DT_RowId'] = $row['DT_RowId'] . '-' . $row[1];
			$row[5] = str_replace('jButton', 'mini_button', $row[5]);
		}
	}
	
	$selected = '';
	
	foreach($result_array['aaData'] as $result){
		$selected .= $result['DT_RowId'] . ':1,';
	}
	
	if($selected != ''){
		$selected = substr($selected, 0, -1);
	}
	
	$result_array['select'] = $_REQUEST['select'];
	$result_array['values'] = $selected;
	
	echo json_encode($result_array);
	
	exit;
}

if ($_REQUEST['rmc_search_php'] == "set_multi_rmc"){

	$index_field = $_REQUEST['index_field'];

	$params 								= array();

	if($index_field == 'rmc_num'){
		$params['method']					= "get_rmc_name_by_num";
	}elseif($index_field == 'rmc_ref'){
		$params['method']					= "get_rmc_name_by_ref";
	}
	
	$params['params']['rmc_ref']			= $_REQUEST['rmcs'];
	$params['class']						= "rmc";
	$params['username']						= $GLOBALS['webservice']['username'];
	$params['password']						= $GLOBALS['webservice']['password'];
	
	$uri									= $GLOBALS['webservice']['location'];
	$response 								= $service->curlWrapper($uri, "POST", json_encode($params));
	$doc_array								= json_decode($response['output'], true);
	$html_string							= $doc_array['output'];

	$contractor_array						= explode(',', $_REQUEST['rmcs']);

	$result_array['results'] = $html_string;
	$result_array['select'] = $_REQUEST['select'];
	$result_array['rmcs'] = $_REQUEST['rmcs'];
	$result_array['num_rows'] = count($contractor_array);
	
	echo json_encode($result_array);
	exit;
}


?>