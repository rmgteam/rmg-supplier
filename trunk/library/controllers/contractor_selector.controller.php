<?php
require_once("utils.php");
require_once($UTILS_CLASS_PATH."webservice.class.php");

Global $GLOBALS;

$service = new webservice();

if ($_REQUEST['contractor_search_php'] == "get_details"){
	
	$params 								= array();
	$params['params']['ref']				= $_REQUEST['contractor_ref'];
	$params['class']						= "contractors";
	$params['method']						= "expose_by_ref";
	$params['username']						= $GLOBALS['webservice']['username'];
	$params['password']						= $GLOBALS['webservice']['password'];
	
	$uri									= $GLOBALS['webservice']['location'];
	$response 								= $service->curlWrapper($uri, "POST", json_encode($params));
	$doc_array								= json_decode($response['output'], true);
	$result_array							= $doc_array['output'];
	
	$result_array['select'] 				= $_REQUEST['select'];
	
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['contractor_search_php'] == "get_contractor"){
	
	$params 								= array();
	
	foreach ($_REQUEST as $key=>$value){
		$params['params']['request'][$key]	= $value;
	}
	
	$params['params']['multi']				= $_REQUEST['multi'];
	$params['params']['output']				= 'datatables';
	$params['class']						= "contractors";
	$params['method']						= "get_results";
	$params['username']						= $GLOBALS['webservice']['username'];
	$params['password']						= $GLOBALS['webservice']['password'];
	
	$uri									= $GLOBALS['webservice']['location'];
	$response 								= $service->curlWrapper($uri, "POST", json_encode($params));
	$doc_array								= json_decode($response['output'], true);
	$result_array							= $doc_array['output'];
	
	if(count($result_array['aaData']) > 0){
		foreach($result_array['aaData'] as &$row){
			$row[3] = str_replace('jButton', 'mini_button', $row[3]);
		}
	}
	$result_array['select'] = $_REQUEST['select'];
	$result_array[$_REQUEST['select'] . '_selected'] = $_REQUEST[$_REQUEST['select'] . '_selected'];
	
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['contractor_search_php'] == "get_all"){
	
	$request = $_REQUEST;
	$request['user_id'] = $_SESSION['user_id'];
	$request['iDisplayLength'] = "-1";
	$request['iDisplayStart'] = 0;
	
	$params 								= array();
	
	foreach ($request as $key=>$value){
		$params['params']['request'][$key]	= $value;
	}
	
	$params['params']['multi']				= $_REQUEST['multi'];
	$params['params']['output']				= 'datatables';
	$params['class']						= "contractors";
	$params['method']						= "get_results";
	$params['username']						= $GLOBALS['webservice']['username'];
	$params['password']						= $GLOBALS['webservice']['password'];
	
	$uri									= $GLOBALS['webservice']['location'];
	$response 								= $service->curlWrapper($uri, "POST", json_encode($params));
	$doc_array								= json_decode($response['output'], true);
	$result_array							= $doc_array['output'];
	
	if(count($result_array['aaData']) > 0){
		foreach($result_array['aaData'] as &$row){
			$row[3] = str_replace('jButton', 'mini_button', $row[3]);
		}
	}
	
	foreach($result_array['aaData'] as $result){
		$selected .= $result['DT_RowId'] . ':1,';
	}
	
	if($selected != ''){
		$selected = substr($selected, 0, -1);
	}
	
	$result_array['select'] = $_REQUEST['select'];
	$result_array['values'] = $selected;
	
	echo json_encode($result_array);
	
	exit;
}

if ($_REQUEST['contractor_search_php'] == "set_multi_contractor"){

	$contractor_field = $_REQUEST['contractor_ref'];
	
	$params 								= array();
	$params['params']['contractor_ref']		= $contractor_field;
	$params['class']						= "contractors";
	$params['method']						= "get_contractor_name_by_ref";
	$params['username']						= $GLOBALS['webservice']['username'];
	$params['password']						= $GLOBALS['webservice']['password'];
	
	$uri									= $GLOBALS['webservice']['location'];
	$response 								= $service->curlWrapper($uri, "POST", json_encode($params));
	$doc_array								= json_decode($response['output'], true);
	$html_string							= $doc_array['output'];

	$contractor_array						= explode(',', $contractor_field);
	
	$result_array['results'] 				= $html_string;
	$result_array['select'] 				= $_REQUEST['select'];
	$result_array['contractor_ref'] 		= $_REQUEST['contractor_ref'];
	$result_array['num_rows'] 				= count($contractor_array);

	echo json_encode($result_array);
	exit;
}

?>
