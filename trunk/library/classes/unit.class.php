<?
//======================================================
// Class to extract and manipulate unit data
//======================================================
require_once($UTILS_CLASS_PATH."subsidiary.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");



class unit {

	var $unit_ref;
	var $rmc_num;
	var $subsidiary_id;
	var $rmc_ref;
	var $resident_num;
	var $unit_description;
	var $unit_address_1;

	function set_unit($ref="", $ref_type="resident"){
		
		if($ref != ""){
			
			if($ref_type == "unit"){
				$ref_clause = " u.unit_num = ".$ref." AND ";	
			}
			else{
				$ref_clause = " u.resident_num = ".$ref." AND ";
			}
			
			$sql = "
			SELECT * 
			FROM cpm_lookup_units lu, cpm_units u LEFT JOIN cpm_lookup_rmcs lrmc ON (u.rmc_num = lrmc.rmc_lookup) 
			WHERE 
			".$ref_clause." 
			lu.unit_lookup = u.unit_num 
			";
			$result = @mysql_query($sql);
			$this->unit = @mysql_fetch_array($result);
			
			@mysql_data_seek($result, 0);
			$row = @mysql_fetch_array($result);
			
			$this->unit_ref = $row['unit_ref'];
			$this->rmc_num = $row['rmc_num'];
			$this->subsidiary_id = $row['subsidiary_id'];
			$this->rmc_ref = $row['rmc_ref'];
			$this->unit_description = $row['unit_description'];
			$this->unit_address_1 = $row['unit_address_1'];
			
		}
		else{
			return false;
		}
	}
	
	function get_unit_desc(){
		
		if($this->unit_description != ""){return trim($this->unit_description);}
		if($this->unit_address_1 != ""){return trim($this->unit_address_1);}
	}
	
	function set_unit_address($nlbr = "<br>"){
		
		if($this->unit['unit_address_1'] != ""){$unit_address_array[] = trim($this->unit['unit_address_1']);}
		if($this->unit['unit_address_2'] != ""){$unit_address_array[] = trim($this->unit['unit_address_2']);}
		if($this->unit['unit_address_3'] != ""){$unit_address_array[] = trim($this->unit['unit_address_3']);}
		if($this->unit['unit_city'] != ""){$unit_address_array[] = trim($this->unit['unit_city']);}
		if($this->unit['unit_county'] != ""){$unit_address_array[] = trim($this->unit['unit_county']);}
		if($this->unit['unit_postcode'] != ""){$unit_address_array[] = trim($this->unit['unit_postcode']);}
		if($this->unit['unit_country'] != ""){$unit_address_array[] = trim($this->unit['unit_country']);}
		
		for($a=0;$a<(count($unit_address_array)-1);$a++){
			$this->unit_address .= $unit_address_array[$a].$nlbr;
		}
		$this->unit_address .= $unit_address_array[$a];
	}
	
	function get_unit_address(){
		return $this->unit_address;
	}
	
	
	// Check for number of Unit-related schedules (search Intranet db). No need to match resident_num as we are only 
	// checking for the presence of unit-related schedules, not whether the resident has any credits.
	function get_num_unit_ins_sched(){ 
		
		global $UTILS_INTRANET_DB_LINK;
		
		$subsidiary = new subsidiary($this->subsidiary_id);
		
		// Get intranet-based subsidiary_id
		$sql_s = "
		SELECT subsidiary_id 
		FROM subsidiary 
		WHERE 
		subsidiary_code <> '' AND 
		subsidiary_code IS NOT NULL AND 
		subsidiary_code = '".$subsidiary->subsidiary_code."'";
		//print $sql_s;
		$result_s = @mysql_query($sql_s, $UTILS_INTRANET_DB_LINK);
		$row_s = @mysql_fetch_row($result_s);
		$int_subsidiary_id = $row_s[0];
		
		// Get intranet-based rmc_num
		$sql_rmc = "
		SELECT rmc_lookup 
		FROM lookup_rmc 
		WHERE 
		rmc_ref <> '' AND 
		rmc_ref IS NOT NULL AND 
		rmc_ref = '".$_SESSION['rmc_ref']."' AND 
		subsidiary_id = ".$int_subsidiary_id." 
		";
		$result_rmc = @mysql_query($sql_rmc, $UTILS_INTRANET_DB_LINK);
		$row_rmc = @mysql_fetch_row($result_rmc);
		$int_rmc_num = $row_rmc[0];
		
		// Get intranet-based unit_num
		$sql_u = "
		SELECT unit_lookup 
		FROM lookup_unit 
		WHERE 
		unit_ref = '".$this->unit_ref."' AND 
		rmc_num = ".$int_rmc_num." AND 
		subsidiary_id = ".$int_subsidiary_id." 
		";
		//print $sql_u;
		$result_u = @mysql_query($sql_u, $UTILS_INTRANET_DB_LINK);
		$row_u = @mysql_fetch_row($result_u);
		$int_unit_num = $row_u[0];
		
		
		$sql = "
		SELECT count(*) 
		FROM ins_sched i, ins_sched_unit_assoc ua 
		WHERE 
		i.ins_sched_id = ua.ins_sched_id AND 
		ua.unit_num = ".$int_unit_num." AND 
		i.ins_sched_reviewed = 'N' AND 
		i.ins_sched_discon = 'N' AND 
		i.ins_sched_is_visible = 'Y' AND 
		i.ins_sched_applicable_to = 1 AND 
		i.ins_sched_filename <> '' AND 
		i.ins_sched_filename IS NOT NULL 
		";
		//print $sql;
		$result = @mysql_query($sql, $UTILS_INTRANET_DB_LINK);
		$row = @mysql_fetch_row($result);		
		return $row[0];
	}
	
	
	// Check for number of Unit-related schedules (search Intranet db). The second part of the main  
	// select statement will get the number of extra credits that this resident has 
	function get_unit_ins_sched(){
		
		global $UTILS_INTRANET_DB_LINK;
		
		$subsidiary = new subsidiary($this->subsidiary_id);
		
		// Get intranet-based subsidiary_id
		$sql_s = "
		SELECT subsidiary_id 
		FROM subsidiary 
		WHERE 
		subsidiary_code <> '' AND 
		subsidiary_code IS NOT NULL AND 
		subsidiary_code = '".$subsidiary->subsidiary_code."'";
		$result_s = @mysql_query($sql_s, $UTILS_INTRANET_DB_LINK);
		$row_s = @mysql_fetch_row($result_s);
		$int_subsidiary_id = $row_s[0];
		
		// Get intranet-based rmc_num
		$sql_rmc = "
		SELECT rmc_lookup 
		FROM lookup_rmc 
		WHERE  
		rmc_ref <> '' AND 
		rmc_ref IS NOT NULL AND 
		rmc_ref = '".$_SESSION['rmc_ref']."' AND 
		subsidiary_id = ".$int_subsidiary_id." 
		";
		$result_rmc = @mysql_query($sql_rmc, $UTILS_INTRANET_DB_LINK);
		$row_rmc = @mysql_fetch_row($result_rmc);
		$int_rmc_num = $row_rmc[0];
		
		// Get intranet-based unit_num 
		$sql_u = "
		SELECT unit_lookup 
		FROM lookup_unit 
		WHERE 
		unit_ref = '".$this->unit_ref."' AND 
		rmc_num = ".$int_rmc_num." AND 
		subsidiary_id = ".$int_subsidiary_id." 
		";
		$result_u = @mysql_query($sql_u, $UTILS_INTRANET_DB_LINK);
		$row_u = @mysql_fetch_row($result_u);
		$int_unit_num = $row_u[0];
		
		// Get intranet-based resident_num 
		$sql_r = "
		SELECT resident_lookup 
		FROM lookup_resident 
		WHERE 
		resident_ref = '".$_SESSION['resident_ref']."' AND 
		subsidiary_id = ".$int_subsidiary_id." 
		";
		$result_r = @mysql_query($sql_r, $UTILS_INTRANET_DB_LINK);
		$row_r = @mysql_fetch_row($result_r);
		$int_resident_num = $row_r[0];
		
		$sql = "
		SELECT *, '".$int_resident_num."' AS int_resident_num, (SELECT count(*) FROM ins_sched_download_log d WHERE d.resident_num = ".$int_resident_num." AND d.unit_num = ".$int_unit_num." AND d.ins_sched_id = i.ins_sched_id) AS num_downloaded 
		FROM ins_sched_unit_assoc ua, ins_sched_types t, ins_sched i LEFT JOIN rmcs r ON (i.rmc_num = r.rmc_num ) LEFT JOIN freeholders f ON (i.ins_sched_freeholder_id = f.freeholder_id ) 
		WHERE 
		i.ins_sched_id = ua.ins_sched_id AND 
		i.ins_sched_type_id = t.ins_sched_type_id AND 
		ua.unit_num = ".$int_unit_num." AND  
		i.ins_sched_reviewed = 'N' AND 
		i.ins_sched_discon = 'N' AND 
		i.ins_sched_is_visible = 'Y' AND 
		i.ins_sched_applicable_to = 1 AND 
		i.ins_sched_filename <> '' AND 
		i.ins_sched_filename IS NOT NULL 
		";	
		$result = @mysql_query($sql, $UTILS_INTRANET_DB_LINK);
		return $result;
	}
	
}

?>