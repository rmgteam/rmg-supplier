<?php

Global $UTILS_CLASS_PATH;


$name='DejaVuSans-Bold';
$type='TTF';
$desc=array (
  'Ascent' => 928,
  'Descent' => -236,
  'CapHeight' => 928,
  'Flags' => 262148,
  'FontBBox' => '[-1069 -415 1975 1174]',
  'ItalicAngle' => 0,
  'StemV' => 165,
  'MissingWidth' => 600,
);
$up=-63;
$ut=44;
$ttffile = $UTILS_CLASS_PATH.'pdf/tfpdf/font/unifont/DejaVuSans-Bold.ttf';
$originalsize=672300;
$fontkey='dejavusansB';
?>