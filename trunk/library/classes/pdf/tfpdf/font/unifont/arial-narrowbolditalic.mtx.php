<?php

Global $UTILS_CLASS_PATH;


$name='ArialNarrow-BoldItalic';
$type='TTF';
$desc=array (
  'Ascent' => 936,
  'Descent' => -212,
  'CapHeight' => 936,
  'Flags' => 262212,
  'FontBBox' => '[-204 -307 1000 1107]',
  'ItalicAngle' => -9.6999969482422,
  'StemV' => 165,
  'MissingWidth' => 778,
);
$up=-106;
$ut=105;
$ttffile = $UTILS_CLASS_PATH.'pdf/tfpdf/font/unifont/Arial-NarrowBoldItalic.ttf';
$originalsize=180084;
$fontkey='arialNBI';
?>