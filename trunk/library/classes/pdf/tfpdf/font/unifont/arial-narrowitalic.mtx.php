<?php

Global $UTILS_CLASS_PATH;


$name='ArialNarrow-Italic';
$type='TTF';
$desc=array (
  'Ascent' => 936,
  'Descent' => -212,
  'CapHeight' => 936,
  'Flags' => 68,
  'FontBBox' => '[-214 -307 1000 1048]',
  'ItalicAngle' => -9.6999969482422,
  'StemV' => 87,
  'MissingWidth' => 228,
);
$up=-106;
$ut=73;
$ttffile = $UTILS_CLASS_PATH.'pdf/tfpdf/font/unifont/Arial-NarrowItalic.ttf';
$originalsize=181124;
$fontkey='arialNI';
?>