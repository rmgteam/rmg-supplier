<?php
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."unit.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."field.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."mysql.class.php");
require_once($UTILS_CLASS_PATH."log.class.php");

class login {
	
	function do_admin_login($username="", $password=""){
		
		Global $UTILS_DB_ENCODE;
		
		$result_array = array();
		
		$mysql = new mysql;
		$security = new security;
		$crypt = new encryption_class();
		
		$sql_backend = "
		SELECT p.manager, 
		p.first_name, 
		p.last_name, 
		b.f_name, 
		b.l_name, 
		b.user_id, 
		b.is_sys_admin,
		b.is_first_logon,
		b.user_type_id,
		b.email,
		b.allow_pms, 
		b.allow_rmc, 
		b.allow_residents, 
		b.allow_passwords, 
		b.allow_letters, 
		b.allow_news, 
		b.allow_users, 
		b.allow_report, 
		b.allow_developers, 
		b.password,
		allow_letters_module, 
		allow_announcements,
		allow_contractor,
		allow_letters_toggle
		FROM cpm_backend b 
		LEFT JOIN cpm_pmt_members p ON b.member_id=p.member_id
		WHERE LOWER(b.user_name) = '".$security->clean_query($username)."'
		AND b.password <> '' 
		AND b.password = '".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $password))."'";
		//print $sql_backend;
		$result_backend = $mysql->query($sql_backend, 'Check Login');
		$num_backed_users = $mysql->num_rows($result_backend);
		
		if($num_backed_users > 0){
		
			$row_backend = $mysql->fetch_array($result_backend);
			
			
			if($row_backend['allow_contractor'] == '1'){
				$result_array['outcome'] = false;
				
				$_SESSION['admin_session'] = session_id();
				$_SESSION['admin_user_id'] = $row_backend['user_id'];
				$_SESSION['admin_user_name'] = $row_backend['f_name'] . ' ' . $row_backend['l_name'];
			}else{
				$result_array['outcome'] = true;
				$result_array['message'] = "You do not have sufficient privileges to access RMG Suppliers Admin";
			}
			
		}else{
			$result_array['outcome'] = true;
			$result_array['message'] = "Your username or password is incorrect.";
		}
		
		return $result_array;
	}
		
	function do_contractor_login($username="", $password="", $override=false){
		
		global $UTILS_DB_ENCODE;
		$mysql = new mysql();
		$security = new security;
		$crypt = new encryption_class;
		$resident = new resident;
		$result_array = array();
		
		if($override == false){
			$sql_where = "
			AND u.cpm_contractors_user_password = '".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $password))."'";
		}
		
		// Firstly, check for resident login
		$sql_contractor = "
		SELECT u.*,c.cpm_contractors_disabled, 
		IFNULL(
			(
				SELECT t.cpm_contractors_user_trail_login
				FROM cpm_contractors_user_trail t
				WHERE t.cpm_contractors_user_trail_user_ref = u.cpm_contractors_user_ref
				ORDER BY t.cpm_contractors_user_trail_id DESC
				LIMIT 1
			),
			''
		)as last_login
		FROM cpm_contractors_user u
		INNER JOIN cpm_contractors c
		ON c.cpm_contractors_qube_id = u.cpm_contractors_user_qube_ref
		WHERE 
		u.cpm_contractors_user_ref = '".$username."'" . $sql_where;

		
		$result_contractor = $mysql->query($sql_contractor, 'Get Contractor');
		$num_valid_users = $mysql->num_rows($result_contractor);
		if($num_valid_users > 0){
		
			$row_contractor = $mysql->fetch_array($result_contractor);
			
			if( $row_contractor['cpm_contractors_user_disabled'] == 'True' ){
				$result_array['outcome'] = true;
				$result_array['message'] = "This account is no longer active";
			}elseif( $row_contractor['cpm_contractors_disabled'] == 'True' ){
				$result_array['outcome'] = true;
				$result_array['message'] = "This contractor is no longer active";
			}
			else{
	
				$data = new data;
				
				$_SESSION['contractor_session'] = session_id();
				$_SESSION['contractors_username'] = $row_contractor['cpm_contractors_user_ref'];
				$_SESSION['contractors_parent'] = $row_contractor['cpm_contractors_user_parent'];
				$_SESSION['contractors_qube_id'] = $row_contractor['cpm_contractors_user_qube_ref'];
				$_SESSION['contractors_last_login'] = $row_contractor['last_login'];
				
				/*if($row_contractor['cpm_contractors_user_password_ts'] != ""){
				
					$sql_days = "SELECT * FROM cpm_password_days";
					
					$result_days = $mysql->query($sql_days, 'Get No Days');
					$num_days = $mysql->num_rows($result_days);
					if($num_days > 0){
					
						$row_days = $mysql->fetch_array($result_days);
						$_SESSION['contractors_next_change'] = $data->date_plus_days($row_contractor['cpm_contractors_user_password_ts'], $row_days['cpm_password_days'], "Ymd", "Ymd");
						
					}
				}else{
					$_SESSION['contractors_next_change'] = "";	
				}*/
				
				$datetime = new DateTime(); 
				
				if ($row_contractor['last_login'] != ""){
				
					$sql_insert = "INSERT INTO cpm_contractors_user_trail SET
					cpm_contractors_user_trail_login = '".$datetime->format('Y-m-d-H-i-s')."',
					cpm_contractors_user_trail_user_ref = '".$row_contractor['cpm_contractors_user_ref']."',
					cpm_contractors_user_trail_ip = '".$_SERVER["REMOTE_ADDR"]."'";
					$has_error = $mysql->insert($sql_insert, 'Insert User Trail');
				}
				
				$result_array['outcome'] = false;
			}
		}else{
			if(isset($_SESSION['contractor_failed_ref'])){
				if ($_SESSION['contractor_failed_ref'] != $username){
					$_SESSION['contractor_failed'] = 0;	
				}
			}
			
			$_SESSION['contractor_failed_ref'] = $username;	
			
			if(isset($_SESSION['contractor_failed'])){
				$_SESSION['contractor_failed'] = $_SESSION['contractor_failed'] + 1;
			}else{
				$_SESSION['contractor_failed'] = 1;	
			}
			
			if($_SESSION['contractor_failed'] == 5){
				$result_array[1] = $this->disable_user($username);
			}
			
			$result_array['outcome'] = true;
			$result_array['message'] = 'User not found';
			
			$sql_contractor = "
			SELECT u.*,c.cpm_contractors_disabled,
			IFNULL(
				(
					SELECT t.cpm_contractors_user_trail_login
					FROM cpm_contractors_user_trail t
					WHERE t.cpm_contractors_user_trail_user_ref = u.cpm_contractors_user_ref
					ORDER BY t.cpm_contractors_user_trail_id DESC
					LIMIT 1
				),
				''
			)as last_login
			FROM cpm_contractors_user u
			INNER JOIN cpm_contractors c
			ON c.cpm_contractors_qube_id = u.cpm_contractors_user_qube_ref
			WHERE u.cpm_contractors_user_ref = '".$username."'";
			
			
			$result_contractor = $mysql->query($sql_contractor, 'Get Contractor');
			$num_valid_users = $mysql->num_rows($result_contractor);
			if($num_valid_users > 0){
				$result_array['message'] = 'Password Incorrect';
			}
		}
		
		return $result_array;
	} 
	
	
	function disable_user($username){
		Global $UTILS_HTTPS_ADDRESS;
		
		$mysql = new mysql();
		
		$sql = "SELECT IFNULL((SELECT cpm_contractors_user_email FROM cpm_contractors_user WHERE cpm_contractors_user_ref = c.cpm_contractors_user_parent), '' ) as parent_email, 
		* 
		FROM cpm_contractors_user c 
		WHERE cpm_contractors_user_ref = '".$username."'";
		$result = $mysql->query($sql, 'Get user');
		$num_valid_users = $mysql->num_rows($result);
		if($num_valid_users > 0){
			while ($row = $mysql->fetch_array($result)){
				$parent_email = $row['parent_email'];
				$user_name = $row['cpm_contractors_user_name'];
				$user_email = $row['cpm_contractors_user_email'];
				$user_disabled = $row['cpm_contractors_user_disabled'];
			}
		}
		
		if ($user_disabled == 'True'){
			$sql = "UPDATE cpm_contractors_user SET cpm_contractors_user_disabled = 'True' WHERE cpm_contractors_user_ref = '".$username."'";
			$mysql->insert($sql, 'update contractor');
			
			$mail_message = 'Dear '.$user_name.',
					
			Update from RMG Living Contractors Portal.
				
			Your account has been locked, due to 5 failed log in attempts. Please contact your supervisor.
				
				
			Kind Regards,
			
			Technical Support Team
			Residential Management Group Ltd
			
			'.$UTILS_HTTPS_ADDRESS.'
			Tel. '.$UTILS_TEL_MAIN;
				
			$sql = "INSERT INTO cpm_mailer SET
			mail_to = '".$user_email.", ".$parent_email.", service.desk@rmgltd.co.uk',
			mail_from = 'support@rmgltd.co.uk',
			mail_subject = 'New RMG Living Password',
			mail_message = '".$mail_message."'";
			
			$mysql->insert($sql, 'Add email');
		}
		
		return "Account disabled for 5 failed login attempts";
	}

	function forgot_check_fields($post, $securimage){
		
		$data = new data;
		$field = new field;
		$unit = new unit;
		$i = 0;
		
		$result_array = array();

		if($post['email'] == "" || !$field->is_valid_email($post['email'])){
			$result_array[$i]['outcome'] = true;
			$result_array[$i]['message'] = "Please enter a valid email address.";
			$i++;
		}
		
		// Check CAPTCHA 
		if ($securimage->check($post['captcha_code']) == false) {
			$result_array[$i]['outcome'] = true;
			$result_array[$i]['message'] = "The words you typed into the Captcha field below are incorrect. Please try again.";
			$i++;
		}
		
		return $result_array;
	}
	
	
	function do_forgot($post){
		
		$security = new security;
		$unit = new unit;
		$crypt = new encryption_class;
		$mysql = new mysql();
		$resident = new resident();
		
		global $UTILS_DB_ENCODE;
		global $UTILS_HTTPS_ADDRESS;
		global $UTILS_TEL_MAIN;
		
		$sql = "
		SELECT * 
		FROM cpm_contractors_user
		WHERE 
		cpm_contractors_user_email = '".$security->clean_query($post['email'])."'";
		
		$result = $mysql->query($sql, 'Get User');
		$num = $mysql->num_rows($result);
		if($num > 0){
			while($row = $mysql->fetch_array($result)){
				
				$password = $resident->get_rand_id(8, "ALPHA", "UPPER");
					
				$sql_insert = "UPDATE cpm_contractors_user SET 
				cpm_contractors_user_password = '".$crypt->encrypt($UTILS_DB_ENCODE, $password)."' 
				WHERE cpm_contractors_user_ref = '".$row['cpm_contractors_user_ref']."'";
				$mysql->insert($sql_insert, 'Update Contractor') ;
			
				$mail_message = 'Dear '.$row['cpm_contractors_user_name'].',
					
				Update from RMG Living Contractors Portal.
					
				Your username is: '.$row['cpm_contractors_user_ref'].'
				Your password has been reset to: '.$password.'
				
				To log in, go to '.$UTILS_HTTPS_ADDRESS.'
					
					
				Kind Regards,
				
				Technical Support Team
				Residential Management Group Ltd
				
				'.$UTILS_HTTPS_ADDRESS.'
				Tel. '.$UTILS_TEL_MAIN;
					
				$sql = "INSERT INTO cpm_mailer SET
				mail_to = '".$post['email']."',
				mail_from = 'support@rmgltd.co.uk',
				mail_subject = 'New RMG Living Password',
				mail_message = '".$mail_message."'";
				
				$mysql->insert($sql, 'Insert Mail');
				
				return true;
			}
		}
	
		return false;
	}
	
	
	function do_logout(){
		unset($_SESSION['contractor_session']);		
		unset($_SESSION['contractors_username']);
		unset($_SESSION['contractors_qube_id']);

		unset($_SESSION['admin_session']);
		unset($_SESSION['admin_user_id']);
	}
	
	function logged_in(){
		if(isset($_SESSION['contractors_username'])){
			return true;
		}elseif(isset($_SESSION['admin_user_id'])){
			return true;
		}else{
			return false;
		}
	}

}

?>