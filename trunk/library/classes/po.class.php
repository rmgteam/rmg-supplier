<?
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."xml.class.php");
require_once($UTILS_CLASS_PATH."xmltoarray.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."mysql.class.php");
require_once($UTILS_CLASS_PATH."webservice.class.php");
require_once($UTILS_CLASS_PATH."search_builder.class.php");

class po {
	
	var $ip_address = '94.250.233.138';
	
	function po (){
		
	}

	function set_image($po_no){
		$mysql = new mysql;
		$sql = "UPDATE cpm_po SET
		cpm_po_image_upload = 'Y'
		WHERE cpm_po_number = '".$po_no."'";
		$mysql->insert($sql, 'Get all residents');
		return true;
	}

	function get_po_details($po_id){
		$mysql = new mysql;
		$sql = "SELECT *
		FROM cpm_po p
		INNER JOIN cpm_subsidiary s ON s.subsidiary_code = p.cpm_po_brand
		LEFT JOIN cpm_lookup_rmcs r ON r.rmc_lookup = p.cpm_po_rmc_id
		WHERE cpm_po_id = '".$po_id."'";
		$result = $mysql->query($sql, 'Get all residents');
		if($mysql->num_rows($result) > 0){
			return $mysql->fetch_array($result);
		}else{
			return false;
		}

	}

	function updatedJob($id){
		$mysql = new mysql;

		$sql = "SELECT *
		FROM cpm_po_job
		WHERE cpm_po_job_po_id = '".$id."'
		AND cpm_po_job_completion_date <> cpm_po_job_estimated_completion_date";

		$result = $mysql->query($sql, 'Get all residents');
		if($mysql->num_rows($result) > 0){
			return true;
		}else{
			return false;
		}
	}

	function holdJob($id){
		$mysql = new mysql;

		$sql = "SELECT *
		FROM cpm_po_job
		WHERE cpm_po_job_po_id = '".$id."'
		AND cpm_po_job_complete = 'Hold'";

		$result = $mysql->query($sql, 'Get all residents');
		if($mysql->num_rows($result) > 0){
			return true;
		}else{
			return false;
		}
	}

	function contractJob($id){
		$mysql = new mysql;

		$sql = "SELECT *
		FROM cpm_po
		WHERE cpm_po_id = '".$id."'
		AND cpm_po_is_contract = 1";

		$result = $mysql->query($sql, 'Get PO');
		if($mysql->num_rows($result) > 0){
			return true;
		}else{
			return false;
		}
	}

	function get_results($request, $contractor=''){

		$data = new data;
		
		$result_array = array();
		$no_fields = $request['no_fields'];
		
		$search_builder = new search_builder;
		$security = new security;
		
		$values_array = $search_builder->request_to_array($request, 'search_filter_');

		if($contractor == '' && isset($_SESSION['contractors_qube_id']) && !isset($_SESSION['admin_session'])){
			$contractor = $_SESSION['contractors_qube_id'];
		}

		if($contractor == '' && !isset($_SESSION['admin_session'])){
			return false;
		}

		if(isset($request['search_term'])){
			$search_term = $request['search_term'];
		}

		$closed =  $request['closed_type'];

		$search_builder->add_index_column('cpm_po_id');
		$search_builder->add_column('cpm_po_number');
		$search_builder->add_function_column('cpm_po_id', 'po', 'get_comment_icon');
		$search_builder->add_function_column('cpm_po_id', 'po', 'get_image_icon');
		$search_builder->add_function_column('subsidiary_code', '', 'strtoupper' );
		$search_builder->add_column('rmc_name');
		$search_builder->add_column('cpm_po_description');
		$search_builder->add_function_column('cpm_po_job_estimated_completion_date', 'data', 'ymd_to_date');
		
		if ($no_fields > 0 ){
			foreach($values_array as $key=>$value) {
		
				$search_term = $value;
		
				switch($key){
					case "contractor":
						$field = "cpm_contractors_qube_id";
						if(strpos($search_term, ",") !== false){
							$search_builder->add_filter('', "(", "", 'AND');
							$terms = explode(",", $search_term);
							foreach($terms as $term){
								$search_builder->add_filter($field, '=', $term, 'OR');
							}
							$search_builder->add_filter('', ")", "", 'OR');
						}else{
							$search_builder->add_filter($field, '=', $search_term, 'AND');
						}
						break;
					case "property":

						if(strpos($search_term, ",") !== false){
							$search_builder->add_filter('', "(", "", 'AND');
							$terms = explode(",", $search_term);
							foreach($terms as $term){
								if(strpos($term, "-") !== false) {
									$rmc_array = explode('-', $term);
									$rmc_ref = $rmc_array[0];
									$subsidiary_code = $rmc_array[1];
									$search_builder->add_filter('', "(", "", 'OR');
									$search_builder->add_filter("l.rmc_ref", '=', $rmc_ref, 'AND');
									$search_builder->add_filter("s.subsidiary_code", '=', $subsidiary_code, 'AND');
									$search_builder->add_filter('', ")", "", 'OR');
								}else{
									$search_builder->add_filter("l.rmc_ref", '=', $term, 'OR');
								}
							}
							$search_builder->add_filter('', ")", "", 'OR');
						}else{
							if(strpos($search_term, "-") !== false) {
								$rmc_array = explode('-', $search_term);
								$rmc_ref = $rmc_array[0];
								$subsidiary_code = $rmc_array[1];

								$search_builder->add_filter("l.rmc_ref", '=', $rmc_ref, 'AND');
								$search_builder->add_filter("s.subsidiary_code", '=', $subsidiary_code, 'AND');
							}else{
								$search_builder->add_filter("l.rmc_ref", '=', $search_term, 'AND');
							}
						}
						break;
					case "closed":
						if($search_term != ''){
							$field = "j.cpm_po_job_complete";
							$search_builder->add_filter($field, '=', $search_term, 'AND');
						}
						break;
					case "key":
						$search_builder->add_filter('', "(", "", 'AND');
						$field = "UCASE(CONVERT(p.cpm_po_description using utf8))";
						$search_builder->add_filter($field, 'LIKE', '%' . strtoupper($search_term) . '%', 'OR');
						$field = "p.cpm_po_number";
						$search_builder->add_filter($field, 'LIKE', '%' . $search_term . '%', 'OR');
						$field = "j.cpm_po_job_no";
						$search_builder->add_filter($field, 'LIKE', '%' . $search_term . '%', 'OR');
						$search_builder->add_filter('', ")", "", 'OR');
						break;
				}
			}

		}else{

			if(isset($search_term)){
				$search_builder->add_filter('', "(", "", 'AND');
				$field = "UCASE(CONVERT(p.cpm_po_description using utf8))";
				$search_builder->add_filter($field, 'LIKE', '%' . strtoupper($search_term) . '%', 'OR');
				$field = "p.cpm_po_number";
				$search_builder->add_filter($field, 'LIKE', '%' . $search_term . '%', 'OR');
				$field = "j.cpm_po_job_no";
				$search_builder->add_filter($field, 'LIKE', '%' . $search_term . '%', 'OR');
				$field = "r.rmc_name";
				$search_builder->add_filter($field, 'LIKE', '%' . $search_term . '%', 'OR');
				$search_builder->add_filter('', ")", "", 'OR');
			}

			if($closed != ''){
				$field = "j.cpm_po_job_complete";
				if($closed == 'False'){
					$search_builder->add_filter('', "(", "", 'AND');
					$search_builder->add_filter($field, '=', $closed, 'OR');
					$search_builder->add_filter($field, '=', 'Hold', 'OR');
					$search_builder->add_filter('', ")", "", 'AND');
				}else {
					$search_builder->add_filter($field, '=', $closed, 'AND');
				}
			}

		}

		if($contractor != ''){
			$search_builder->add_filter('cpm_contractors_qube_id', '=', $contractor, 'AND');
		}

		$iColumnCount = count($search_builder->columns);

		//Paging
		if ( isset( $request['iDisplayStart'] ) && $request['iDisplayLength'] != '-1' ) {
			$search_builder->paging($request['iDisplayStart'], $request['iDisplayLength']);
		}
		
		//Ordering
		$iSortingCols = intval( $request['iSortingCols'] );
		for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
			if ( $request[ 'bSortable_'.intval($request['iSortCol_'.$i]) ] == 'true' ) {
		
				$column = $search_builder->columns[intval($request['iSortCol_'.$i])];
		
				$search_builder->add_order($column, ($request['sSortDir_'.$i]==='asc' ? 'asc' : 'desc'));
			}
		}
		
		//Filtering
		if ( isset($request['sSearch']) && $request['sSearch'] != "" ) {
			$search_builder->add_filter('', "(", "", 'AND');
			$search_builder->all_column_filter( $security->clean_query( $request['sSearch'] ));
			$search_builder->add_filter('', ")", "", 'OR');
		}
		
		// Individual column filtering
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			if ( isset($request['bSearchable_'.$i]) && $request['bSearchable_'.$i] == 'true' && $request['sSearch_'.$i] != '' ) {
				$search_builder->add_filter($search_builder->columns[$i], "LIKE", '%'.$security->clean_query( $request['sSearch_'.$i] ).'%', 'AND');
			}
		}
		
		$sql = "
		FROM cpm_po p
		INNER JOIN cpm_subsidiary s ON s.subsidiary_code = p.cpm_po_brand
		INNER JOIN cpm_contractors c ON c.cpm_contractors_qube_id = p.cpm_po_contractors_ref
		INNER JOIN cpm_lookup_rmcs l ON l.rmc_ref = p.cpm_po_rmc_id
		INNER JOIN cpm_rmcs r ON r.rmc_num = l.rmc_lookup AND r.subsidiary_id = s.subsidiary_id
		INNER JOIN cpm_po_job j ON j.cpm_po_job_po_id = p.cpm_po_id";
		
		$search_builder->add_sql($sql);
		$search_builder->add_group('p.cpm_po_id');
		
		$data = $search_builder->output($request);

		foreach($data['aaData'] as &$row){
			$dataRow = $this->updatedJob($row['DT_RowId']);
			$holdRow = $this->holdJob($row['DT_RowId']);
			$contractRow = $this->contractJob($row['DT_RowId']);

			if($holdRow == true){
				$row['DT_RowClass'] = 'hold';
			}elseif($dataRow == true){
				$row['DT_RowClass'] = 'updated';
			}

			if($contractRow == true){
				$row['DT_RowClass'] .= ' contract';
			}

		}

		return $data;
	}

	function get_comment_icon($id){

		$mysql = new mysql;

		$sql = "SELECT cpm_po_job_advice, last_update
		FROM cpm_po_job
		WHERE cpm_po_job_po_id = '".$id."'
		AND cpm_po_job_advice <> ''";
		$result = $mysql->query($sql, 'Find Abandoned');

		$numrows = $mysql->num_rows($result);

		if($numrows > 0) {
			return '<a onclick="javascript:get_note(' . "'" . $id . "'" . ');return false;"><i class="fa fa-file-text"></i></a>';
		}else{
			return '';
		}

	}

	function get_image_icon($id){

		$mysql = new mysql;

		$sql = "SELECT cpm_po_image_upload
		FROM cpm_po
		WHERE cpm_po_id = '".$id."'
		AND cpm_po_image_upload = 'Y'";
		$result = $mysql->query($sql, 'Find Abandoned');

		$numrows = $mysql->num_rows($result);

		if($numrows > 0) {
			return '<i class="fa fa-picture-o"></i>';
		}else{
			return '';
		}

	}

	function get_jobs($request, $po_id, $search_term, $closed_type){

		$mysql = new mysql();
		$data = new data();

		$security = new security();
		$input = $request;

		$closed =  $closed_type;

		$aColumns = array( 'cpm_po_number', 'cpm_po_job_completion_date', 'cpm_po_job_estimated_completion_date', 'cpm_po_job_amount', 'cpm_po_job_id' );
		$iColumnCount = count($aColumns);

		$which = 3;

		// Indexed column (used for fast and accurate table cardinality)
		$sIndexColumn = 'cpm_po_job_id';

		$sWhere = "";

		/**
		 * Paging
		 */
		$sLimit = "";
		if ( isset( $input['iDisplayStart'] ) && $input['iDisplayLength'] != '-1' ) {
			$sLimit = " LIMIT ".intval( $input['iDisplayStart'] ).", ".intval( $input['iDisplayLength'] );
		}

		/**
		 * Ordering
		 */
		$aOrderingRules = array();
		if ( isset( $input['iSortCol_0'] ) ) {
			$iSortingCols = intval( $input['iSortingCols'] );
			for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
				if ( $input[ 'bSortable_'.intval($input['iSortCol_'.$i]) ] == 'true' ) {

					$aOrderingRules[] = $aColumns[intval($input['iSortCol_'.$i])] . " ".($input['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
				}
			}
		}

		if (!empty($aOrderingRules)) {
			$sOrder = " ORDER BY ".implode(", ", $aOrderingRules);
		} else {
			$sOrder = " ORDER BY cpm_po_number DESC";
		}

		/**
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$iColumnCount = count($aColumns);

		if ( isset($input['sSearch']) && $input['sSearch'] != "" ) {
			$aFilteringRules = array();
			for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
				if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' ) {
					$variable = $input['sSearch'];

					if($i == $which){
						$variable = $data->date_to_ymd($input['sSearch_'.$i]);
					}

					$aFilteringRules[] = "`".$aColumns[$i]."` LIKE '%".$security->clean_query( $variable )."%'";
				}
			}
			if (!empty($aFilteringRules)) {
				$aFilteringRules = array('('.implode(" OR ", $aFilteringRules).')');
			}
		}

		// Individual column filtering
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' && $input['sSearch_'.$i] != '' ) {
				$variable = $input['sSearch_'.$i];

				if($i == $which){
					$variable = $data->date_to_ymd($input['sSearch_'.$i]);
				}

				$aFilteringRules[] = "`".$aColumns[$i]."` LIKE '%".$security->clean_query( $variable )."%'";
			}
		}

		if (!empty($aFilteringRules)) {
			$sWhere = " AND ".implode(" AND ", $aFilteringRules);
		}

		if(($closed_type != False && $closed_type != True) || $closed_type == ''){
			$closed = '';
		}else{
			$sWhere .= "
			AND cpm_po_job_complete = '".$closed_type."'";
		}

		$sql = "SELECT j.*,
		p.*
		FROM cpm_po_job j
		INNER JOIN cpm_po p ON j.cpm_po_job_po_id = p.cpm_po_id
		WHERE cpm_po_job_po_id = '".$po_id."'
		".$sWhere.$sOrder;

		$result = $mysql->query($sql, 'Find Abandoned');

		$numrows = $mysql->num_rows($result);
		$iFilteredTotal = $mysql->num_rows($result);

		$sql.=$sLimit;
		$result = $mysql->query($sql, 'Get Abandoned');

		$iTotal = $numrows;

		/**
		 * Output
		 */
		$output = array(
			"sEcho"                => intval($input['sEcho']),
			"iTotalRecords"        => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData"               => array(),
			"numrows"			   => $numrows
		);

		while($aRow = $mysql->fetch_array($result)){
			$row = array();
			for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
				// General output
				if($i == 1 || $i == 2) {
					$row[] = $data->ymd_to_date($aRow[$aColumns[$i]]);
				}elseif($i == 4){
					$days = $data->today_to_ymd() - $row['cpm_po_date_raised'];
					if($days > 6) {
						$days = 6;
					}
					if($aRow['cpm_po_job_complete'] != 'True' || (isset($_SESSION['admin_session']) && !isset($_SESSION['contractors_qube_id'])) ) {
						$row[] = '<input type="button" class="mini_button" id="button_' . $aRow[$aColumns[$i]] . '" onclick="complete_job(' . "'" . $aRow[$aColumns[$i]] . "', '" . $days . "'" . ')" value="Update" />';
					}else{
						$row[] = '';
					}

				}else{
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}

			$row["DT_RowId"] =  $aRow[$sIndexColumn];

			$output['aaData'][] = $row;

		}

		$output['sql'] = $sql;

		return $output;

	}
	
	function is_blank($str, $output){
		if ($str == ''){
			return $output;
		}else{
			return $str;	
		}
	}
	
	function retrieve_po ($contractor_qube_ref){
		
		$data = new data;
		$sqls = '';
		
		$po_id_array = array();
		$mysql = new mysql();
		
		$result_array = $this->get_po($contractor_qube_ref, 'getorders', "RMG Live");
		
		for( $c=0;$c<count($result_array['orders']);$c++ ) {
			
			$po_id = '';
			
			$order_number = addslashes(trim( $result_array['orders'][$c]['order-number'] )); if(is_array($result_array['orders'][$c]['order-number'])){$order_number = "";}
			$order_description = addslashes(trim( $result_array['orders'][$c]['description'] )); if(is_array($result_array['orders'][$c]['description'])){$order_description = "";}
			$job_number = addslashes(trim( $result_array['orders'][$c]['job-audit-number'] )); if(is_array($result_array['orders'][$c]['job-audit-number'])){$job_number = "";}
			$action_date = addslashes(trim( $result_array['orders'][$c]['action'] )); if(is_array($result_array['orders'][$c]['action'])){$action_date = "";}
			$job_unique = addslashes(trim( $result_array['orders'][$c]['uniqueid'] )); if(is_array($result_array['orders'][$c]['uniqueid'])){$job_unique = "";}
			$amount = addslashes(trim( $result_array['orders'][$c]['amount'] )); if(is_array($result_array['orders'][$c]['amount'])){$amount = "";}
			$property_ref = addslashes(trim( $result_array['orders'][$c]['property-ref'] )); if(is_array($result_array['orders'][$c]['property-ref'])){$property_ref = "";}
			$contract = addslashes(trim( $result_array['orders'][$c]['is-contract'] )); if(is_array($result_array['orders'][$c]['is-contract'])){$contract = "";}
			$date_raised = addslashes(trim( $result_array['orders'][$c]['date-raised'] )); if(is_array($result_array['orders'][$c]['date-raised'])){$date_raised = "";}
			$brand = addslashes(trim( $result_array['orders'][$c]['brand'] )); if(is_array($result_array['orders'][$c]['brand'])){$brand = "";}
			$sequence = addslashes(trim( $result_array['orders'][$c]['sequence'] )); if(is_array($result_array['orders'][$c]['sequence'])){$sequence = "";}
			$job_status = addslashes(trim( $result_array['orders'][$c]['job-status-str'] )); if(is_array($result_array['orders'][$c]['job-status-str'])){$job_status = "";}
			$job_status_int = addslashes(trim( $result_array['orders'][$c]['job-status-int'] )); if(is_array($result_array['orders'][$c]['job-status-int'])){$job_status_int = "";}
			$completion_date = addslashes(trim( $result_array['orders'][$c]['job-completion'] )); if(is_array($result_array['orders'][$c]['job-completion'])){$completion_date = "";}
			$our_date = addslashes(trim( $result_array['orders'][$c]['job-target-date'] )); if(is_array($result_array['orders'][$c]['job-target-date'])){$our_date = "";}
			$estimated_date = addslashes(trim( $result_array['orders'][$c]['job-est-comp'] )); if(is_array($result_array['orders'][$c]['job-est-comp'])){$estimated_date = "";}
			$resident_reference = addslashes(trim( $result_array['orders'][$c]['tenant-reference'] )); if(is_array($result_array['orders'][$c]['tenant-reference'])){$resident_reference = "0";}
			
			$property_ref = ltrim( ltrim( ltrim( ltrim( trim($property_ref), "z-"), "Z-"), "z"), "Z");
			
			if($action_date != ""){
				$date_parts = explode("-", $action_date);
				$action_date = $date_parts[0].$date_parts[1].$date_parts[2];
			}
			
			if($date_raised != ""){
				$date_parts = explode("-", $date_raised);
				$date_raised = $date_parts[0].$date_parts[1].$date_parts[2];
			}
			
			if($completion_date != ""){
				$date_parts = explode("-", $completion_date);
				$completion_date = $date_parts[0].$date_parts[1].$date_parts[2];
			}
			
			if($estimated_date != ""){
				$date_parts = explode("-", $estimated_date);
				$estimated_date = $date_parts[0].$date_parts[1].$date_parts[2];
			}
			
			if($our_date != ""){
				$date_parts = explode("-", $our_date);
				$our_date = $date_parts[0].$date_parts[1].$date_parts[2];
			}
			
			if($contract == ""){
				$contract = 0;
			}
			
			$sql_po = "SELECT *
			FROM cpm_po
			WHERE cpm_po_number = '".$order_number."'
			AND cpm_po_brand = '".$brand."'";
			
			$result_po = $mysql->query($sql_po, 'Get POs');
			$num_result_po = $mysql->num_rows($result_po);
			
			//to remove 
			$result_array['orders'][$c]['num_result_po'] = $num_result_po;
			
			
			if($num_result_po == 0){
				$sql = "INSERT INTO cpm_po SET 
				cpm_po_number = '".$order_number."', ";
			}else{				
				$sql = "UPDATE cpm_po SET ";
			}
			
			//if resident reference is null insert 0 as field content.
			$resident_reference = $resident_reference ? $resident_reference : 0;
			
			$sql .= "cpm_po_rmc_id = '".$property_ref."',
			cpm_po_contractors_ref = '".$contractor_qube_ref."', 
			cpm_po_is_contract = '".$contract."', 
			cpm_po_date_raised = '".$date_raised."', 
			cpm_po_brand = '".$brand."',
			cpm_po_resident_ref = '".$resident_reference."',  
			last_update = '".$data->now_to_ymdhis()."',
			cpm_po_description = '".$order_description."'";

			
			if($num_result_po == 0){

				$po_id = $mysql->insert($sql, 'Insert PO');
				
			}else{

				$row_po = $mysql->fetch_array($result_po);
				$po_id = $row_po['cpm_po_id'];
				
				$sql .= " WHERE cpm_po_id = '".$po_id."'";
				$mysql->insert($sql, 'Update PO');
				
			}

			$result_array['orders'][$c]['sql'] = $sql;
			array_push($po_id_array, $po_id);
			
			$sql_job = "SELECT *
			FROM cpm_po_job
			WHERE cpm_po_job_no = '".$job_number."'
			AND cpm_po_job_po_id = '" . $po_id ."'";
			
			$result_job = $mysql->query($sql_job, 'Get Jobs');
			$num_result_job = $mysql->num_rows($result_job);
			if($num_result_job == 0){
				$sql = "INSERT INTO cpm_po_job SET 
				cpm_po_job_no = '".$job_number."',
				cpm_po_job_unique = '".$job_unique."', ";
			}else{

				$sql_job_audit = "SELECT *
				FROM cpm_po_job_audit
				WHERE cpm_po_job_no = '".$job_number."'
				AND cpm_po_job_po_id = '" . $po_id ."'
				ORDER BY cpm_po_job_id DESC
				LIMIT 1";
				$result_job_audit = $mysql->query($sql_job_audit, 'Get Jobs');

				$sql = "UPDATE cpm_po_job SET ";
				$row = $mysql->fetch_array($result_job_audit);
				if( $row['cpm_po_job_complete'] == 'Hold' ) {
					$job_completed = 'Hold';
				}else{
					$job_completed = 'False';
				}
			}
			
			$sql .= "cpm_po_job_amount = '".$amount."', 
			cpm_po_job_completion_date = '".$our_date."',  
			cpm_po_job_estimated_completion_date = '".$estimated_date."',  
			cpm_po_job_sequence = '".$sequence."', 
			cpm_po_job_status = '".$job_status."', 
			cpm_po_job_status_int = '".$job_status_int."',
			cpm_po_job_po_id = '".$po_id."',
			last_update = '".$data->now_to_ymdhis()."'";
			
			if($job_status_int >= 4 && $job_status_int <= 6){
				if($num_result_job > 0){
					$row_job = $mysql->fetch_array($result_job);
					$job_id = $row_job['cpm_po_job_id'];

					$sql .= ",
					cpm_po_job_complete = 'True'";
					
					if ($row_job['cpm_po_job_user_ref'] == ''){
						$sql .= ", cpm_po_job_user_ref = 'Qube'";
					}
					if($completion_date != ""){
						$sql .= ",
						cpm_po_job_ts = '".$completion_date."'";
					}
				}
			}else{
				$sql .= ", cpm_po_job_complete = '".$job_completed."',
				cpm_po_job_user_ref = '', 
				cpm_po_job_reason = '', 
				cpm_po_job_reason_id = '',
				cpm_po_job_ts = ''";
			}
			
			if($num_result_job > 0){				
				$sql .= " WHERE cpm_po_job_no = '".$job_number."'
				AND cpm_po_job_po_id = '" . $po_id ."'";
			}

			//var_dump($sql);
			
			$mysql->insert($sql, 'Update Job');
			//to remove
			$result_array['orders'][$c]['sql'] = $sql;

			$sql = "UPDATE cpm_po_job_audit SET
			cpm_po_job_po_id = '".$po_id."'
			WHERE cpm_po_job_no = '".$job_number."'
			AND cpm_po_job_unique = '".$job_unique."'
			AND cpm_po_job_amount = '".$amount."'";
			$mysql->insert($sql, 'Update Job');

		}
		
		$sql = "SELECT j.*
		FROM cpm_po p
		INNER JOIN cpm_po_job j
		ON j.cpm_po_job_po_id = p.cpm_po_id
		WHERE ";
		
		if(count($po_id_array) > 0){
			$sql .= "(";
			
			foreach($po_id_array as $id){
				$sql .= "cpm_po_id <> '".$id."' AND ";
			}
			
			$sql = substr($sql,0,-5) .") AND ";
		}
		
		$sql .= "cpm_po_contractors_ref = '".$contractor_qube_ref."'";
		
		$result = $mysql->query($sql, 'Get Jobs');
		$num_result = $mysql->num_rows($result);
		if($num_result > 0){
			while($row = $mysql->fetch_array($result)){
				$sql_complete = "UPDATE cpm_po_job SET
				cpm_po_job_complete = 'True'";
				
				if ($row['cpm_po_job_user_ref'] == ''){
					$sql_complete .= ", cpm_po_job_user_ref = 'Qube'";
				}
				$sql_complete .= " WHERE cpm_po_job_id = '".$row['cpm_po_job_id']."'";
				$mysql->insert($sql_complete, 'Update Job');
				$sqls .= preg_replace( '/\n\r|\r\n|\t/', ' ', $sql_complete);
			}
		}
		return $result_array;
	}
	
	function get_po($key, $process, $group="RMG Test"){
		$security = new security();
		$xml = new xml;
		$data = new data;
		$output_html = "";
		$mysql = new mysql();
		
		$session_id = $security->gen_serial(16);
		$ch = curl_init();
	
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$host = "Host: hodd1srctxprs13";
		$content_type = "Content-Type: text/xml; charset=utf-8";
		
		$sql = "SELECT j.last_update 
		FROM cpm_po_job j
		INNER JOIN cpm_po p
		ON p.cpm_po_id = j.cpm_po_job_po_id
		WHERE
		p.cpm_po_contractors_ref = '".$key."' 
		ORDER BY j.cpm_po_job_id DESC 
		LIMIT 1";
		
		$last_update = '2016-07-01';
		
		/*$result_job = $mysql->query($sql, 'Get Date');
		$num_result_job = $mysql->num_rows($result_job);
		if($num_result_job > 0){
			$row_job = $mysql->fetch_array($result_job);
			$last_update = 	$data->ymdhis_to_date($row_job['last_update'], "Y-m-d");
		}*/
		
		$data = '<parameters>
		<key>'.$key.'</key>
		<filter-date>'.$last_update.'</filter-date>
		<include-closed>no</include-closed>
		</parameters>';
		
		$webservice = new webservice;
		
		$xml_result = $webservice->qube_execute(1, $process, $data, $group, $session_id);
		
		$result_array['output'] = $xml_result['desc'];
		
		if(!is_null($xml_result['code'])){
			$output_html = "XML is invalid/Curl is unavailable. Http response: " . $xml_result['code'] . " XML = " . $xml_result['desc'];
		}else {	
			$return_code = $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['suppliervalues'][0]['code'];
			
			if($return_code != "00"){ //service unavailable
				$result_array['result'] = "Error code: " . $return_code . " - " . $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['suppliervalues'][0]['desc'] . $data;
			}else{
				$num_trans = $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['suppliervalues'][0]['recordcount'];
				
				if($num_trans == 0 || $num_trans == ""){
					
					$result_array['result'] = "There are no po's available.";
					return $result_array;
					exit;
				}
				else{

					$transact_array = $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['suppliervalues'][0]['orders'];
					
					$result_array['result'] = "success";
					$result_array['orders'] = array();
					$result_array['orders'] = $transact_array;
				}
			}
		}				
		curl_close($ch);
		return $result_array;
		exit;
	}
	
	function complete_job($job_id){
		$security = new security();
		$xml = new xml;
		$data = new data;
		$output_html = "";
		$mysql = new mysql();
		
		$session_id = $security->gen_serial(16);
		$ch = curl_init();
	
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$content_type = "Content-Type: text/xml; charset=utf-8";
		
		$sql = "SELECT j.*, p.cpm_po_brand
		FROM cpm_po_job j
		INNER JOIN cpm_po p ON p.cpm_po_id = j.cpm_po_job_po_id
		WHERE cpm_po_job_id = '".$job_id."'
		ORDER BY j.cpm_po_job_id DESC 
		LIMIT 1";
		
		$job_no = '';
		$brand = '';
		$para = '';
		
		$result_job = $mysql->query($sql, 'Get Jobs');
		$num_result_job = $mysql->num_rows($result_job);
		if($num_result_job > 0){
			$row_job = $mysql->fetch_array($result_job);
			$job_no = $row_job['cpm_po_job_no'];
			$brand = $row_job['cpm_po_brand'];
			if ($row_job['cpm_po_job_complete'] == 'True'){
				if ($row_job['cpm_po_job_reason_id'] == ''){
					$complete = 1;	
					
					if($row_job['cpm_po_job_advice'] != ''){
						$para = '
						<advice>true</advice><comments>'.$row_job['cpm_po_job_advice'].'</comments>';	
					}

					$para .= '
					<completion>'.$data->ymd_to_date($row_job['cpm_po_job_estimated_completion_date']).'</completion>';
				}else{
					$complete = $row_job['cpm_po_job_reason_id'];
					
					if($row_job['cpm_po_job_reason'] != ''){
						$para = '
						<comments>'.$row_job['cpm_po_job_reason'].'</comments>';
					}else{
						$para = '
						<comments>-</comments>';
					}
				}

				$para .= '
				<hold>0</hold>';
			}else{
				$complete = 0;
				
				if($row_job['cpm_po_job_advice'] != ''){
					$para = '
					<comments>'.$row_job['cpm_po_job_advice'].'</comments>';
				}else{
					$para = '
					<comments>-</comments>';
				}

				if ($row_job['cpm_po_job_complete'] == 'Hold'){
					$para .= '
					<hold>1</hold>';
				}else{
					$para .= '
					<hold>0</hold>';
				}
				
				$para .= '
				<completion>'.$data->ymd_to_date($row_job['cpm_po_job_estimated_completion_date']).'</completion>';
				
			}
		}
		$group = '';
		
		switch(strtoupper($brand)){
			case 'RMG':
				$group = 'RMG Live';
				break;
			case 'RM2':
				$group = 'RM2 Live';
				break;
			case 'GF':
				$group = 'RMG Gross Fine Live';
				break;
			case 'WM':
				$group = 'RMG Woods Management Live';
				break;
			case 'JC':
				$group = 'RMG Johnson Cooper Live';
				break;
			case 'TF':
				$group = 'RMG Taskfine Live';
				break;
			case 'HPS':
				$group = 'RMG Haywards Live';
				break;
		}
		
		$data = '<parameters>
		<reference>'.$job_no.'</reference>
		<status>'.$complete.'</status>'.$para.'
		</parameters>';
		
		$output_html = "";
		
		$webservice = new webservice;
		
		$xml_result = $webservice->qube_execute(3, 'closejob', $data, $group, $session_id);
		
		$result_array['data'] = $data;
		$result_array['group'] = $group;
		$log = new log();
		
		if(!is_null($xml_result['code'])){
			$output_html = "XML is invalid/Curl is unavailable. Http response: " . $xml_result['code'] . " XML = " . $xml_result['desc'];
			$result_array['result'] = $output_html;
			$log->string_log( 'closejob fail for ' . $job_id, $result_array['result'] );
		}else {		
			$return_code = $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['closure-result'][0]['code'];
			$result_array['code'] = $return_code;
			if($return_code != "00"){ //service unavailable
				$result_array['result'] = $xml_result['code'] . " - Error code: " . $return_code . " - " . $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['closure-result'][0]['desc'] . " XML = " . $data;
				$log->string_log( 'closejob fail for ' . $job_id, $result_array['result'] );
			}else{
				$result_array['result'] = $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['closure-result'][0]['desc'];
			}
		}				
		curl_close($ch);
		//$webservice->logout($session_id);
		return $result_array;
	}

	function trigger_survey($cpm_po_id){
	
		//check if there is not any other open job and trigger survey to cpm_lookup_residents.resident_lookup
		$sql = "SELECT count(*)
				FROM cpm_po_job
				WHERE cpm_po_job_po_id = '".$cpm_po_id."' and cpm_po_job_complete= 'false'";
		$result = mysql_query($sql);
		$row = @mysql_fetch_array($result);

		if($row[0] > 0)
		{
			$update_reason = null;
			//finding cpm_lookup_residents.resident_lookup from cpm_po.cpm_po_id
			$sql = "SELECT resident_ref
					FROM cpm_po
					JOIN cpm_lookup_residents ON cpm_lookup_residents.resident_ref = cpm_po.cpm_po_resident_ref
					WHERE cpm_po.cpm_po_id = '".$cpm_po_id."' AND cpm_po.cpm_po_resident_ref != 0
					LIMIT 1";
			$result = mysql_query($sql);
			$num_rows = @mysql_num_rows($result);
			if($num_rows > 0){
				$row = @mysql_fetch_array($result);
				$resident_ref = $row['resident_ref'];

				if($resident_ref > 0){
					$call_to = "http://www.rmgsurveys.co.uk/admin/survey.php?";
					$call_to .= "a=letters&";
					$call_to .= "survey_id=25&";
					$call_to .= "letter_table_residents='".$resident_ref."'&";
					$call_to .= "user_serial=06fbcb5624c7";
	
					$ch = curl_init($call_to);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					$output = curl_exec($ch);
					$output = json_decode($output);
					
					if(!curl_errno($ch))
					{
						$info = curl_getinfo($ch);
						$update_reason = array(	
												'success' => $output['success'],
												'sent'=>$output['sent']
												);
					}
					
					curl_close($ch);
				}
			}
			else{
				$update_reason = "No resident reference";
			}
			$sql = "Update cpm_po set cpm_po_survey = '".$update_reason."' where cpm_po_id = '".$cpm_po_id."'";
			$result = mysql_query($sql);
		}
	}

	function getNotes($request, $po_id) {


		$mysql = new mysql();
		$input = $request;

		$aColumns = array( 'cpm_po_job_advice', 'last_update' );
		$iColumnCount = count($aColumns);

		// Indexed column (used for fast and accurate table cardinality)
		$sIndexColumn = 'cpm_po_job_id';

		/**
		 * Paging
		 */
		$sLimit = "";
		if ( isset( $input['iDisplayStart'] ) && $input['iDisplayLength'] != '-1' ) {
			$sLimit = " LIMIT ".intval( $input['iDisplayStart'] ).", ".intval( $input['iDisplayLength'] );
		}

		/**
		 * Ordering
		 */
		$aOrderingRules = array();
		if ( isset( $input['iSortCol_0'] ) ) {
			$iSortingCols = intval( $input['iSortingCols'] );
			for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
				if ( $input[ 'bSortable_'.intval($input['iSortCol_'.$i]) ] == 'true' ) {

					$aOrderingRules[] = $aColumns[intval($input['iSortCol_'.$i])] . " ".($input['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
				}
			}
		}

		if (!empty($aOrderingRules)) {
			$sOrder = " ORDER BY ".implode(", ", $aOrderingRules);
		} else {
			$sOrder = " ORDER BY last_update DESC";
		}


		$sql = "SELECT cpm_po_job_advice, last_update, cpm_po_job_id
		FROM cpm_po_job_audit
		WHERE cpm_po_job_po_id = '".$po_id."'
		AND cpm_po_job_advice <> ''" . $sOrder;

		$result = $mysql->query($sql, 'Find Abandoned');

		$numrows = $mysql->num_rows($result);
		$iFilteredTotal = $mysql->num_rows($result);

		$sql.=$sLimit;
		$result = $mysql->query($sql, 'Get Abandoned');

		$iTotal = $numrows;

		/**
		 * Output
		 */
		$output = array(
			"sEcho"                => intval($input['sEcho']),
			"iTotalRecords"        => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData"               => array(),
			"numrows"			   => $numrows
		);

		while($aRow = $mysql->fetch_array($result)){
			$row = array();
			for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
				// General output
				if( $i == 1 ) {
					$row[] = date('d/m/Y H:i', $aRow[ $aColumns[$i] ]);
				}else{
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}

			$row["DT_RowId"] =  $aRow[$sIndexColumn];

			$output['aaData'][] = $row;

		}

		$output['sql'] = $sql;

		return $output;
	}
	
	function PrintArray($array){
	
		$info = '';
		
		if(is_array($array)){
			foreach($array as $key=>$value){
				if(is_array($value)){
				// the value of the current array is also a array, so call this function again to process that array
					$this->PrintArray($value);
				}else{
				// This part of the array is just a key/value pair
					$info.= "$key: $value<br>";
				}
			}
		}
		
		return $info;
	}

	function getSOWRef( $po_number, $group ) {
		$security = new security();
		$data = '<parameters>
		<method>order</method>
		<type>SoW</type>
		<reference>' . $po_number . '</reference>
		</parameters>';
		$webservice = new webservice;
		$session_id = $security->gen_serial(16);
		$xml_result = $webservice->qube_execute(3, 'getassets', $data, $group, $session_id);
		$log = new log();
		if(!is_null($xml_result['code'])){
			$output_html = "XML is invalid/Curl is unavailable. Http response: " . $xml_result['code'] . " XML = " . $xml_result['desc'];
			$log->string_log( 'getassets fail for ' . $po_number, $output_html );
			return array($xml_result['code'], $output_html);
		}else {
			$return_code = $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['assets'][0]['code'];
			if($return_code != "00"){ //service unavailable
				$output_html = $xml_result['code'] . " - Error code: " . $return_code . " - " . $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['assets'][0]['desc'] . " XML = " . $data;
				$log->string_log( 'getassets fail for ' . $po_number, $output_html );
				return array($return_code, $output_html);
			}else{
				$lines = $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['assets'][0]['lines'];
				if( $lines != "" ) {
					return $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['assets'][0]['asset'][0]['ref'];
				}else{
					return array(0, 'No lines');
				}
			}
		}
	}
}

?>	