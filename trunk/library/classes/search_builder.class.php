<?php

class search_builder {
	
	var $columns = array();
	var $field_array = array();
	var $order = array();
	var $group = array();
	var $filter = array();
	var $overdue = array();
	var $index_column = array();
	var $relevance = 1;
	var $sql = '';
	var $paging = '';
	var $relevance_array = array();
	
	function __construct(){
	}

	/**
	 * Add Index Column
	 *
	 * @param	string	$colum_name		Column Name
	 *
	 **/
	function add_index_column($column_name){
		
		$this->index_column[] = $column_name;
		
	}

	/**
	 * Add Column
	 *
	 * @param	string	$string		Column Name		OR
	 * @param	array	$string		array(['data'], ['format'])
	 *
	 **/
	function add_column($string){
		$this->columns[] = $string;
	}

	/**
	 * Add Multi Column
	 *
	 * @param	array	$array		Columns
	 *
	 **/
	function add_multi_column($array){
		$temp_array = array();
		$temp_array['data'] = $array;
		$temp_array['format'] = 'multi';
		$this->columns[] = $temp_array;
	}

	/**
	 * Add Tick Column
	 *
	 * @param	string	$column_name	Column Name
	 * @param	array	$selected		Array of selected rows
	 *
	 **/
	function add_tick_column($column_name, $selected=array()){
		$temp_array = array();
		$temp_array['data'] = $column_name;
		$temp_array['format'] = 'tick';
		$temp_array['selected'] = $selected;
		$this->columns[] = $temp_array;
	}

	/**
	 * Add Tick Column
	 *
	 * @param	string	$column_name	Column Name
	 * @param	array	$selected		Array of selected rows
	 *
	 **/
	function add_selector_column( $class="" ){
		$temp_array = array();
		$temp_array['data'] = '';
		$temp_array['format'] = 'selector';
		$temp_array['class'] = $class;
		$this->columns[] = $temp_array;
	}

	/**
	 * Add Function Column
	 *
	 * @param	string	$column_name	Column Name
	 * @param	string	$class			Module
	 * @param	string	$function		Function
	 * @param	array	$parameters		Array of parameters
	 *
	 **/
	function add_function_column($column_name, $class, $function, $parameters=array()){
		$temp_array = array();
		$temp_array['data'] = $column_name;
		$temp_array['format'] = 'function';
		$temp_array['module'] = $class;
		$temp_array['function'] = $function;
		$temp_array['params'] = $parameters;
		$this->columns[] = $temp_array;
	}

	/**
	 * Add Button Column
	 *
	 * @param	string	$column_name	Column Name
	 * @param	string	$button_text	Button Text
	 * @param	string	$function		Function
	 * @param	string	$class			CSS Class
	 * @param	array	$parameters		Array of parameters
	 *
	 **/
	function add_button_column($column_name, $button_text, $function, $parameters=array(), $class="jButton"){
		$temp_array = array();
		$temp_array['data'] = $column_name;
		$temp_array['format'] = 'button';
		$temp_array['text'] = $button_text;
		$temp_array['class'] = $class;
		$temp_array['function'] = $function;
		$temp_array['params'] = $parameters;
		$this->columns[] = $temp_array;
	}

	/**
	 * Add Button Column
	 *
	 * @param	string	$column_name		Column Name
	 * @param	string	$button_text		Button Text
	 * @param	string	$button_function	Function
	 * @param	string	$class				CSS Class
	 * @param	array	$parameters			Array of parameters
	 *
	 **/
	function add_function_button_column($column_name, $button_text, $button_function, $button_parameters=array(), $class='', $function='', $function_parameters=array()){
		$temp_array = array();
		$temp_array['data'] = $column_name;
		$temp_array['format'] = 'function_button';
		$temp_array['text'] = $button_text;
		$temp_array['button_function'] = $button_function;
		$temp_array['button_params'] = $button_parameters;
		$temp_array['module'] = $class;
		$temp_array['function'] = $function;
		$temp_array['params'] = $function_parameters;
		$this->columns[] = $temp_array;
	}

	/**
	 * Add table SQL Statement
	 *
	 * @param	string	$sql		Partial SQL Statement
	 *
	 **/
	function add_sql($sql){
		$this->sql = $sql;
	}

	/**
	 * Add Paging
	 *
	 * @param	int		$start			Starting record
	 * @param	int		$num			Number of records to show
	 *
	 **/
	function paging($start, $num){
		
		$limit = " LIMIT ".intval( $start ).", ".intval( $num );
		$this->paging = $limit;
		
		return $limit;
		
	}

	/**
	 * Add Join
	 *
	 * @param	string	$sql		Mysql JOIN Statement
	 *
	 **/
	function add_join($sql){
		
		$this->sql .= "
		" . $sql;
		
	}

	/**
	 * Add Order
	 *
	 * @param	string	$colum_name		Column Name
	 * @param	string	$direction		Direction of order "ASC" or "DESC"
	 *
	 **/
	function add_order($column_name, $direction){
		
		$order_column = $column_name;
		
		if(is_array($column_name)){
			$order_column = $column_name['data'];
		}
		
		if(is_array($order_column)){
			$order_column = $order_column[count($order_column)-1];
		}
		
		$this->order[] = array($order_column, $direction);
		
	}

	/**
	 * Add Filter
	 *
	 * @param	string	$column			Column Name
	 * @param	string	$condition		Comparison type I.E. =, LIKE, MATCH AGAINST
	 * @param	string	$value			Value
	 * @param	string	$operator		Operator "AND" or "OR"
	 *
	 **/
	function add_filter($column = '', $condition = '', $value = '', $operator){
		
		$security = new security();
		$value = $security->clean_query($value);

		if(!is_array($column)){
			$col_data = $column;
		}else{
			
			$col_format = $column['format'];
			$col_data = $column['data'];
		
			// If col_data is an array then we need to CONCAT the columns
			if(is_array($col_data)){
				
				if($col_format == 'multi'){
					
					$sql_concat = "CONCAT(";
					$multi_loop = 0;
						
					foreach($col_data as $column_name){
						if($multi_loop % 2 == 0){
							$sql_concat .= $column_name . ',';
						}else{
							$sql_concat .= "'" . $column_name . "',";
						}
			
						$multi_loop++;
					}
			
					$sql_concat = substr($sql_concat, 0, -1);
					$sql_concat .= ") ";
						
					$col_data = $sql_concat;
				}
			}
		}
		
		// If column name is not empty
		if($col_data != ''){
			// If column name does not contain a . or , surround it in `
			if(strpos($col_data, '.') === false && strpos($col_data, ',') === false){
				$col_data = '`' . $col_data . '`';
			}
			$col_data .= ' ';
		}
		
		switch($condition) {
			case 'MATCH AGAINST':
				$this->filter[] = array('(', 'AND');
				$this->filter[] = array("MATCH (" . $col_data . ") AGAINST ('" . $value . "')", $operator);
				if(strpos($value, ' ') === false){
					$this->filter[] = array("MATCH (" . $col_data . ") AGAINST ('" . $value . "*' IN BOOLEAN MODE)", 'OR');
				}else{
					$terms = explode(' ', $value);
					
					$value = '';
					
					foreach($terms as $term){
						$value .= $term.'* ';
					}
					
					$value = substr($value, 0, -1);
					
					$this->filter[] = array("MATCH (" . $col_data . ") AGAINST ('" . $value . "' IN BOOLEAN MODE)", 'OR');
				}
				$this->filter[] = array(')', $operator);
				break;
			default:
		
				// If value is not blank and doesnt contain and . surround in quotes
				if($value != "''" && $value != '' && $value != "NULL" && strpos($value, '.') === false){
					$value = " '" . $value . "'";
				}
					
				$this->filter[] = array($col_data . $condition . $value, $operator);
				break;
		}
	}

	/**
	 * Add Group
	 *
	 * @param	string	$column			Column Name
	 *
	 **/
	function add_group($column = ''){
		
		$this->group[] = $column;
		
	}

	/**
	 * Add Overdue
	 *
	 * @param	string	$column			Column Name
	 * @param	string	$condition		Comparison type I.E. =, >, >=, etc.
	 * @param	string	$value			Value
	 *
	 **/
	function add_overdue($column = '', $condition = '', $value = ''){
		
		$this->overdue[] = array($column, $condition, $value);
		
	}

	/**
	 * Add Relevance Ordering
	 *
	 * @param	string	$column			Column Name
	 * @param	string	$value			Value
	 * @param	string	$direction		Direction of order "ASC" or "DESC"
	 *
	 **/
	function add_relevance_order($column, $value, $direction){
		
		$security = new security;
		
		// Relevance for all words
		$orderresult = "
		(MATCH (" .$column. ") AGAINST ('";
		
		$search_term = explode(' ', $value);
			
		// Append terms to search string
		foreach ($search_term as &$value){
			$orderresult .= "+" . $security->clean_query($value) . " ";
		}
		
		$orderresult = substr($orderresult,0,strlen($orderresult)-1);
			
		$orderresult .= "' IN BOOLEAN MODE)) as all_match_" . $this->relevance;
		
		$this->add_order("all_match_" . $this->relevance, "DESC");
		
		// Relevance for at least one word			
		$orderresult .= ",
		(MATCH (" .$column. ") AGAINST ('";
			
		// Append terms to search string
		foreach ($search_term as &$value){
			$orderresult .= $security->clean_query($value) . " ";
		}
			
		$orderresult = substr($orderresult,0,strlen($orderresult)-1);
			
		$orderresult .= "' IN BOOLEAN MODE)) as at_least_one_match_" . $this->relevance;
		$this->add_order("at_least_one_match_" . $this->relevance, "DESC");

		// Relevance for all partial word
		$orderresult .= ",
		(MATCH (" .$column. ") AGAINST ('";
			
		// Append terms to search string

		foreach ($search_term as &$value){
			$orderresult .= $security->clean_query($value) . "* ";
		}
		$orderresult = substr($orderresult,0,strlen($orderresult)-1);
		
		$orderresult .= "' IN BOOLEAN MODE)) as partial_match_" . $this->relevance;
		$this->add_order("partial_match_" . $this->relevance, "DESC");
		
		$this->relevance += 1;

		$this->relevance_array[] = $orderresult;
		
		return $orderresult;
	}

	/**
	 * Add Filter to all columns
	 *
	 * @param	string	$value			Value
	 *
	 **/
	function all_column_filter($value){
		$this->add_filter('', "(", "", 'AND');
		foreach($this->columns as $column){

			if(!is_array($column)){
				$col_data = $column;
			}else{
					
				$col_format = $column['format'];
				$col_data = $column['data'];
			
				// If col_data is an array then we need to CONCAT the columns
				if(is_array($col_data)){
			
					if($col_format == 'multi'){
							
						$sql_concat = "CONCAT(";
						$multi_loop = 0;
			
						foreach($col_data as $column_name){
							if($multi_loop % 2 == 0){
								$sql_concat .= $column_name . ',';
							}else{
								$sql_concat .= "'" . $column_name . "',";
							}
								
							$multi_loop++;
						}
							
						$sql_concat = substr($sql_concat, 0, -1);
						$sql_concat .= ") ";
			
						$col_data = $sql_concat;
					}
				}
			}
			
			if($col_data != ''){
				$this->add_filter($col_data, 'LIKE', '%'.$value.'%', 'OR');
			}
		}
		$this->add_filter('', ")", "", 'AND');
	}

	/**
	 * Turn page requests in to array
	 *
	 * @param	array	$request		$_REQUEST variable
	 * @param	string	$type			String to hold the type variable
	 * @return	array	$field_array	Array of variables
	 *
	 **/
	function request_to_array($request, $type){
		$no_fields = $request['no_fields'];
		$field_array = array();
		
		if ($no_fields > 0 ){
			for ($i = 1; $i <= $no_fields; $i++) {
		
				if(isset($request[$type . $request[$type . 'type'.$i] . $i . '_node_radios-id_answer'])){
					$search_term =  trim($request[$type . $request[$type . 'type'.$i] . $i . '_node_radios-id_answer']);
				}elseif(isset($request[$type . $request[$type . 'type'.$i] . '_input' . $i])){
					$search_term =  trim($request[$type . $request[$type . 'type'.$i] . '_input' . $i]);
				}elseif($request[$type . $_REQUEST[$type . 'type'.$i] . $i. '_node_radios-id_answer']){
					$search_term =  trim($request[$type . $_REQUEST[$type . 'type'.$i] . $i. '_node_radios-id_answer']);
				}else{
					$search_term =  trim($request[$type . $request[$type . 'type'.$i] . $i . '_input']);
				}
				$field = $request[$type . 'type'.$i];
				
				$field_array[$field] = $search_term;
			}
		}
		
		$this->field_array = $field_array;
		
		return $field_array;
	}

	/**
	 * Return Where clause
	 *
	 * @return	string	$sWhere		Mysql where clause
	 *
	 **/
	function return_where(){
		
		$sWhere = '';
		$no_start = true;
		$last_filter = '';
		
		if(count($this->filter) > 0){
			$sWhere = '
			WHERE ';
			
			for($i = 0; $i < count($this->filter); $i++){
				$filter = $this->filter[$i];
				
				if($i == 0 ||  $last_filter == '(' || $filter[0] == ')'){
					$no_start = true;
				}
				
				if($no_start == false){
					$sWhere .= ' ' . $filter[1] . ' ';
				}

				$sWhere .= $filter[0];
				$no_start = false;
				$last_filter = $filter[0];
			}
		}
		
		return $sWhere;
	}

	/**
	 * Return Order clause
	 *
	 * @return	string	$sOrder		Mysql order clause
	 *
	 **/
	function return_order(){


		$sOrder = '';
		
		if(count($this->order) > 0){
			$sOrder = '
			ORDER BY ';
			
			for($i = 0; $i < count($this->order); $i++){
				$order = $this->order[$i];
				$sOrder .= implode(' ', $order) . ', ';
			}
			
			$sOrder = substr($sOrder, 0, -2);
		}
		
		return $sOrder;
	}
	
	/**
	 * Return Group clause
	 *
	 * @return	string	$sGroup		Mysql group clause
	 *
	 **/
	function return_group(){


		$sGroup = '';
		
		if(count($this->group) > 0){
			$sGroup = '
			GROUP BY ';
			
			for($i = 0; $i < count($this->group); $i++){
				$group = $this->group[$i];
				$sGroup .= $group . ', ';
			}
			
			$sGroup = substr($sGroup, 0, -2);
		}
		
		return $sGroup;
	}

	/**
	 * Return Limit Clause
	 *
	 * @return	string	$sOrder		Mysql order clause
	 *
	 **/
	function return_paging(){
		
		return $this->paging;
	}

	/**
	 * Output data
	 *
	 * @param	array	$request	$_REQUEST variable
	 * @return	array	$output		Datatables output
	 *
	 **/
	function output($request){
		
		$mysql = new mysql;
		$data = new data;
		
		$sql_main = "SELECT ";
		
		// Generate columns for SELECT statement
		for($i=0;$i<count($this->columns);$i++){
			
			$column = $this->columns[$i];
			
			if(!is_array($column)){
				if(strpos($sql_main, $column.',') === false){
					$sql_main .= $column .", ";
				}
			}else{
				$col_format = $column['format'];
				$col_data = $column['data'];
				
				// If col_data is an array then we need to CONCAT the columns
				if(is_array($col_data)){
					
					if($col_format == 'multi'){
						$sql_concat = "CONCAT(";
						$multi_loop = 0;
						
						foreach($col_data as $column_name){
							if($multi_loop % 2 == 0){
								
								$col_text = $column_name;
								$pos = strpos(strtoupper($col_text), ') AS ');
									
								if($pos !== false){
									$col_text = substr($col_text, 0, $pos+1);
								}
								
								$sql_concat .= $col_text . ',';
								if(strpos($sql_main, $col_text.',') === false){
									$sql_main .= $column_name . ', ';
								}
							}else{
								$sql_concat .= "'" . $column_name . "',";
							}
							
							$multi_loop++;
						}
	
						$sql_concat = substr($sql_concat, 0, -1);
						$sql_concat .= ") as combined_".$i.", ";
						
						$this->columns[$i]['multi_name'] = "combined_".$i;
						
						$sql_main .= $sql_concat;
					}
				// If it's not an array then it just has a format
				}else{
					if(strpos($sql_main, $col_data.',') === false){
						$sql_main .= $col_data .", ";
					}
				}
			}
		}
		
		$sql_main = substr($sql_main, 0, -2);
		
		// Add index columns to SELECT columns
		for($i=0;$i<count($this->index_column);$i++){
			$column = $this->index_column[$i];
				
			if(strpos($sql_main, $column.',') === false){
				$sql_main .= ", " . $column;
			}
		}
		
		// Add overdue columns to SELECT columns
		for($i=0;$i<count($this->overdue);$i++){
			$column = $this->overdue[$i];
			
			if(strpos($sql_main, $column[0].',') === false){
				$sql_main .= ", " . $column[0];
			}
		}
		
		// Add relevance columns to SELECT columns
		for($i=0;$i<count($this->relevance_array);$i++){
			$column = $this->relevance_array[$i];
			
			if(strpos($sql_main, $column.',') === false){
				$sql_main .= ", " . $column;
			}
		}
		
		// Build main SQL statment
		$sql_main .= "
		" . $this->sql . $this->return_where() . $this->return_group() . $this->return_order();
		$result = $mysql->query($sql_main, 'Find Filtered Results');
		$iFilteredTotal = $mysql->num_rows($result);
		
		// Build SQL with page limits
		$sql_main.= $this->return_paging();
		$result = $mysql->query($sql_main, 'Get Results');
		
		$sEcho = 0;
		if(isset($request['sEcho'])){
			$sEcho = $request['sEcho'];
		}
		
		$output = array(
				"sEcho"                => intval($sEcho),
				"iTotalRecords"        => 0,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData"               => array()
		);
		
		while($aRow = $mysql->fetch_array($result)){
			$row = array();
			for ( $i=0 ; $i<count($this->columns) ; $i++ ) {
				$col_format = '';
				
				if(is_array($this->columns[$i])){
					$col_format = $this->columns[$i]['format'];
					$col_data = $this->columns[$i]['data'];
				}else{
					$col_data = $this->columns[$i];
				}
				
				if($col_format == 'date'){
					// Format data to date
					$row[] = $data->ymd_to_date($aRow[ $this->format_col_name($col_data) ]);
				}elseif($col_format == 'yes_no'){
					// Format Y to Yes, N to No
					if($aRow[ $this->format_col_name($col_data) ] == 'Y'){
						$row[] = 'Yes';
					}elseif($aRow[ $this->format_col_name($col_data) ] == 'N'){
						$row[] = 'No';
					}else{
						$row[] = '&nbsp;';
					}
				}elseif($col_format == 'locked'){
					// Show locked icon if Y
					if($aRow[$this->format_col_name($col_data)] == "Y"){
						$row[] = '<i class="icon-icomoon-lock"></i>';
					}else{
						$row[] = '&nbsp;';
					}
				}elseif($col_format == 'multi'){
					// Multiple column format i.e [rmc_num][divider][group_num]
					// Even array elements are dividers, odds are column names
					$multi_loop = 0;
					
					$column_data = '';
					
					foreach($col_data as $column){
						
						if($multi_loop % 2 == 0){
							
							$col_text = $column;
							$pos = strpos(strtoupper($col_text), ') AS ');
							
							if($pos !== false){
								$col_text = substr($col_text, $pos+5);
							}else{
								$col_text = $this->format_col_name($col_text);
							}
							
							$column_data .= $aRow[ $col_text ];
						}else{
							$column_data .= $column;
						}
						
						$multi_loop++;
					}
					
					$row[] = $aRow[ $this->format_col_name($col_multi_name) ];
					
				}elseif($col_format == 'function'){
					// Run a function with the column
					$col_module = $this->columns[$i]['module'];
					$col_function = $this->columns[$i]['function'];
					$col_params = $this->columns[$i]['params'];
					
					$col_params = array_merge(array($aRow[ $this->format_col_name($col_data) ]), $col_params);
					
					// If a specific module has been specified, call function in class
					if($col_module != ''){
						$module = new $col_module;
					
						$item = call_user_func_array(array($module, $col_function), $col_params);
					// Otherwise call PHP function
					}else{
						$item = call_user_func_array($col_function, $col_params);
					}
					$row[] = $item;
				}elseif($col_format == 'tick'){
					// Add jquery.radio placeholder
					$is_ticked = '';
					
					$col_results = $this->columns[$i]['selected'];
					
					if(count($col_results) > 0){
						foreach($col_results as $sa){
							if($sa == $aRow[$this->format_col_name($col_data)]){
								$is_ticked = 'ticked_1';
							}
						}
					}
					
					$row[] = '<div href="javascript:;" class="tickbox '.$is_ticked.'"></div>';
				}elseif($col_format == 'selector'){
					// Add selector placeholder
					$class = $this->columns[$i]['class'];
					
					$row[] = '<span class="'.$class.'"></span>';
				}elseif($col_format == 'button'){
					// Create a button
					$col_text = $this->columns[$i]['text'];
					$col_class = $this->columns[$i]['class'];
					$col_function = $this->columns[$i]['function'];
					
					$col_params = array_merge($this->columns[$i]['params'], array($aRow[ $this->format_col_name($col_data) ]));
					
					$func_params = '';
					if(count($col_params) > 0){
						foreach($col_params as $param){
							$func_params .= "'".$param."', ";
						}
						$func_params = substr($func_params, 0, -2);
					}
						
					$row[] = '<a href="javascript:;" class="'.$col_class.'" onclick="'.$col_function.'('.$func_params.');">'.$col_text.'</a>';
				}elseif($col_format == 'function_button'){
					// Create a button
					$col_array = $this->columns[$i];
					$col_array['button_class'] = 'jButton';
					
					$col_module = $this->columns[$i]['module'];
					$col_function = $this->columns[$i]['function'];
						
					$col_params = array_merge($this->columns[$i]['params'], array($aRow[ $this->format_col_name($col_data) ]));
					
					if($col_module != ''){
						$module = new $col_module;
							
						$item = call_user_func_array(array($module, $col_function), $col_params);
						// Otherwise call PHP function
					}else{
						$item = call_user_func_array($col_function, $col_params);
					}
					
					$col_array = array_merge($col_array, $item);
						
					$col_params = array_merge($this->columns[$i]['button_params'], array($aRow[ $this->format_col_name($col_data) ]));
					
					$func_params = '';
					if(count($col_params) > 0){
						foreach($col_params as $param){
							$func_params .= "'".$param."', ";
						}
						$func_params = substr($func_params, 0, -2);
					}
					
					$row[] = '<a href="javascript:;" class="'.$col_array['button_class'].'" onclick="'.$col_array['button_function'].'('.$func_params.');">'.$col_array['text'].'</a>';
				}else{
					// General output
					$row[] = $aRow[ $this->format_col_name($col_data) ];
				}
			}
			
			// Colour row if overdue
			$overdue_met = $this->is_overdue($aRow);
		
			if($overdue_met){
				$row["DT_RowClass"] = 'overdue';
			}
			
			$index_value = '';
			
			for($i=0;$i<count($this->index_column);$i++){
				$column = $this->index_column[$i];
			
				$index_value .= $aRow[$this->format_col_name($column)] . ',';
			}
			
			if($index_value != ''){
				$index_value = substr($index_value, 0, -1);
			}
			
			$row["DT_RowId"] = $index_value;
		
			$output['aaData'][] = $row;
		
		}
		
		$output['sql'] = $sql_main;
		
		return $output;
	}

	/**
	 * Check if row is overdue
	 *
	 * @param	array	$aRow			mySQL row
	 * @return	boolean	$overdue_met	Is overdue
	 *
	 **/
	function is_overdue($aRow){
				
		$overdues = 0;
		$overdues_met = array();
		
		foreach($this->overdue as $overdue_rule){
			$overdue_col = $this->format_col_name($overdue_rule[0]);
			$overdue_operator = $overdue_rule[1];
			$overdue_value = $overdue_rule[2];
		
			switch($overdue_operator){
					
				case "<":
					if($aRow[$overdue_col] < $overdue_value){
						$overdues_met[$overdues] = true;
					}else{
						$overdues_met[$overdues] = false;
					}
					break;
				case "<=":
					if($aRow[$overdue_col] <= $overdue_value){
						$overdues_met[$overdues] = true;
					}else{
						$overdues_met[$overdues] = false;
					}
					break;
				case ">":
					if($aRow[$overdue_col] > $overdue_value){
						$overdues_met[$overdues] = true;
					}else{
						$overdues_met[$overdues] = false;
					}
					break;
				case ">=":
					if($aRow[$overdue_col] >= $overdue_value){
						$overdues_met[$overdues] = true;
					}else{
						$overdues_met[$overdues] = false;
					}
					break;
				case "<>":
					if($aRow[$overdue_col] <> $overdue_value){
						$overdues_met[$overdues] = true;
					}else{
						$overdues_met[$overdues] = false;
					}
					break;
				default:
					if($aRow[$overdue_col] == $overdue_value){
						$overdues_met[$overdues] = true;
					}else{
						$overdues_met[$overdues] = false;
					}
					break;
			}
		
			$overdues++;
		}
			
		$overdue_met = true;
			
		if(count($overdues_met) > 0){
				
			foreach($overdues_met as $overdue_rule){
				if($overdue_rule == false){
					$overdue_met = false;
				}
			}
		}else{
			$overdue_met = false;
		}
		
		return $overdue_met;
	}

	/**
	 * Format Column name for mysql results
	 *
	 * @param	string	$string		Column Name
	 * @return	string	$string		Column Name
	 *
	 **/
	function format_col_name($string){
	
		if(strpos($string, '.') !== false){
			$array = explode('.',$string);
			$string = $array[1];
		}
		
		return $string;
	
	}
}

?>
