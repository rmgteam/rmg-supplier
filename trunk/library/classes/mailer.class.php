<?
require_once($UTILS_CLASS_PATH."field.class.php");
require_once($UTILS_CLASS_PATH."mysql.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."pdf/html2text.class.php");

class mailer {
	

	// SET
	function set_mail_type($str){
		$this->row['mail_type'] = $str;
	}

	function set_mail_to($to){
		
		$field = new field();
		
		if(is_array($to)){
			foreach($to as $rec){
				if($field->is_valid_email($rec)){
					$to_str .= $rec.",";
				}
			}
			$to_str = substr($to_str, 0, -1);
		}
		else{
			$to_str = $to;
		}	
		$this->row['mail_to'] = $to_str;
	}
	
	function set_mail_from($from){
		$field = new field();
		
		if($field->is_valid_email($from)){
			$this->row['mail_from'] = $from;
		}
	}
	
	function set_mail_subject($subject){
		$this->row['mail_subject'] = $subject;
	}
	
	function set_mail_message($message){
		$this->row['mail_message'] = $message;
	}
	
	function set_mail_html($message, $convert=true){
		$this->row['mail_html'] = $message;
		
		if($convert == true){
			$convert_html_to_text = new html2text();
			$string =  $convert_html_to_text->convert_html_to_text($message);
			
			if(gettype($string) == "string"){
				$this->row['mail_message'] = $string;
			}else{
				print(gettype($string));
			}
		}
	}
	
	function set_mail_headers($headers){
		$this->row['mail_headers'] = $headers;
	}
	
	function set_mail_attachment($file, $del){
		$this->row['mail_attachment'] = $file;
		$this->row['mail_del_attach'] = $del;
	}
	
	function add_embed_image($image, $cid=NULL){
		
		if($cid != NULL){
			$image = $image."|".$cid.";";	
		}
		
		$this->row['mail_images'] .= $image;
	}
	
	function add_mail_attachment($file){
		$this->row['mail_attachment'] .= ';' . $file;
	}
	
	function mail_ready_to_send(){
		return true;
	}
	
	function send(){
		$security = new security();
		$mysql = new mysql();
	
		$sql = "
		INSERT INTO cpm_mailer SET
		mail_type = '".$security->clean_query($this->get_mail_type())."',
		mail_to = '".$security->clean_query($this->get_mail_to())."',
		mail_from = '".$security->clean_query($this->get_mail_from())."',
		mail_subject = '".$security->clean_query($this->get_mail_subject())."',
		mail_message = '".$security->clean_query($this->get_mail_message())."',
		mail_html = '".$security->clean_query($this->get_mail_html())."',
		mail_images = '".$security->clean_query($this->get_mail_images())."',
		mail_attachment = '".addslashes($this->get_mail_attachment())."',
		mail_del_attach = '".$security->clean_query($this->get_mail_del_attach())."',
		mail_headers = '".$security->clean_query($this->get_mail_headers())."'";
			
		if($this->mail_ready_to_send()){
			$has_error = $mysql->insert($sql, 'Insert Mail');
			
			if (!is_bool($has_error)){
				return true;
			}else{
				return $sql . '-' . $has_error;
			}
		}else{
			return $sql;
		}
	}
	
	
	// GET
	function get_mail_to(){
		return $this->row['mail_to'];
	}
	
	function get_mail_from(){
		
		Global $UTILS_ADMIN_EMAIL;
		
		if($this->row['mail_from'] == ""){
			return $UTILS_ADMIN_EMAIL;
		}else{
			return $this->row['mail_from'];
		}
	}
	
	function get_mail_subject(){
		return $this->row['mail_subject'];
	}
	
	function get_mail_type(){
		return $this->row['mail_type'];
	}
	
	function get_mail_message(){
		return $this->row['mail_message'];
	}
	
	function get_mail_html(){
		return $this->row['mail_html'];
	}
	
	function get_mail_images(){
		return $this->row['mail_images'];
	}
	
	function get_mail_attachment(){
		return $this->row['mail_attachment'];
	}
	
	function get_mail_del_attach(){
		if($this->row['mail_del_attach'] == ''){
			$this->row['mail_del_attach'] = 'N';	
		}
		return $this->row['mail_del_attach'];
	}
	
	function get_mail_headers(){
		return $this->row['mail_headers'];
	}
	
	function string_to_html($string){
		$lines = preg_split( '/\r\n|\r|\n/', $string );
		
		$new_string = '';
		$i = 0;
		
		foreach ($lines as &$line){
			if($line == '' && $i == 0){
				
			}else{
			
				if($line == ''){
					$new_string = substr($new_string, 0, strlen($new_string) - 4) . '<br />' . substr($new_string, strlen($new_string) - 4, strlen($new_string));
				}else{
					$regex = '%^((https?://)|(www\.))([a-z0-9-].?)+(:[0-9]+)?(/.*)?$%i';
					preg_match($regex, $line, $matches);
					if(count($matches) > 0){
					 	$line = '<a href="' . $line . '">Click Here</a>';
					}else{
						$regex = "/\\\\/s";
						preg_match($regex, $line, $matches);
						if(count($matches) > 0){
							$line = '<a href="' . $line . '">Click Here</a>';
						}
					}
					
					$new_string .= '<p>' . $line . '</p>';
				}				
			}
			$i++;
		}
		
		$output = '<html>
		<body>
		<font face="verdana" size="2">
		' . $new_string . '
		</font></body></html>';
		
		return $output;
	}

}

?>