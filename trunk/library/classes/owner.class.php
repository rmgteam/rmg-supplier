<?php
require_once($UTILS_CLASS_PATH."subsidiary.class.php");


class owner {
	
	
	var $owner_id;
	var $owner_ref;
	var $owner_name;
	var $owner_co_house_num;
	var $subsidiary_id;
	var $owner_is_hcs;
	var $owner_is_dir;
	var $date_created;
	var $date_updated;
	
	
	function owner($ref="", $ref_type="id", $subsidiary_id=""){
		
		if( $ref_type == "ref" ){
			$ref_clause = " owner_ref = '".$ref."' ";
			if($subsidiary_id != ""){
				$ref_clause = " subsidiary_id = ".$subsidiary_id." AND ".$ref_clause;
			}
		}
		else{
			$ref_clause = " owner_id = ".$ref." ";
			if($subsidiary_id != ""){
				$ref_clause = " subsidiary_id = ".$subsidiary_id." AND ".$ref_clause;
			}
		}
		
		$sql = "
		SELECT * 
		FROM cpm_owner 
		WHERE 
		".$ref_clause;
		$result = @mysql_query($sql);
		$row = @mysql_fetch_array($result);
		
		$this->owner_id = $row['owner_id'];
		$this->owner_ref = $row['owner_ref'];
		$this->owner_name = $row['owner_name'];
		$this->owner_co_house_num = $row['owner_co_house_num'];
		$this->subsidiary_id = $row['subsidiary_id'];
		$this->owner_is_hcs = $row['owner_is_hcs'];
		$this->owner_is_dir = $row['owner_is_dir'];
		$this->date_created = $row['date_created'];
		$this->date_updated = $row['date_updated'];
	}
	
	
	function get_intranet_owner_id(){
		
		global $UTILS_INTRANET_DB_LINK;
		
		$subsidiary = new subsidiary($this->subsidiary_id);
		
		// Get intranet-based subsidiary_id
		$sql_s = "
		SELECT subsidiary_id 
		FROM subsidiary 
		WHERE 
		subsidiary_code <> '' AND 
		subsidiary_code IS NOT NULL AND 
		subsidiary_code = '".$subsidiary->subsidiary_code."'";
		$result_s = @mysql_query($sql_s, $UTILS_INTRANET_DB_LINK);
		$row_s = @mysql_fetch_row($result_s);
		$int_subsidiary_id = $row_s[0];
		
		$sql = "
		SELECT owner_id 
		FROM owner 
		WHERE 
		owner_ref = '".$this->owner_ref."' AND 
		subsidiary_id = ".$int_subsidiary_id." 
		";
		$result = @mysql_query($sql, $UTILS_INTRANET_DB_LINK);
		$row = @mysql_fetch_row($result);
		
		return $row[0];
	}
	
	
}



?>