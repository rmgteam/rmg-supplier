<?
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."field.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."search_builder.class.php");

class contractors {
	
	var $contractor_qube_id;
	var $contractor_name;
	var $contractor_disabled;
	var $contractor_default_outercodes;
	var $contractor_email;
	var $contractor_address;
	var $contractor_pcm;
	
	function contractors($contractor_ref){
		
		$this->contractor_qube_id = '';
		$this->contractor_name = '';
		$this->contractor_disabled = '';
		$this->contractor_default_outercodes = '';
		$this->contractor_email = '';
		$this->contractor_address = '';
		$this->contractor_pcm = '';
		
		$sql = "
		SELECT * 
		FROM cpm_contractors
		WHERE
		cpm_contractors_qube_id = '".$contractor_ref."'";
		$result = @mysql_query($sql);
		$num_results = @mysql_num_rows($result);
		if($num_results > 0){			
			while($row = @mysql_fetch_array($result)){	
				$this->contractor_qube_id = $row['cpm_contractors_qube_id'];
				$this->contractor_name = $row['cpm_contractors_name'];
				$this->contractor_disabled = $row['cpm_contractors_disabled'];
				$this->contractor_default_outercodes = $row['cpm_contractor_default_outercodes'];
				$this->contractor_email = $row['cpm_contractors_email'];
				$this->contractor_address = $row['cpm_contractors_address'];
				$this->contractor_pcm = $row['cpm_contractors_pcm'];
			}
		}
	}
	
	function contractor_list($request){
		$sql = "SELECT *
		FROM cpm_contractors
		WHERE (";
		
		$search_term = explode(" ", $request['search_term']);
		
		foreach ($search_term as &$term){
			$sql .= "cpm_contractors_name LIKE '%".$term."%' OR ";
			$sql .= "cpm_contractors_qube_id LIKE '%".$term."%' OR ";
		}
		
		$sql = substr($sql,0,-3);
		
		$sql .=")
		ORDER BY cpm_contractors_name";
		
		$result = @mysql_query($sql);
		
		return $result;
	}

	function get_list ($request)
	{

		$mysql = new mysql();
		$data = new data();
		$search_builder = new search_builder;
		$security = new security();

		$input = $request;

		$contractors = '';

		if(isset($request['search_term'])) {

			// Get id's for select all
			$sql = "SELECT * FROM cpm_contractors WHERE cpm_contractors_letter_sent = 'N'
			AND (
				cpm_contractors_name LIKE '%".$security->clean_query($request['search_term'])."%'
				OR cpm_contractors_qube_id LIKE '%".$security->clean_query($request['search_term'])."%'
			)";

			$result = $mysql->query($sql, 'Get all contractors - no letter sent');
			$num_results = $mysql->num_rows($result);
			if ($num_results > 0) {
				while ($row = $mysql->fetch_array($result)) {
					$contractors .= $row['cpm_contractors_qube_id'] . ',';
				}
				$contractors = substr($contractors, 0, -1);
			}
		}
	
		$search_builder->add_index_column('cpm_contractors_qube_id');
		
		if(isset($request['results_table_all_node_radios-1_answer'])){
			if($request['results_table_all_node_radios-1_answer'] == '1'){
				$request['contractors'] = $contractors;
			}
		}
		
		if(isset($request['letters'])){
			$search_builder->add_tick_column('cpm_contractors_qube_id', explode(',',$request['contractors']));
			$search_builder->add_filter('cpm_contractors_letter_sent', "=", "N", 'AND');
		}
		
		$search_builder->add_column('cpm_contractors_qube_id');
		$search_builder->add_column('cpm_contractors_name');
		$search_builder->add_column('cpm_contractors_email');
		$iColumnCount = count($search_builder->columns);

		if(isset($request['search_term'])) {
			$search_builder->add_filter('', "(", "", 'AND');
			$search_builder->add_filter('cpm_contractors_name', "LIKE", "%" . $security->clean_query($request['search_term']) . "%", 'OR');
			$search_builder->add_filter('cpm_contractors_qube_id', "LIKE", "%" . $security->clean_query($request['search_term']) . "%", 'OR');
			$search_builder->add_filter('', ")", "", 'AND');
		}else{

			$search_term = $security->clean_query($request['search_term_input']);
			$search_builder->add_filter('cpm_contractors_qube_id', "=", $security->clean_query($search_term), 'AND');
		}
		
		//Paging
		if ( isset( $request['iDisplayStart'] ) && $request['iDisplayLength'] != '-1' ) {
			$search_builder->paging($request['iDisplayStart'], $request['iDisplayLength']);
		}
	
		//Ordering
		$iSortingCols = intval( $request['iSortingCols'] );
		for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
			if ( $request[ 'bSortable_'.intval($request['iSortCol_'.$i]) ] == 'true' ) {
				$search_builder->add_order($search_builder->columns[intval($request['iSortCol_'.$i])], ($request['sSortDir_'.$i]==='asc' ? 'asc' : 'desc'));
			}
		}
		
		//Filtering		
		if ( isset($request['sSearch']) && $request['sSearch'] != "" ) {
			$search_builder->add_filter('', "(", "", 'AND');
			$search_builder->all_column_filter( $security->clean_query( $request['sSearch'] ));
			$search_builder->add_filter('', ")", "", 'OR');
		}
		
		// Individual column filtering
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			if ( isset($request['bSearchable_'.$i]) && $request['bSearchable_'.$i] == 'true' && $request['sSearch_'.$i] != '' ) {
				$search_builder->add_filter($search_builder->columns[$i], "LIKE", '%'.$security->clean_query( $request['sSearch_'.$i] ).'%', 'AND');
			}
		}
		
		if(isset($request['letters'])){
			$search_builder->add_filter('cpm_contractors_letter_sent', "=", $request['letters'], 'AND');
		}
	
		$sql = "
		FROM cpm_contractors";
	
		$search_builder->add_sql($sql);
		
		$output = $search_builder->output($request);
		$output['contractors'] = $request['contractors'];
		$output['all_contractor_ids'] = $contractors;
		
		return $output;
	
	}

	function get_user_list ($request){
	
		$mysql = new mysql();
		$data = new data();
	
		$security = new security();
		$input = $request;
	
		$aColumns = array( 'cpm_contractors_user_ref', 'cpm_contractors_user_name', 'cpm_contractors_user_email'  );
		$iColumnCount = count($aColumns);
	
		// Indexed column (used for fast and accurate table cardinality)
		$sIndexColumn = 'cpm_contractors_user_ref';
	
		$sWhere = "";
	
		/**
		 * Paging
		 */
		$sLimit = "";
		if ( isset( $input['iDisplayStart'] ) && $input['iDisplayLength'] != '-1' ) {
			$sLimit = " LIMIT ".intval( $input['iDisplayStart'] ).", ".intval( $input['iDisplayLength'] );
		}
	
		/**
		 * Ordering
		 */
		$aOrderingRules = array();
		if ( isset( $input['iSortCol_0'] ) ) {
			$iSortingCols = intval( $input['iSortingCols'] );
			for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
				if ( $input[ 'bSortable_'.intval($input['iSortCol_'.$i]) ] == 'true' ) {
	
					$aOrderingRules[] = $aColumns[intval($input['iSortCol_'.$i])] . " ".($input['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
				}
			}
		}
	
		if (!empty($aOrderingRules)) {
			$sOrder = " ORDER BY ".implode(", ", $aOrderingRules);
		} else {
			$sOrder = " ORDER BY cpm_contractors_user_ref ASC";
		}
	
		/**
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$iColumnCount = count($aColumns);
	
		if ( isset($input['sSearch']) && $input['sSearch'] != "" ) {
			$aFilteringRules = array();
			for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
				if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' ) {
					$variable = $input['sSearch'];
						
					$aFilteringRules[] = "`".$aColumns[$i]."` LIKE '%".$security->clean_query( $variable )."%'";
				}
			}
			if (!empty($aFilteringRules)) {
				$aFilteringRules = array('('.implode(" OR ", $aFilteringRules).')');
			}
		}
	
		// Individual column filtering
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' && $input['sSearch_'.$i] != '' ) {
				$variable = $input['sSearch_'.$i];
	
				$aFilteringRules[] = "`".$aColumns[$i]."` LIKE '%".$security->clean_query( $variable )."%'";
			}
		}
	
		if (!empty($aFilteringRules)) {
			$sWhere = " AND ".implode(" AND ", $aFilteringRules);
		}
	
		$sql = "SELECT *
		FROM cpm_contractors_user
		WHERE cpm_contractors_user_qube_ref = '".$request['contractor_ref']."'
		".$sWhere.$sOrder;
	
		$result = $mysql->query($sql, 'Find Abandoned');
		$iFilteredTotal = $mysql->num_rows($result);
	
		$sql.=$sLimit;
		$result = $mysql->query($sql, 'Get Abandoned');
	
		$totalSQL = "SELECT *
		FROM cpm_contractors_user
		WHERE cpm_contractors_user_qube_ref = '".$request['contractor_ref']."'
		".$sOrder;
		$totalResult = $mysql->query($totalSQL, 'Get all residents');
		$iTotalRow = $mysql->fetch_array($totalResult);
		$iTotal = $iTotalRow[0];
	
		/**
		 * Output
		 */
		$output = array(
				"sEcho"                => intval($input['sEcho']),
				"iTotalRecords"        => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData"               => array()
		);
	
		while($aRow = $mysql->fetch_array($result)){
			$row = array();
			for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
				// General output
				$row[] = $aRow[ $aColumns[$i] ];
			}
	
			$row["DT_RowId"] =  $aRow[$sIndexColumn];
	
			$output['aaData'][] = $row;
	
		}
	
		$output['sql'] = $sql;
	
		return $output;
	
	}
	
	function get_users($contractor_ref=''){
		
		if($contractor_ref == ''){
			$contractor_ref = $this->contractor_qube_id;
		}
		
		$sql = "SELECT * 
		FROM cpm_contractors_user 
		WHERE cpm_contractors_user_qube_ref = '".$contractor_ref."'";
		
		$result = @mysql_query($sql);
		
		return $result;
	}
	
	function get_user($user_ref){
		$sql = "
		SELECT u.*, 
		IFNULL(
			(
				SELECT t.cpm_contractors_user_trail_login
				FROM cpm_contractors_user_trail t
				WHERE t.cpm_contractors_user_trail_user_ref = u.cpm_contractors_user_ref
				ORDER BY t.cpm_contractors_user_trail_id DESC
				LIMIT 1
			),
			''
		)as last_login
		FROM cpm_contractors_user u
		WHERE
		u.cpm_contractors_user_ref = '".$user_ref."'";
		
		$result = @mysql_query($sql);
		
		return $result;
	}
	
	function check_user($contractor_ref, $request){
		$field = new field;
		$err_message = "";
		$i = 0;
		$result_array = array();
	
		if($request['name'] == ""){
			$result_array[$i]['outcome'] = true;
			$result_array[$i]['message'] = "Please enter a name";
			$i++;
		}
		
		if($request['email'] == ""){
			$result_array[$i]['outcome'] = true;
			$result_array[$i]['message'] = "Please enter an email address";
			$i++;
		}else{
			if ($field->is_valid_email(trim($request['email'])) == false){
				$result_array[$i]['outcome'] = true;
				$result_array[$i]['message'] = "Email address is not a valid email address";
				$i++;
			}
		}
		
		$sql = "SELECT * 
		FROM cpm_contractors_user 
		WHERE cpm_contractors_user_qube_ref = '".$contractor_ref."'
		AND cpm_contractors_user_qube_ref = cpm_contractors_user_ref";
		$result = @mysql_query($sql);
		$num_rows = @mysql_num_rows($result);
		if($num_rows > 0){
			while($row = @mysql_fetch_array($result)){
				$parent_email = explode("@", $row['cpm_contractors_user_email']);
				$parent_domain = $parent_email[1];
			}
		}
		
		if(strpos($_REQUEST['email'], $parent_domain) === false){
			$result_array[$i]['outcome'] = true;
			$result_array[$i]['message'] = "Email address must have the same domain as the parent account: ".$parent_domain;
			$i++;
		}
		
		return $result_array;	
	}
	
	function save($contractor_ref, $request){
		Global $UTILS_HTTPS_ADDRESS, $UTILS_DB_ENCODE, $UTILS_TEL_MAIN;
		
		$crypt = new encryption_class;
		$data = new data;
		
		$sql = "SELECT * 
		FROM cpm_contractors_user 
		WHERE cpm_contractors_user_qube_ref = '".$contractor_ref."'
		ORDER BY cpm_contractors_user_ref DESC
		LIMIT 1";
		
		$result = @mysql_query($sql);
		$num_rows = @mysql_num_rows($result);
		$last_id = "";
		$new_id = "";
		$pass_error = 'Y';
	
		if($num_rows > 0){
			$result_array['success'] = 'Y';
			while($row = @mysql_fetch_array($result)){
				$last_id = $row['cpm_contractors_user_ref'];
			}
		}else{
			$result_array['success'] = 'N';	
		}
		
		if(strpos($last_id, "-") === false){
			$new_id = $contractor_ref . "-001";
		}else{
			$last_id_str = explode("-", $last_id);
			$new_id = $contractor_ref . "-" . str_pad(($last_id_str[1] + 1), 3, "0", STR_PAD_LEFT);
		}
		
		$security = new security();
		$password = $security->gen_serial(8, "AlphaOnly", true);
		
		if ($_REQUEST['user_id'] == ""){
			$sql_insert = "INSERT INTO cpm_contractors_user SET cpm_contractors_user_ref = '".$new_id."', 
			cpm_contractors_user_password = '".$crypt->encrypt($UTILS_DB_ENCODE, $password)."',  
			cpm_contractors_user_parent = '".$contractor_ref."', ";
		}else{
			$sql_insert = "UPDATE cpm_contractors_user SET ";
		}
		
		$sql_insert .= "cpm_contractors_user_qube_ref = '".$contractor_ref."', 
		cpm_contractors_user_name = '".$security->clean_query($request['name'])."',
		cpm_contractors_user_email = '".$request['email']."'";
		
		if ($request['user_id'] != ""){
			$disabled = "True";
			if ($request['disabled'] == ''){
				$disabled = "False";
				$sql_insert .= ", cpm_contractors_user_disabled_date = ''";
			}else{
				$datetime = new DateTime();
				$sql_insert .= ", cpm_contractors_user_disabled_date = '".$datetime->format('Ymd')."'";	
			}
			$sql_insert .= ", cpm_contractors_user_disabled = '".$disabled."' 
			WHERE cpm_contractors_user_ref = '".$request['user_id']."'";
		}
		
		@mysql_query($sql_insert) or $pass_error = $sql_insert;
		
		if ($request['user_id'] == ""){
		
			$mail_message = 'Dear '.$request['name'].',
			
		Welcome to the RMG Living Contractors Portal.
			
		Your username is: '.$new_id.'
		Your password is: '.$password.'
		
		To log in, go to '.$UTILS_HTTPS_ADDRESS.'
			
			
		Kind Regards,
		
		Technical Support Team
		Residential Management Group Ltd
		
		'.$UTILS_HTTPS_ADDRESS.'
		Tel. '.$UTILS_TEL_MAIN;
			
			$sql = "INSERT INTO cpm_mailer SET
			mail_to = '".$request['email']."',
			mail_from = 'support@rmgltd.co.uk',
			mail_subject = 'New RMG Living Login',
			mail_message = '".$mail_message."'";
			
			@mysql_query($sql);
		}
		
		$result_array['success'] = $pass_error;
		
		return $result_array;
	}
	
	function reset_password($request){
		Global $UTILS_HTTPS_ADDRESS, $UTILS_TEL_MAIN;	
		
		$password = $this->new_password($request);
	
		if ($request['user_id'] != "" && $password != false){
			
			$mail_message = 'Dear '.$request['name'].',
			
			Update from RMG Living Contractors Portal.
				
			Your password has been reset to: '.$password.'
			
			To log in, go to '.$UTILS_HTTPS_ADDRESS.'
				
				
			Kind Regards,
			
			Technical Support Team
			Residential Management Group Ltd
			
			'.$UTILS_HTTPS_ADDRESS.'
			Tel. '.$UTILS_TEL_MAIN;
				
			$sql = "INSERT INTO cpm_mailer SET
			mail_to = '".$request['email']."',
			mail_from = 'support@rmgltd.co.uk',
			mail_subject = 'New RMG Living Password',
			mail_message = '".$mail_message."'";
			
			@mysql_query($sql);
		
			$result_array['success'] = 'Y';	
		}else{
			$result_array['success'] = 'N';	
		}
		
		return $result_array;
	}
	
	function new_password($request){
		Global $UTILS_DB_ENCODE;
		
		$mysql = new mysql;
		$security = new security();
		$crypt = new encryption_class;
		
		$security = new security();
		$password = $security->gen_serial(8, "ALPHA", "UPPER");
	
		if ($request['user_id'] != ""){
		
			$sql_insert = "UPDATE cpm_contractors_user SET 
			cpm_contractors_user_password = '".$crypt->encrypt($UTILS_DB_ENCODE, $password)."' 
			WHERE cpm_contractors_user_ref = '".$request['user_id']."'";
			$mysql->query($sql_insert, 'New Password');
		
			return $password;
		}else{
			return false;
		}
	}
	
	function new_master_user(){
		
		$mysql = new mysql;
		$request = array();
		
		$request['success'] = 'N';
		
		if($this->contractor_qube_id != ''){
			$result = $this->get_users();
			$num_rows = $mysql->num_rows($result);
			if($num_rows == 0){
				$sql = "INSERT INTO cpm_contractors_user SET
				cpm_contractors_user_ref = '".$this->contractor_qube_id."',
				cpm_contractors_user_parent = '0',
				cpm_contractors_user_qube_ref = '".$this->contractor_qube_id."', 
				cpm_contractors_user_name = '".$this->contractor_name."',
				cpm_contractors_user_email = '".$this->contractor_email."'";
				
				$has_error = $mysql->insert($sql, 'Insert Master Account');
				
				if(!is_bool($has_error)){
					$sql = "UPDATE cpm_contractors SET
					cpm_contractors_letter_sent = 'Y'
					WHERE cpm_contractors_qube_id = '".$this->contractor_qube_id."'";
					
					$has_error = $mysql->insert($sql, 'Update Letter sent');
					
					if(!is_bool($has_error)){
						$request['user_id'] = $this->contractor_qube_id;
						$request['password'] = $this->new_password($request);
						$request['success'] = 'Y';
					}
				}
			}
		}
		
		return $request;
	}
	
	function save_contractor($contractor_ref, $request){
		
		$pass_error = "Y";
		
		$sql_insert = "UPDATE cpm_contractors SET ";
		
		$disabled = "True";
		
		if ($request['chk_disabled'] == ''){
			$disabled = "False";
			$sql_insert .= "cpm_contractors_disabled_date = '', ";
		}else{
			$datetime = new DateTime();
			$sql_insert .= "cpm_contractors_disabled_date = '".$datetime->format('Ymd')."', ";	
		}
		
		$sql_insert .= "cpm_contractors_disabled = '".$disabled."'
		WHERE cpm_contractors_qube_id = '".$contractor_ref."'";
		
		@mysql_query($sql_insert) or $pass_error = $sql_insert;
		
		$result_array['success'] = $pass_error;
		
		return $result_array;
	}

	//services starts
	function get_service_descs($id){


		$sql = " SELECT ( SELECT GROUP_CONCAT(cpm_c_outercodes_description)
 							FROM cpm_c_outercodes
 							WHERE  FIND_IN_SET( cpm_c_outercodes_id, cpm_contractors_service_outercode_ids )
 							ORDER BY cpm_c_outercodes_code
						)as descr
				FROM `cpm_contractors_service`
				WHERE cpm_contractors_service_id = ".$id;
		$result = @mysql_query($sql);
		while($row = @mysql_fetch_assoc($result)){
			return $row['descr'];
		}
	}

	//services starts
	function get_services($contractor_ref=''){

		if($contractor_ref == ''){
			$contractor_ref = $this->contractor_qube_id;
		}

		$sql = "SELECT *,( SELECT cpm_c_services_name FROM cpm_c_services WHERE cpm_c_services_id = cpm_contractors_service_service_id) as service_name,
				( SELECT GROUP_CONCAT(cpm_c_outercodes_id) FROM cpm_c_outercodes WHERE  FIND_IN_SET( cpm_c_outercodes_id, cpm_contractors_service_outercode_ids ) )as service_outercodes_ids
				FROM `cpm_contractors_service`
				WHERE cpm_contractors_service_contractor_qube_ref = '".$contractor_ref."'";

		$result = @mysql_query($sql);

		return $result;
	}

	function get_service($service_id){
		/*$sql = "
		SELECT *,( SELECT cpm_c_services_name FROM cpm_c_services WHERE cpm_c_services_id = cpm_contractors_service_service_id) as service_name,
						( SELECT GROUP_CONCAT(cpm_c_outercodes_code) FROM cpm_c_outercodes WHERE  FIND_IN_SET( cpm_c_outercodes_id, cpm_contractors_service_outercode_ids ) )as service_outercodes
				FROM `cpm_contractors_service`
				WHERE cpm_contractors_service_service_id =  '".$service_id."'";*/

		$sql = " SELECT *
				FROM `cpm_contractors_service`
				WHERE cpm_contractors_service_id =  '".$service_id."'";

		$result = @mysql_query($sql);

		return $result;
	}

	function save_service($request){

		$security = new security();
		$request['outercodes_id'] = $security->clean_query($request['outercodes_id']);
		$request['service_name_id'] = $security->clean_query($request['service_name_id']);
		if(strpos($request['outercodes_id'], ',') !== false ) {
			$outercodes = explode( ',', $request['outercodes_id'] );
			$outercodes_str = '';
			//echo '<pre>';print_r($outercodes);
			foreach($outercodes as $outercode ){
				$outercodes_str .= ','.$outercode;
			}
		}else{
			$outercodes_str = $request['outercodes_id'];
		}
		$outercodes_str = trim($outercodes_str,',');

		if ($request['service_id'] == ""){
			$sql_insert = "INSERT INTO cpm_contractors_service SET ";
		}else{
			$sql_insert = "UPDATE cpm_contractors_service SET ";
		}

		$sql_insert .= "cpm_contractors_service_contractor_qube_ref = '".$request['contractor_ref']."',
		cpm_contractors_service_service_id = '".$request['service_name_id']."',
		cpm_contractors_service_outercode_ids = '".$outercodes_str."'";

		if ($request['service_id'] != ""){
			$sql_insert .= " WHERE cpm_contractors_service_id = '".$request['service_id']."'";
		}

		if( @mysql_query($sql_insert) == true ) {
			$pass_error = 'Y';
		}else{
			$pass_error = $sql_insert;
		}

		$result_array['success'] = $pass_error;

		return $result_array;
	}

	function check_service( $request){
		$field = new field;
		$err_message = "";
		$i = 0;
		$result_array = array();

		if(empty($request['service_name_id'])){
			$result_array[$i]['outcome'] = true;
			$result_array[$i]['message'] = "Please select a service";
			$i++;
		}
		if(empty($request['outercodes_id']) || $request['outercodes_id'] == 'null'){
			$result_array[$i]['outcome'] = true;
			$result_array[$i]['message'] = "Please select outer code(s)";
			$i++;
		}

		$security = new security();

		if(empty($request['service_id'])) {
			$sql = "SELECT *
		FROM cpm_contractors_service
		WHERE cpm_contractors_service_contractor_qube_ref = '" . $request['contractor_ref'] . "'
		AND cpm_contractors_service_service_id = " . $security->clean_query( $request['service_name_id'] );

			$result = @mysql_query( $sql );
			$num_rows = @mysql_num_rows( $result );
			if ( $num_rows > 0 ) {
				$result_array[$i]['outcome'] = true;
				$result_array[$i]['message'] = "Service already exists.Please update it";
				$i++;
			}
		}

		return $result_array;
	}

	function get_service_names(){
		$sql = "SELECT * FROM cpm_c_services";
		$result = @mysql_query($sql);
		$servicesArr = array();
		while($row = @mysql_fetch_array($result)){
			$servicesArr[$row['cpm_c_services_id']] = $row['cpm_c_services_name'];
		}
		return $servicesArr;
	}

	function get_outer_codes(){
		$sql = "SELECT * FROM cpm_c_outercodes";
		$result = @mysql_query($sql);
		$servicesArr = array();
		while($row = @mysql_fetch_array($result)){
			$servicesArr[$row['cpm_c_outercodes_id']] = $row['cpm_c_outercodes_description'] .' ( '.$row['cpm_c_outercodes_code'].' ) ';
		}
		return $servicesArr;
	}

	function update_outercodes($request){

		$mysql = new mysql;

		if ($request['contractor_ref'] != ""){

			$sql_insert = "UPDATE cpm_contractors SET
			cpm_contractor_default_outercodes = '".$request['contractor_outercodes']."'
			WHERE cpm_contractors_qube_id = '".$request['contractor_ref']."'";

			$mysql->query($sql_insert, 'Update Default Outercodes');
			$result_array['success'] = 'Y';
		}else{
			$result_array['success'] = 'N';
		}
		return $result_array;
	}

	function get_outercodes_array(){
			$sql = "SELECT * FROM cpm_c_outercodes";
			$result = @mysql_query($sql);
			$outercodesArr = array();
			while($row = @mysql_fetch_array($result)){
				$outercodesArr[$row['cpm_c_outercodes_id']]['desc'] = $row['cpm_c_outercodes_description'];
				$outercodesArr[$row['cpm_c_outercodes_id']]['code'] = $row['cpm_c_outercodes_code'];
			}
		return $outercodesArr;
	}

}

?>