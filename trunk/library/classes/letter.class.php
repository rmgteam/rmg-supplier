<?php
require_once($UTILS_CLASS_PATH."pdf/pdf.class.php");
require_once($UTILS_CLASS_PATH."pdf/tfpdf/class.tfpdftable.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");

class letter extends pdf  {

	function letter($orientation='P',$unit='mm',$size='A4'){
		$this->tFPDF($orientation,$unit,$size);
		$this->pdf();
		$this->SetMargins(20,43,20);
	}
	
	function add_letter_address($address_array){
		
		for($a=0;$a < count($address_array);$a++){
			if($address_array[$a] != ''){
				$this->Write($this->line_height, $address_array[$a]);
				$this->Ln();
			}
		}
	}
	
	function add_letter_date($date="", $format="d/m/Y"){
		
		if($date == ""){
			$date = date($format);
		}
		$this->Cell(0,$this->line_height,$date,0,1,'R');
	}
	
	function add_salutation($name="", $sal="Dear"){
		
		$sal = $sal." ".$name.",";
		$this->Cell(0,$this->line_height,$sal,0,1,'L');
		$this->Ln();
	}
	
	function add_ref($str=""){
		
		$str = "Our Ref: ".$str;
		$this->Cell(0, $this->line_height, $str, 0, 1, 'L');
		$this->Ln();
		$this->Ln();
	}
	
	function add_regarding($str=""){
		
		$str = "Re: ".$str;
		$this->SetFont('','BU');
		//$this->Cell(0, $this->line_height, $str, 0, 1, 'L');
		$this->MultiCell(0, $this->line_height, $str, 0, 'L', 0);
		$this->SetFont('','');
		$this->Ln();
	}
	
	function add_closing($object="", $str="Yours sincerely,", $type="rmc"){
		
		$this->Cell(0,$this->line_height,$str,0,1,'L');
		
		if(is_object($object)){
			
			if($type=="rmc"){
				$this->Cell(0,$this->line_height, "For and on behalf of ".stripslashes($object->rmc_name),0,1,'L'); 
			}
			elseif($type=="owner"){
				$this->Cell(0,$this->line_height, "For and on behalf of ".stripslashes($object->owner_name),0,1,'L');	
			}
			$this->Ln();
		}
		elseif(is_string($object) && $object != ""){
			$this->Cell(0,$this->line_height, "For and on behalf of ".stripslashes($object),0,1,'L');
			$this->Ln(); 
		}
		else{
			$this->Ln();
		}
	}
	
	function add_pp($str=""){
		
		$str = "pp ".$str;
		$this->Cell(0,$this->line_height,$str,0,1,'L');
	}

}
?>