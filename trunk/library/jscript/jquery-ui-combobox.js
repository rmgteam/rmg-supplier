(function( $ ) {
	$.widget( "ui.combobox", {
		options: {
			max_width: 0
		},
		_create: function() {
			var self = this;
			var options = this.options;
			var select = this.element.hide(),
				selected = select.children( ":selected" ),
				value = selected.val() ? selected.text() : "",
				wrapper = this.wrapper = $( "<span>" )
					.addClass( "ui-combobox" )
					.insertAfter( select );
				
			select.children("option" ).each(function() {
				var a = $( document.createElement('span') );
				a.html(this.text);
				a.attr('id', 'comboremovable');
				$('body').append(a);
				temp_width = $('#comboremovable').width() + 15;
				$('#comboremovable').remove();
				if(temp_width > options.max_width){
					options.max_width = temp_width;
				}
			});
				
			var input = $( "<input />" )
				.appendTo(wrapper)
				.val( value )
				.attr( "id", self.element.attr('id') + "_input" )
				.addClass( "ui-state-default ui-combobox-input" )
				.width(options.max_width)
				.click(function() {
					// close if already visible
					if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
						input.autocomplete( "close" );
						return;
					}
					// work around a bug (likely same cause as #5265)
					$( this ).blur();
					// pass empty string as value to search for, displaying all results
					input.autocomplete( "search", "" );
				})
				.autocomplete({
					delay: 0,
					minLength: 0,
					source: function(request, response) {
						var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
						response( select.children("option" ).map(function() {
							var text = $( this ).text();
							if (( !request.term || matcher.test(text) ) )
								return {
									label: text.replace(
										new RegExp(
											"(?![^&;]+;)(?!<[^<>]*)(" + 
											$.ui.autocomplete.escapeRegex(request.term) +
											")(?![^<>]*>)(?![^&;]+;)", "gi"),
											"<strong>$1</strong>"),
									value: text,
									option: this
								};
							}) 
						);
					},
					
					select: function( event, ui ) {
						ui.item.option.selected = true;
						self._trigger( "selected", event, {
							item: ui.item.option
						});
						//Move cursor out of input to stop editing
						setTimeout(function(){
							input.blur();
						},100);
						//trigger select change event
						$('#' + self.element.attr('id')).trigger("change");
					},
					
					change: function(event, ui) {
						if ( !ui.item ) {
							var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
								valid = false;
							select.children( "option" ).each(function() {
								if ( this.value.match( matcher ) ) {
									this.selected = valid = true;
									return false;
								}
							});
							if ( !valid ) {
								// remove invalid value, as it didn't match anything
								$( this ).val( "" );
								select.val( "" );
								return false;
							}
						}
					}
			   })
			   .removeClass( "ui-corner-all" )
			   .addClass("ui-widget ui-widget-content ui-corner-left");
			
			input.data( "autocomplete" )._renderItem = function( ul, item ) {
				return $( "<li></li>" )
					.data( "item.autocomplete", item )
					.append( "<a>" + item.label + "</a>" )
					.appendTo( ul );
			};
			var toggle_anchor = $( "<a>" )
				.attr( "tabIndex", -1 )
				.attr( "title", "Show All Items" )
				.attr( "id", self.element.attr('id') + "_toggle" )
				.appendTo(wrapper)
				.button({
					icons: {
						primary: "ui-icon-triangle-1-s"
					},
					text: false
				})
				.removeClass( "ui-corner-all" )
				.addClass( "ui-corner-right ui-combobox-toggle" )
				.click(function() {
					// close if already visible
					if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
						input.autocomplete( "close" );
						return;
					}
					// work around a bug (likely same cause as #5265)
					$( this ).blur();
					// pass empty string as value to search for, displaying all results
					input.autocomplete( "search", "" );
				});
		},
		
		destroy: function() {
			this.wrapper.remove();
			this.element.show();
			$.Widget.prototype.destroy.call( this );
		},
		
		update: function() {
			var select = this.element;
			var options = this.options;
			
			select.children("option").each(function() {
				var a = $( document.createElement('span') );
				a.html(this.text);
				a.attr('id', 'comboremovable');
				$('body').append(a);
				temp_width = $('#comboremovable').width() + 15;
				$('#comboremovable').remove();
				if(temp_width > options.max_width){
					options.max_width = temp_width;
				}
			});
			
			$('#' + this.element.attr('id') + "_input" ).width(options.max_width);
			this.element.val($("#" + this.element.attr('id') + " option:first").val());
			$('#' + this.element.attr('id') + "_input" ).val($("#" + this.element.attr('id') + " option:selected").text());
		}
	});
})( jQuery );