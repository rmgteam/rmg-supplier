/**
 * Created by Marc.Evans on 26/11/2014.
 */

jQuery.event.props.push( "dataTransfer" );

(function( $, undefined ) {

	$.widget( "ui.multiFileUpload", {

		options: {
			element: $(),
			language:{
				navigation: {
					up: '<i class="fa fa-caret-up"></i>',						/// text for up button
					down: '<i class="fa fa-caret-down"></i>',					/// text for down button
					slideDown: '<i class="fa fa-angle-double-down"></i>',		/// text for slide down button
					slideUp: '<i class="fa fa-angle-double-up"></i>'			/// text for slide up button
				},
				closeText: '<i class="fa fa-remove"></i>',						/// text for close button
				addButtonText: 'Add Files',										/// text for add button
				clearButtonText: 'Remove All Files',							/// text for clear button
				removeButtonText: 'Remove Selected File',						/// text for remove button
				uploadButtonText: 'Upload All Files',							/// text for upload button
				fileUploadBoxText: 'Drag & Drop files or click to browse'		/// text for main upload box
			},
			classes: {
				main: 'multi-file-upload',										/// class of pop up
				header: 'multi-file-upload-heading',							/// class for header of pop up
				footer: 'multi-file-upload-footer',								/// class for footer
				surround: 'multi-file-upload-content',							/// class for surround
				fade: 'multi-file-upload-back-drop',							/// class for background
				navigation: 'multi-file-upload-navigation',						/// class for navigation bar
				ulSurround: 'multi-file-upload-ul-surround',					/// class for ul surround
				content: 'multi-file-upload-section',							/// class for content
				group: 'multi-file-upload-group',								/// class for group
				slide: 'multi-file-upload-slide',								/// class for file surround
				fileSurround: 'multi-file-upload-file',							/// class for file surround
				fieldSurround: 'multi-file-upload-fields',						/// class for field surround
				fieldColumns: 'multi-file-upload-column',						/// class for field column
				fieldSurroundButton: 'btn btn-default',							/// class for field surround button
				hiddenFile: 'multi-file-upload-hidden-file',					/// class for hidden file input
				progressSurround: 'multi-file-upload-progress',					/// class for progress surround
				progress: 'progress',											/// class for progress bar surround
				progressBar: 'progress-bar progress-bar-info progress-bar-striped',/// class for progress bar
				navigationButton: 'multi-file-upload-navigation-button',		/// class for navigation buttons
				buttonSurround: 'multi-file-upload-button-surround',			/// class for button container
				buttonContainerSm: 'multi-file-upload-button-sm',				/// class for small button container
				addButton: 'btn btn-primary',									/// class for add button
				removeButton: 'btn btn-danger',									/// class for clear button
				clearButton: 'btn btn-danger',									/// class for clear button
				mainButton: 'btn btn-success',									/// class for upload button
				fileIcon: 'fa fa-file',											/// file icon
				thumb: 'multi-file-upload-thumb'								/// class for non image thumbnail
			},
			elements: [
				{
					label: 'Title',												/// element label
					type: 'input',												/// element type
					id: 'title'													/// element id
				},
				{
					label: 'Description',										/// element label
					type: 'textarea',											/// element type
					id: 'description'											/// element id
				}
			],
			ajax: {
				'url': '',														/// upload url
				'data': {}														/// post data
			},
			completeCallback: function(){},
			mode: 'inline',														/// mode inline/popup
			transition: 'fade',													/// transition
			prefix: '',															/// id prefix
			noFiles: 0,															/// Number of Files
			fileArray: [],														/// File List
			accept: '',															/// File types
			capture: '',														/// camera/microphone/camcorder
			isMoving: false,													/// is the menu scrolling?
			menuSpeed: 500,														/// speed menu scrolls at
			transitionSpeed: 1500												/// time taken for files to swap
		},

		/// Initialise
		_create: function () {
			var self = this;
			var options = this.options;

			options.element = self.element;

			var windowWidth = screen.width;

			var isMobile = false;

			if( windowWidth < 768 ){
				isMobile = true;
			}

			if(options.element.attr('id') && options.prefix == ''){
				options.prefix = options.element.attr('id');
			}else if(!options.element.attr('id') && options.prefix == ''){
				options.prefix = 'multiImage';
			}

			var mainSurround = options.element;

			if(options.mode == 'popup') {

				options.element.on('click', {'self': self}, function(e){
					var self = e.data['self'];
					self.open();
				});

				/// Create Pop up
				var popup = $(document.createElement('div'))
					.addClass(options.classes.main)
					.attr('id', options.prefix + 'Popup');

				var header = $(document.createElement('div'))
					.addClass(options.classes.header);

				var button = $(document.createElement('button'))
					.attr({
						'type': 'button'
					})
					.on('click', {'self': self}, function(e){
						var self = e.data['self'];
						self.close();
					})
					.addClass("close");

				var span = $(document.createElement('span'))
					.html('<i class="fa fa-remove"></i>')
					.appendTo(button);

				button.appendTo(header);
				header.appendTo(popup);
				popup.appendTo($('body'));

				/// Create backdrop
				var backdrop = $(document.createElement('div'))
					.attr('id', options.prefix + 'BackDrop')
					.addClass(options.classes.fade)
					.appendTo($('body'));

				mainSurround = $('#' + options.prefix + 'Popup');
			}

			/// Create content
			var surround = $(document.createElement('div'))
				.attr('id', options.prefix + 'Surround')
				.addClass(options.classes.surround);

			/// Create Navigation bar
			var naviBar = $(document.createElement('div'))
				.addClass(options.classes.navigation)
				.attr('id', options.prefix + 'Navigation');

			/// Create up button
			var upButton = $(document.createElement('button'))
				.addClass(options.classes.navigationButton)
				.html(options.language.navigation.up)
				.attr('id', options.prefix + 'NavigationUp')
				.on('click', {'self':self}, function(e){
					e.preventDefault();
					var self = e.data['self'];
					self.menuScroll('up');
				})
				.appendTo(naviBar);

			var navSurround = $(document.createElement('div'))
				.addClass(options.classes.ulSurround)
				.attr('id', options.prefix + 'NavigationSurround');

			var ul = $(document.createElement('ul'))
				.attr('id', options.prefix + 'NavigationFiles')
				.appendTo(navSurround);

			navSurround.appendTo(naviBar);

			/// Create down button
			var downButton = $(document.createElement('button'))
				.addClass(options.classes.navigationButton)
				.html(options.language.navigation.down)
				.attr('id', options.prefix + 'NavigationDown')
				.on('click', {'self':self}, function(e){
					e.preventDefault();
					var self = e.data['self'];
					self.menuScroll('down');
				})
				.appendTo(naviBar);

			naviBar.appendTo(surround);

			/// Create right hand pane
			var rightPane = $(document.createElement('div'))
				.addClass(options.classes.content)
				.attr('id', options.prefix + 'Content');

			var group = $(document.createElement('div'))
				.addClass(options.classes.group)
				.attr('id', options.prefix + 'Group')
				.appendTo(rightPane);

			rightPane.appendTo(surround);
			surround.appendTo(mainSurround);

			/// Footer
			var footer = $(document.createElement('div'))
				.addClass(options.classes.footer);

			/// Create Add button container
			var buttonContainer = $(document.createElement('div'))
				.addClass(options.classes.buttonSurround)
				.attr('id', options.prefix + 'ButtonContainer')

			/// Create Add button container
			var buttonPane = $(document.createElement('div'))
				.addClass(options.classes.buttonContainerSm);

			/// Add Button
			var addButton = $(document.createElement('button'))
				.addClass(options.classes.addButton)
				.html(options.language.addButtonText)
				.attr('id', options.prefix + 'AddButton')
				.on('click', {'self':self}, function(e){
					e.preventDefault();
					var self = e.data['self'];
					self.goTo(1);
				})
				.appendTo(buttonPane);

			buttonPane.appendTo(buttonContainer);

			/// Create Add button container
			var buttonPane = $(document.createElement('div'))
				.addClass(options.classes.buttonContainerSm);

			/// Add Button
			var removeButton = $(document.createElement('button'))
				.addClass(options.classes.removeButton)
				.html(options.language.removeButtonText)
				.hide()
				.attr('id', options.prefix + 'RemoveButton')
				.on('click', {'self':self}, function(e){
					e.preventDefault();
					var self = e.data['self'];
					self.removeFile();
				})
				.appendTo(buttonPane);

			buttonPane.appendTo(buttonContainer);

			/// Create Add button container
			var buttonPane = $(document.createElement('div'))
				.addClass(options.classes.buttonContainerSm);

			/// Clear Button
			var clearButton = $(document.createElement('button'))
				.addClass(options.classes.clearButton)
				.html(options.language.clearButtonText)
				.attr({
					'id': options.prefix + 'ClearButton',
					'disabled': true
				})
				.on('click', {'self':self}, function(e){
					e.preventDefault();
					var self = e.data['self'];
					self.clear();
				})
				.appendTo(buttonPane);

			buttonPane.appendTo(buttonContainer);

			/// Create Add button container
			var buttonPane = $(document.createElement('div'))
				.addClass(options.classes.buttonContainerSm);

			/// Upload Button
			var uploadButton = $(document.createElement('button'))
				.addClass(options.classes.mainButton)
				.html(options.language.uploadButtonText)
				.attr({
					'id': options.prefix + 'UploadButton',
					'disabled': true
				})
				.on('click', {'self':self}, function(e){
					e.preventDefault();
					var self = e.data['self'];
					self.upload();
				})
				.appendTo(buttonPane);

			buttonPane.appendTo(buttonContainer);
			buttonContainer.appendTo(footer);

			/// Progress Bar
			var progressSurround = $(document.createElement('div'))
				.addClass(options.classes.progressSurround)
				.attr('id', options.prefix + 'ProgressSurround');

			/// Create progress bar
			var progress = $(document.createElement('div'))
				.addClass(options.classes.progress)
				.attr('id', options.prefix + 'Progress')
				.hide();

			var progressBar = $(document.createElement('div'))
				.addClass(options.classes.progressBar)
				.attr({
					'id': options.prefix + 'ProgressBar'
				})
				.appendTo(progress);

			progress.appendTo(progressSurround);
			progressSurround.appendTo(footer);

			footer.appendTo(mainSurround);

			var form = $(document.createElement('form'))
				.attr({
					'id': options.prefix + 'Form',
					'enctype': 'multipart/form-data'
				});

			mainSurround.wrapInner(form);

			var slide = $(document.createElement('div'))
				.addClass(options.classes.slide)
				.attr({
					'id': options.prefix + 'Files',
					'data-slide-no' : 1
				});

			/// Stop drags from redirecting when missing the box
			$(window).on('drop dragover', function(e){
				e.stopPropagation();
				e.preventDefault();
			});

			/// Create File Surround
			var fileSurround = $(document.createElement('div'))
				.addClass(options.classes.fileSurround)
				.text(options.language.fileUploadBoxText)
				.on('dragover', {'self': self}, function(e){
					var self = e.data['self'];
					e.stopPropagation();
					e.preventDefault();
					e.originalEvent.dataTransfer.dropEffect = 'copy';
				})
				.on('drop', {'self': self}, function(e){
					var self = e.data['self'];
					e.stopPropagation();
					e.preventDefault();
					self.selectFile(e);
				})
				.on('click', {'self': self}, function(e){
					var self = e.data['self'];
					var options = self.options;
					console.log("GHJ");
					$('#multiFileUploadFile').click();
					e.stopPropagation();
					e.preventDefault();
				});

			/// Create hidden file input
			var fileUpload = $(document.createElement('input'))
				.addClass(options.classes.hiddenFile)
				.attr({
					'id': 'multiFileUploadFile',
					'type': 'file',
					'multiple': true
				})
				.on('change', {'self': self}, function(e){
					var self = e.data['self'];
					/*e.stopPropagation();
					e.preventDefault();*/
					self.selectFile(e);
				});

			/// Add accept condition, if exists
			if(options.accept != ''){
				fileUpload.attr('accept', options.accept);
			}

			/// Add capture condition, if exists
			if(options.capture != ''){
				fileUpload.attr('capture', options.capture);
			}

			fileSurround.appendTo(slide);
			fileUpload.appendTo(slide);
			slide.appendTo($('#' + options.prefix + 'Group'));

			$(window).on('resize', {'self': self}, function(e) {
				var self = e.data['self'];
				self.recalculate();
			});

			/// After show event
			self.element.parents().on('afterShow', {'self': self}, function (e) {
				var self = e.data['self'];
				self.recalculate();
			});

			/// After show event
			self.element.parent().on('afterToggleClass', {'self': self}, function (e) {
				var self = e.data['self'];
				self.recalculate();
			});

			self.goTo(1);

			self._trigger('_create');
		},

		/// Resize conSlider
		recalculate: function(){
			var self = this;
			var options = this.options;
			options.slides = $('#' + options.prefix + 'Group').children();
			var wrapperWidth = options.slides.length * 3;

			var windowWidth = screen.width;
			var windowHeight = screen.height;
			var sliderWidth = options.width;

			$('#' + options.prefix + 'Content').removeAttr('style');
			$('#' + options.prefix + 'Group').attr('style', '');

			var tallestSlide = 0;
			var slideWidth = $('#' + options.prefix + 'Surround').innerWidth() - $('#' + options.prefix + 'Navigation').outerWidth(true);

			options.slides.each(function(){
				$(this).attr('style', '');
			});

			if( options.transition == 'slide' ){

				options.slides.each(function(){
					$(this).css({
						'height': '100%'/*,
						 'width': slideWidth + 'px'*/
					});
				});

				/// Get width and height
				options.slides.each(function(){
					wrapperWidth += $(this).outerWidth(true);

					var slideHeight = $(this).outerHeight(true);

					if(tallestSlide < slideHeight){
						tallestSlide = slideHeight;
					}
				});

				$('#' + options.prefix + 'Group').css({
					/*'width': wrapperWidth + 'px',*/
					'height': tallestSlide + 'px'
				});
			}else{

				/// Get width and height
				options.slides.each(function(){
					var slideHeight = $(this).outerHeight(true);

					if(tallestSlide < slideHeight){
						tallestSlide = slideHeight;
					}
				});

				var GroupHeight = $('#' + options.prefix + 'Surround').outerHeight(true);

				$('#' + options.prefix + 'Group').css({
					'height': tallestSlide + 'px',
					'width': slideWidth + 'px'
				});

				options.slides.each(function() {
					$(this).css({
						'height': tallestSlide + 'px',
						'width': slideWidth + 'px'
					});
				});

			}

			$('#' + options.prefix + 'NavigationUp').hide();
			$('#' + options.prefix + 'NavigationDown').hide();

			$('#' + options.prefix + 'NavigationSurround').removeAttr('style');

			if($('#' + options.prefix + 'NavigationFiles').outerHeight(true) > $('#' + options.prefix + 'Group').outerHeight(true)){
				var naviHeight = 0;
				var fullHeight = $('#' + options.prefix + 'Group').outerHeight(true);

				$('#' + options.prefix + 'NavigationFiles').find('li').each(function(){
					if(naviHeight <= fullHeight){
						naviHeight += $(this).height();
					}
				});

				$('#' + options.prefix + 'NavigationSurround').css('height', naviHeight + 'px');
			}

			if($('#' + options.prefix + 'NavigationFiles').outerHeight(true) > $('#' + options.prefix + 'Group').outerHeight(true) && $('#' + options.prefix + 'NavigationFiles').find('li').length > 1){
				$('#' + options.prefix + 'NavigationUp').show();
				$('#' + options.prefix + 'NavigationDown').show();
			}

			/// Check current slide for tabs, add resize on change even
			$(options.slides[options.current - 1]).children().each(function(){
				//if we have tabs iterate through to find the highest one
				if($(this).hasClass('nav-tabs') && options.transition == 'slide'){
					$(this).on('change', {'self':self} , function (e) {
						var self = e.data['self'];
						self.recalculate();
					});
				}
			});

			self._trigger('recalculate');
		},

		viewFields: function(index){
			var self = this;
			var options = this.options;

			if($('#' + options.prefix + 'FieldSurround-' + index).hasClass('active')){
				$('#' + options.prefix + 'FieldSurround-' + index).removeClass('active');
				$('#' + options.prefix + 'FieldSurround-' + index).find('button').text(options.language.slideUp);
			}else{
				$('#' + options.prefix + 'FieldSurround-' + index).addClass('active');
				$('#' + options.prefix + 'FieldSurround-' + index).find('button').text(options.language.slideDown);
			}

			self._trigger('viewFields');

		},

		open: function() {
			var self = this;
			var options = this.options;

			$('body').css('overflow', 'hidden');

			$('#' + options.prefix + 'BackDrop').show();
			$('#' + options.prefix + 'Progress').hide();
			$('#' + options.prefix + 'ButtonContainer').show();
			$('#' + options.prefix + 'Popup').show();

			self.recalculate();

			self._trigger('open');
		},

		close: function(index) {
			var self = this;
			var options = this.options;

			self.clear();

			$('#' + options.prefix + 'Progress').hide();
			$('#' + options.prefix + 'Popup').hide();

			var noOpenModals = $( 'body' ).find( '.' + options.classes.main + ':visible' ).length;
			if( noOpenModals == 0 ) {
				$('body').removeAttr('style');
				$('#' + options.prefix + 'BackDrop').hide();
			}

			self._trigger('close');
		},

		goTo: function(index) {
			var self = this;
			var options = this.options;

			if(index == 1){
				$('#' + options.prefix + 'RemoveButton').hide();
			}else{
				$('#' + options.prefix + 'RemoveButton').removeAttr('style');
			}

			$('#' + options.prefix + 'NavigationFiles').find('li').removeClass('active');
			$('#' + options.prefix + 'Thumb-' + index).addClass('active');

			var seconds = options.transitionSpeed / 1000;

			$('#' + options.prefix + 'Group').css({
				'-webkit-transition': 'opacity ' +seconds + 's ease-in-out',
				'-moz-transition': 'opacity ' +seconds + 's ease-in-out',
				'-o-transition': 'opacity ' +seconds + 's ease-in-out',
				'transition': 'opacity ' +seconds + 's ease-in-out'
			});

			$('#' + options.prefix + 'Group').children().each(function() {
				$(this).css({
					'-webkit-transition': 'opacity ' +seconds + 's ease-in-out',
					'-moz-transition': 'opacity ' +seconds + 's ease-in-out',
					'-o-transition': 'opacity ' +seconds + 's ease-in-out',
					'transition': 'opacity ' +seconds + 's ease-in-out'
				});
			});

			var width = $('#' + options.prefix + 'Group').children().innerWidth();

			if(options.transition == 'slide'){
				/// if transition is slide
				var amount = (index -1) * (-width);
				$('#' + options.prefix + 'Group').css("transform","translateX("+amount+"px)");

			}else if(options.transition == 'fade'){
				/// if transition is fade
				$('#' + options.prefix + 'Group').children().removeClass('active');
				$('#' + options.prefix + 'Group').find("[data-slide-no='" + index + "']").addClass('active');
			}

			self._trigger('open');
		},

		menuScroll: function (direction){
			var self = this;
			var options = this.options;

			/// Get list
			var ul = $('#' + options.prefix + 'NavigationFiles');

			/// If not already moving
			if(!options.isMoving){
				/// Set that we are moving
				options.isMoving = true;

				if(direction == 'down'){

					var element = ul.children('li:first');

					var menuItemHeight = element.outerHeight(true);

					element
						.animate({'marginTop': '-' + menuItemHeight + 'px'}, options.menuSpeed, function(){
							element
								.detach()
								.css('marginTop', '0')
								.appendTo(ul);

							options.isMoving	= false;
						}
						.bind(this));

				}else if(direction == 'up'){

					var element = ul.children('li:last');

					var menuItemHeight = element.outerHeight(true);

					element
						.detach()
						.prependTo(ul)
						.css('marginTop', '-' + menuItemHeight + 'px')
						.animate({'marginTop': '0px'}, options.menuSpeed, function(){
							options.isMoving = false;
						}
						.bind(this));
				}
			}

			self._trigger('menuScroll');
		},

		removeFile: function(){
			var self = this;
			var options = this.options;

			var i = 0;
			$('#' + options.prefix + 'Group').children().each(function() {
				if ($(this).attr('id') != options.prefix + 'Files') {
					if($(this).hasClass('active')){
						$(this).remove();
						return;
					}
					i ++;
				}
			});

			options.fileArray.splice(i, 1);

			$('#' + options.prefix + 'NavigationFiles').find('li.active').remove();

			self.recalculate();

			self.goTo(1);

			self._trigger('removeFile');
		},

		clear: function(){
			var self = this;
			var options = this.options;

			options.fileArray = [];

			$('#' + options.prefix + 'Group').children().each(function(){
				if($(this).attr('id') != options.prefix + 'Files'){
					$(this).remove();
				}
			});

			$('#' + options.prefix + 'NavigationFiles').find('li').remove();

			$('#' + options.prefix + 'UploadButton').attr('disabled', 'disabled');
			$('#' + options.prefix + 'ClearButton').attr('disabled', 'disabled');

			self.recalculate();

			self.goTo(1);

			self._trigger('clear');
		},

		selectFile: function (evt) {
			var self = this;
			var options = this.options;

			$('#' + options.prefix + 'UploadButton').removeAttr('disabled');
			$('#' + options.prefix + 'ClearButton').removeAttr('disabled');

			if(evt.type == 'drop'){
				var files = evt.dataTransfer.files; /// FileList object
			}else {
				var files = evt.target.files; /// FileList object
			}

			var filesArr = Array.prototype.slice.call(files);

			/// Loop through the FileList and render image files as thumbnails.
			filesArr.forEach(function(f) {

				/// Only process accepted files, if set
				if(options.accept != '') {
					if (!f.type.match(options.accept)) {
						return;
					}
				}

				options.fileArray.push(f);

				options.noFiles ++;

				var noFiles = options.noFiles + 1;

				/// Create Slide
				var slide = $(document.createElement('div'))
					.addClass(options.classes.slide)
					.attr({
						'id': options.prefix + 'Files-' + noFiles,
						'data-slide-no': noFiles
					});

				/// Create File Surround
				var fileSurround = $(document.createElement('div'))
					.addClass(options.classes.fileSurround)
					.attr({
						'id': options.prefix + 'File-' + noFiles
					});

				fileSurround.appendTo(slide);
				slide.appendTo($('#' + options.prefix + 'Group'));

				/// Create Thumb holder
				var li = $(document.createElement('li'))
					.attr('id', options.prefix + 'Thumb-' + noFiles)
					.appendTo($('#' + options.prefix + 'NavigationFiles'));

				if (f.type.match('image/*')) {
					/// Create Thumbnail if file is image
					var reader = new FileReader();

					/// Closure to capture the file information.
					$(reader).on('load', {'self':self, 'noFiles': noFiles}, function(e){

						var self = e.data['self'];
						var noFiles = e.data['noFiles'];
						var theFile = e.target.result;

						/// Render thumbnail.
						var img = $(document.createElement('img'))
							.attr({
								src : e.target.result,
								title : theFile.name
							})
							.on('click', {'self':self, 'noFiles': noFiles}, function(e){
								var self = e.data['self'];
								var noFiles = e.data['noFiles'];
								self.goTo(noFiles);
							})
							.appendTo($('#' + options.prefix + 'Thumb-' + noFiles));

					});

					/// Read in the image file as a data URL.
					reader.readAsDataURL(f);
				}else{
					/// Create Icon
					var thumb = $(document.createElement('div'))
						.addClass(options.classes.thumb)
						.on('click', {'self':self, 'noFiles': noFiles}, function(e){
							var self = e.data['self'];
							var noFiles = e.data['noFiles'];
							self.goTo(noFiles);
						})
						.attr({
							title : f.name
						});

					var icon = $(document.createElement('i'))
						.addClass(options.classes.fileIcon)
						.appendTo(thumb);

					var span = $(document.createElement('span'))
						.text( f.name )
						.appendTo(thumb);

					thumb.appendTo($('#' + options.prefix + 'Thumb-' + noFiles));
				}

				/// Add elements if configured
				if(options.elements.length > 0) {
					var fieldSurround = $(document.createElement('div'))
						.addClass(options.classes.fieldSurround)
						.attr({
							'id': options.prefix + 'FieldSurround-' + noFiles,
							'data-file-no': noFiles
						});

					var fieldsSurround = $(document.createElement('div'));

					$.each(options.elements, function(){
						var data = this;

						/// Add surround
						var columnsSurround = $(document.createElement('div'))
							.addClass(options.classes.fieldColumns);

						/// Add label
						var label = $(document.createElement('label'))
							.attr('for', data['id'] + '-' + noFiles)
							.text(data['label'])
							.appendTo(columnsSurround);

						/// Add element
						var element = $(document.createElement(data['type']))
							.attr('id', data['id'] + '-' + noFiles)
							.appendTo(columnsSurround);

						columnsSurround.appendTo(fieldsSurround)
					});

					fieldsSurround.appendTo(fieldSurround);
					fieldSurround.appendTo($('#' + options.prefix + 'File-' + noFiles));
				}
			});

			setTimeout(function() {
				self.recalculate();
			}, 500);

			self._trigger('selectFile');
		},

		upload: function (){
			var self = this;
			var options = this.options;

			$('#' + options.prefix + 'Progress').show();
			$('#' + options.prefix + 'ButtonContainer').hide();

			if(self.supportFormData()){
				var formData = new FormData();

				for(var i=0, len=options.fileArray.length; i<len; i++) {
					formData.append('files[]', options.fileArray[i]);
				}

				if(options.elements.length > 0) {
					for(var i = 1; i <= options.noFiles; i ++) {
						$.each(options.elements, function () {
							var data = this;
							if($('#' + data['id']+ '-' + (i + 1)).length > 0) {
								formData.append(data['id'] + '[]', $('#' + data['id'] + '-' + (i + 1)).val());
							}
						});
					}
				}

				if(!$.isEmptyObject(options.ajax.data)){
					$.each(options.ajax.data, function(index, value){
						if(typeof(value) == "string") {
							formData.append(index, value);
						}else{
							formData.append(index, value.val());
						}
					});
				}

				$.ajax({
					url: options.ajax.url,  //Server script to process data
					type: 'POST',
					xhr: function() {  // Custom XMLHttpRequest
						var myXhr = $.ajaxSettings.xhr();
						if(myXhr.upload){ // Check if upload property exists
							myXhr.upload.addEventListener("progress", function(event){
								var percent = 0;
								var position = event.loaded || event.position; /*event.position is deprecated*/
								var total = event.total;
								if (event.lengthComputable) {
									$('#' + options.prefix + 'ProgressBar').css('width', (position / total) * 100 + '%');
								}
							}, false);
						}
						return myXhr;
					},
					//Ajax events
					complete: function(data) {
						options.completeCallback();
						self.close();
					},
					// Form data
					data: formData,
					//Options to tell jQuery not to process data or worry about content-type.
					cache: false,
					contentType: false,
					processData: false
				});

			}else{

				var formData = $(":text", $(options.prefix + 'Form')).serializeArray();

				if(!$.isEmptyObject(options.ajax.data)){
					$.each(options.ajax.data, function(index, value){
						formData.push({'name': index, 'value':value});
					});
				}

				if(!$.isEmptyObject(options.ajax.data)){
					$.each(options.ajax.data, function(index, value){
						if(typeof(value) == "string") {
							formData.push({'name': index, 'value':value});
						}else{
							formData.push({'name': index, 'value':value.val()});
						}
					});
				}

				var fileData = options.fileArray.serializeArray();

				$.ajax(options.ajax.url, {
					data: formData,
					files: fileData,
					iframe: true,
					processData: false
				}).complete(function(data) {
					options.completeCallback();
					self.close();
				});
			}

			self._trigger('upload');
		},

		supportFormData: function () {
			return !! window.FormData;
		}

	});
}( jQuery ) );