/*
 * jQuery UI PDF Embed
 */
(function( $, undefined ) {
	
	$.widget( "ui.pdfEmbed", {
	
		options: {
			objId : '',
			width  : '600',
			height : '700',
			url : '',
			pluginTypeFound : false,
			pdfOpenParams: {
				navpanes: 1,
				statusbar: 0,
				view: "FitH",
				pagemode: "thumbs"
			}
		},
		
		_create: function(){
			var self = this;
			var options = this.options;
			
			options.objID = self.element.attr('id');
			
			if(options.url != ''){
				self.load();
			}
		},
		
		_setOption: function( key, value ) {
			this.options[ key ] = value;
		},
		
		load: function(url){
			var self = this;
			var options = this.options;
			
			if(url != ''){
				options.url = url;	
			}
			
			//The hash (#) prevents odd behavior in Windows
			//Append optional Adobe params for opening document
			url = encodeURI(options.url) + "#" + self.buildQueryString(options.pdfOpenParams);
			self.pluginFound(function(){
				self.embed(url);
			});
		},
		
		hasReaderActiveX: function (){
			var self = this;
			var options = this.options;

			var axObj = null;
			
			if (window.ActiveXObject) {
				
				axObj = new ActiveXObject("AcroPDF.PDF");
					
				//If "AcroPDF.PDF" didn't work, try "PDF.PdfCtrl"
				if(!axObj){
					axObj = new ActiveXObject("PDF.PdfCtrl");
				}
				
				//If either "AcroPDF.PDF" or "PDF.PdfCtrl" are found, return true
				if (axObj !== null) {
					return true;
				}
		
			}
			
			//If you got to this point, there's no ActiveXObject for PDFs
			return false;
			
		},

		//Tests specifically for Adobe Reader (aka Adobe Acrobat) in non-IE browsers
		hasReader: function (){
			var self = this;
			var options = this.options;
		
			var i,
				n = navigator.plugins,
				count = n.length,
				regx = /Adobe Reader|Adobe PDF|Acrobat/gi;
			
			for(i=0; i<count; i++){
				if(regx.test(n[i].name)){
					return true;
				}
			}
			
			return false;
		
		},
		
		//Detects unbranded PDF support
		hasGeneric: function (){
			var self = this;
			var options = this.options;
			
			var plugin = navigator.mimeTypes["application/pdf"];
			return (plugin && plugin.enabledPlugin);
		},

		//Determines what kind of PDF support is available: Adobe or generic
		pluginFound: function (callback){
			var self = this;
			var options = this.options;
		
			var type = null;
			
			if(self.hasReader() || self.hasReaderActiveX()){
				
				type = "Adobe";
			
			} else if(self.hasGeneric()) {
			
				type = "generic";
			
			}
			
			options.pluginTypeFound = type;

			
            if($.isFunction(callback) === true){
				callback();
			}
		
		},

		//If setting PDF to fill page, need to handle some CSS first
		setCssForFullWindowPdf: function (){
			var self = this;
			var options = this.options;
			
			var html = $("html");
			var body = $("body");
			
			if(!html){ return false; }
			
			html.css({
				height : "100%",
				overflow : "hidden"
			});
			
			body.css({
				margin : "0",
				padding : "0",
				height : "100%",
				overflow : "hidden"
			});
			
		},

		//Creating a querystring for using PDF Open parameters when embedding PDF
		buildQueryString: function(pdfParams){
			var self = this;
			var options = this.options;
			
			var string = "",
				prop;
			
			if(!pdfParams){ return string; }
			
			for (prop in pdfParams) {
				
				if (pdfParams.hasOwnProperty(prop)) {
					
					string += prop + "=";
					
					if(prop === "search") {
						
						string += encodeURI(pdfParams[prop]);
					
					} else {
						
						string += pdfParams[prop];
						
					}	
					
					string += "&";
					
				}
				
			}
			
			//Remove last ampersand
			return string.slice(0, string.length - 1);
		
		},
		
		/* ----------------------------------------------------
		   PDF Embedding functions
		   ---------------------------------------------------- */
		embed: function(url){
			var self = this;
			var options = this.options;

			if(!options.pluginTypeFound){ return false; }

			var targetNode = null;

			targetNode = $('#' + options.objID);
			
			targetNode.html('<object	data="' +url +'" type="application/pdf" width="' + options.width +'" height="' + options.height +'"></object>');

		}
		
	});
}( jQuery ) );